//compile with: g++ -std=c++11 -o outfile convert_rootfile_to_data_structure.cc `root-config --cflags --glibs`

#include <iostream>
#include <iomanip>
#include <string>
#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <cstdlib>
#include <fstream>
#include<vector>
#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TMarker.h"
#include "TError.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TChain.h"

using namespace std;

# ifndef __CINT__

bool check_if_zero(double array[], int size)
{
  for (int i = 0; i < size; i++)
  {
      if(array[i] != 0)
      {
        return false;
      }
  }
  return true;
}

int main(int argc, char *argv[]) {

    TString newfilename = "converted_g4_pbars_0_0_carbon_ftf.root";
    cout << newfilename << endl;
	
	TFile* newfile = new TFile(newfilename, "RECREATE");
	
	TFile  myFile(argv[1]);
	
	TTree* newTree = new TTree("HbarEventTree", "HbarEventTree");
	newTree->SetDirectory(0);
	
	cout << "file..." << argv[1] << endl;	
	TTree* myTree = (TTree*) myFile.Get("hitTree"); 
	
	TString timestampI_name = "timeStampsInnerLayer";
	TString timestampO_name = "timeStampsOuterLayer";

	TString timestampfI_name = "timeStampsInnerFibre";
	TString timestampfO_name = "timeStampsOuterFibre";
		
    TString chargeI_name = "ChargeInnerLayer";
	TString chargeO_name = "ChargeOuterLayer";

    TString chargeIf_name = "ChargeInnerFibre";
	TString chargeOf_name = "ChargeOuterFibre";
	
	TString zpositionI_name = "ZPositionInnerLayer";
	TString zpositionO_name = "ZPositionOuterLayer";
    TString xpositionI_name = "XPositionInnerLayer";
	TString xpositionO_name = "XPositionOuterLayer";
    TString ypositionI_name = "YPositionInnerLayer";
	TString ypositionO_name = "YPositionOuterLayer";
	
	TString zpositionfI_name = "ZPositionInnerFibre";
	TString zpositionfO_name = "ZPositionOuterFibre";
    TString xpositionfI_name = "XPositionInnerFibre";
	TString xpositionfO_name = "XPositionOuterFibre";
    TString ypositionfI_name = "YPositionInnerFibre";
	TString ypositionfO_name = "YPositionOuterFibre";	
	
	TString bgoenergy_name = "BGOEnergy"; 
	TString bgo_time_name = "BGOTime";
	TString bgo_xpos_name = "BGOXPos";
	TString bgo_ypos_name = "BGOYPos";
    TString bgo_zpos_name = "BGOZPos";
    
    /*TString particleI_name = "particleI";
    TString particlefI_name = "particlefI";    
    TString particleO_name = "particleO";
    TString particlefO_name = "particlefO";
    TString particleBGO_name = "particleBGO";*/
        
    double timestampI[32];
    double timestampO[32];

    double timestampfI[63];
    double timestampfO[100];
        
    double chargeI[32];
    double chargeO[32];

    double chargefI[63];
    double chargefO[100];
    
    double zposI[32];
    double zposO[32];
    double xposI[32];
    double xposO[32];
    double yposI[32];
    double yposO[32]; 
    
    double zposfI[63];
    double zposfO[100];
    double xposfI[63];
    double xposfO[100];
    double yposfI[63];
    double yposfO[100];     
      
    
    for (int i=0; i<32; i++) {
        zposI[i] = 0.0;
        zposO[i] = 0.0;        
        xposI[i] = 0.0;     
        xposO[i] = 0.0;
        yposI[i] = 0.0;        
        yposO[i] = 0.0;                
    }
            
    //Char_t particleBGO[1000][30];
    //newTree->Branch(particleBGO_name, &particleBGO, particleBGO_name + "[30][30]/C");
    
    /*vector<string> particleBGO;
    newTree->Branch(particleBGO_name, &particleBGO);*/
      
    double bgoenergy = 0.0;
    double bgo_xpos = 0.0;
    double bgo_ypos = 0.0;
    double bgo_zpos = 0.0;
    double bgo_time = 0.0;
        
    newTree->Branch(timestampI_name, timestampI, timestampI_name + "[32]/D");
    newTree->Branch(timestampO_name, timestampO, timestampO_name + "[32]/D");

    newTree->Branch(timestampfI_name, timestampfI, timestampfI_name + "[63]/D");
    newTree->Branch(timestampfO_name, timestampfO, timestampfO_name + "[100]/D");
        
    newTree->Branch(chargeI_name, chargeI, chargeI_name + "[32]/D");
    newTree->Branch(chargeO_name, chargeO, chargeO_name + "[32]/D");
    
    /////////////////////////////////////////////
    newTree->Branch(chargeIf_name, chargefI, chargeIf_name + "[63]/D");
    newTree->Branch(chargeOf_name, chargefO, chargeOf_name + "[100]/D");
        
    /////////////////////////////////////////////

    /*Char_t particleI[32][30];
    Char_t particleO[32][30];
    
    Char_t particlefI[63][30];
    Char_t particlefO[100][30];
    
    for(int i=0; i<32; i++) {
        newTree->Branch(particleI_name + to_string(i), &particleI[i], particleI_name + to_string(i)+ "[30][20]/C"); // up to 20 particle entries per event
        newTree->Branch(particleO_name + to_string(i), &particleO[i], particleO_name + to_string(i)+ "[30][20]/C");   
    }
    
    for(int i=0; i<63; i++) {
        newTree->Branch(particlefI_name + to_string(i), &particlefI[i], particlefI_name + to_string(i)+ "[30][20]/C");
   
    }

    for(int i=0; i<100; i++) {
        newTree->Branch(particlefO_name + to_string(i), &particlefO[i], particlefO_name + to_string(i)+ "[30][20]/C");
   
    }    */
    //TClonesArray* particleI = new TClonesArray("TObjString");    
    //newTree->Branch("particleI", &particleI);   
    //newTree->Branch(particleBGO_name, &particleBGO,"particleBGO/C");
    
    //newTree->Branch(particleO_name, "TObjString", &particleO[0]);
    //newTree->Branch(particleBGO_name, "TObjString", &particleBGO);
    //newTree->Branch(particleI_name, "TObjString", &particleI[0]);
    
    //newTree->Branch(particleO_name, &particleO[0]);
    //newTree->Branch(particleI_name, &particleI[0]);
    
    newTree->Branch(zpositionI_name, zposI, zpositionI_name + "[32]/D");
    newTree->Branch(zpositionO_name, zposO, zpositionO_name + "[32]/D");
    newTree->Branch(xpositionI_name, xposI, xpositionI_name + "[32]/D");
    newTree->Branch(xpositionO_name, xposO, xpositionO_name + "[32]/D");
    newTree->Branch(ypositionI_name, yposI, ypositionI_name + "[32]/D");
    newTree->Branch(ypositionO_name, yposO, ypositionO_name + "[32]/D");
    
    
    newTree->Branch(zpositionfI_name, zposfI, zpositionfI_name + "[63]/D");
    newTree->Branch(zpositionfO_name, zposfO, zpositionfO_name + "[100]/D");
    newTree->Branch(xpositionfI_name, xposfI, xpositionfI_name + "[63]/D");
    newTree->Branch(xpositionfO_name, xposfO, xpositionfO_name + "[100]/D");
    newTree->Branch(ypositionfI_name, yposfI, ypositionfI_name + "[63]/D");
    newTree->Branch(ypositionfO_name, yposfO, ypositionfO_name + "[100]/D");
        
    newTree->Branch(bgo_time_name, &bgo_time, bgo_time_name + "/D");
    newTree->Branch(bgoenergy_name, &bgoenergy, bgoenergy_name + "/D");
    newTree->Branch(bgo_xpos_name, &bgo_xpos, bgo_xpos_name + "/D");
    newTree->Branch(bgo_ypos_name, &bgo_ypos, bgo_ypos_name + "/D");    
    newTree->Branch(bgo_zpos_name, &bgo_zpos, bgo_zpos_name + "/D"); 
		
	//newTree->Fill();
	// Write tree to file:
    //newTree->Write("", TObject::kOverwrite);
    // Close file:
    //newfile.Close();

	//newTree->SetDirectory(0);
	
	vector<double> energies_events;
	Float_t BGO_Energy; 	
	TString det_name1;

	TLeaf* energyleaf = myTree->GetLeaf("energy", "Edep");
	
	TLeaf* evtleaf = myTree->GetLeaf("event", "event");
	TLeaf* poszleaf = myTree->GetLeaf("position", "z"); 
	TLeaf* posxleaf = myTree->GetLeaf("position", "x"); 
	TLeaf* posyleaf = myTree->GetLeaf("position", "y"); 
	
	double timing = 0.0;
	myTree->SetBranchAddress("time", &timing);
		
	//TObject *centerstr = myFile.Get("HodoscopeCenter");
	double bgodet_center = 2.; //1.866 bis 1.871
	double hodo_center = 2; //1.899;
	TObject *primary_entr = myFile.Get("PrimaryName");
	 
	TString bgo_name = "BGO2014";
	
	TString hodo_name = "Hodoscope";
	
	TString innerfibre_name = "InnerFibre";
	TString outerfibre_name = "OuterFibre";
	
	//TString hodo_name = "Hodor";
		
	char detchar[20];
	det_name1 = "detector";
	myTree->SetBranchAddress(det_name1, &detchar);
	
	
	cout << "here" << endl;
	
	Char_t particlechar[30];
	particlechar[0] = 'n';
	particlechar[1] = 'o';
	particlechar[2] = 'n';
	particlechar[3] = 'e';
	particlechar[4] = '\0';
	TString particle_name = "particle";
	myTree->SetBranchAddress(particle_name, &particlechar);
	
	cout << "here2" << endl;
	
    double etotbgo = 0.0;

    int oldevt = 0;
    int oldbarnr = 0;

    int oldfibre_nr = 0;    
    
    int oldlayer = 0;
    int nEntries = myTree->GetEntries(); 
    
    int i_counter = 0;
    int bgo_counter = 0;
    int o_counter = 0;
    
    
    for (int iEnt = 0; iEnt < nEntries; iEnt++) {
    
        //cout << "evt: " << evtleaf->GetValue() << " bgo_counter: "<<  bgo_counter << endl;
    
        myTree->GetEntry(iEnt); 
        
        if(oldevt < evtleaf->GetValue()) {
		    oldevt = evtleaf->GetValue();	
		    
		    
		    if( !check_if_zero(chargeI, 32) && !check_if_zero(chargeO, 32) ) newTree->Fill();
		    
		    //particleBGO.clear();
		    
		    bgo_counter = 0;
		    bgoenergy = 0.0;
		    bgo_time = 0.0;	
		    bgo_zpos = 0.0;
		    bgo_xpos = 0.0;
		    bgo_ypos = 0.0;
		    for (int i=0; i<32; i++) {
		        chargeI[i] = 0.0;
		        chargeO[i] = 0.0;
		        xposI[i] = 0.0;
		        xposO[i] = 0.0;
		        yposI[i] = 0.0;
		        yposO[i] = 0.0;
		        zposI[i] = 0.0;
		        zposO[i] = 0.0;
		        timestampI[i] = 0.0;
		        timestampO[i] = 0.0;
		       		        
		    }
		    for (int i=0; i<63; i++) {
		        chargefI[i] = 0.0;
		        xposfI[i] = 0.0;
		        yposfI[i] = 0.0;
		        zposfI[i] = 0.0;
		        timestampfI[i] = 0.0;
		       		        
		    }
		    for (int i=0; i<100; i++) {
		        chargefO[i] = 0.0;
		        xposfO[i] = 0.0;
		        yposfO[i] = 0.0;
		        zposfO[i] = 0.0;
		        timestampfO[i] = 0.0;
		       		        
		    }
		}
               
        string detstr = string(detchar);
        string particlestr = string(particlechar);
        
	    if(detstr.find(bgo_name) != std::string::npos) {
	        bgoenergy += energyleaf->GetValue();
	        
	        bgo_counter += 1;
	        
	        //if (particlestr == primary_entr->GetName()) {
	        //for(int i = 0; i< 30; i++)
	        //particleBGO[bgo_counter] = particlechar;
	        //particleBGO.push_back(particlestr);
	        //bgo_counter +=1;
	        
	        
	        if(bgo_counter == 1) { 
	            cout << "particle str: "<< evtleaf->GetValue() << particlestr <<  endl;
	            cout << "IN!" << endl;   
	            bgo_zpos = poszleaf->GetValue() - bgodet_center;
	            bgo_xpos = posxleaf->GetValue();
	            bgo_ypos = posyleaf->GetValue();
	        }
	        
	        if (bgo_time == 0 ) bgo_time = timing;
	        //}
	    }
	    
	    if(detstr.find(hodo_name) != std::string::npos) {
	        int barnr = 0; 
	        int layer = detstr[9] - '0';
	        
	        //std::cout << "layer " << layer << " det " << detstr<< std::endl;
	        
	        //int layer = detstr[5] - '0';
	        if(detstr[12]=='-') barnr = detstr[11] - '0';
	        else barnr = (detstr[11]-'0')*10+(detstr[12]-'0');
	        
	        //std::cout <<  barnr << std::endl;
	        //if(detstr[7]=='0') barnr = detstr[8] - '0';
	        //else barnr = (detstr[7]-'0')*10+(detstr[8]-'0');

	        /*if (oldlayer != layer && oldbarnr != barnr) {
	            if(layer==0) for (int i=0; i<30; i++) particleI[barnr][i]=particlechar[i]; 

	            if(layer==1) for (int i=0; i<30; i++) particleO[barnr][i]=particlechar[i];     
	            
	        }*/
	        layer == 0 ? chargeI[barnr] += energyleaf->GetValue() : chargeO[barnr] += energyleaf->GetValue(); // save energy deposited in bar
	        
            if(layer == 0) {
                if(zposI[barnr]==0.0) zposI[barnr] = poszleaf->GetValue() - hodo_center;
                if(xposI[barnr]==0.0) xposI[barnr] = posxleaf->GetValue();
                if(yposI[barnr]==0.0) yposI[barnr] = posyleaf->GetValue();
                if(timestampI[barnr]==0.0) timestampI[barnr] = timing;
                
                //chargeI[barnr] += energyleaf->GetValue();
            
            }
            if(layer == 1) {
                if(zposO[barnr]==0.0) zposO[barnr] = poszleaf->GetValue() - hodo_center;
                if(xposO[barnr]==0.0) xposO[barnr] = posxleaf->GetValue();
                if(yposO[barnr]==0.0) yposO[barnr] = posyleaf->GetValue();
                if(timestampO[barnr]==0.0) timestampO[barnr] = timing;
                //chargeO[barnr] += energyleaf->GetValue();
            }
	        oldbarnr = barnr;
	        oldlayer = layer;
	        
	        //cout << evtleaf->GetValue() << " " << barnr << " " << particlestr << " " << poszleaf->GetValue() - 1.8685 << endl;
	    }
	    
	    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    
		    if(detstr.find(innerfibre_name) != std::string::npos) {
	        int fibre_nr = 0; 

	        if(detstr[11]=='-') fibre_nr = detstr[10] - '0';
	        else fibre_nr = (detstr[10]-'0')*10+(detstr[11]-'0');
	        //std::cout <<  detstr << std::endl;
	        //std::cout <<  "calc " << fibre_nr << std::endl;

	        /*if (oldfibre_nr != fibre_nr) {
	            for (int i=0; i<30; i++) particlefI[fibre_nr][i]=particlechar[i]; 
                //std::cout <<  "for loop 1  " << fibre_nr << std::endl;
	            
	        }*/
	        
	        chargefI[fibre_nr] += energyleaf->GetValue();

            if(zposfI[fibre_nr]==0.0) zposfI[fibre_nr] = poszleaf->GetValue() - hodo_center;
            if(xposfI[fibre_nr]==0.0) xposfI[fibre_nr] = posxleaf->GetValue();
            if(yposfI[fibre_nr]==0.0) yposfI[fibre_nr] = posyleaf->GetValue();
            if(timestampfI[fibre_nr]==0.0) timestampfI[fibre_nr] = timing;
	        oldfibre_nr = fibre_nr;
	        //cout << evtleaf->GetValue() << " " << barnr << " " << particlestr << " " << poszleaf->GetValue() - 1.8685 << endl;*/
	    }
	    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7        
	   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    
		    if(detstr.find(outerfibre_name) != std::string::npos) {
	        int fibre_nr = 0; 

	        if(detstr[11]=='-') fibre_nr = detstr[10] - '0';
	        else fibre_nr = (detstr[10]-'0')*10+(detstr[11]-'0');
	        //std::cout <<  detstr << std::endl;
	        //std::cout <<  "calc " << fibre_nr << std::endl;

	        /*if (oldfibre_nr != fibre_nr) {
	            for (int i=0; i<30; i++) particlefO[fibre_nr][i]=particlechar[i]; 
                //std::cout <<  "for loop 1  " << fibre_nr << std::endl;
	            
	        }*/
	        
	        chargefO[fibre_nr] += energyleaf->GetValue();

            if(zposfO[fibre_nr]==0.0) zposfO[fibre_nr] = poszleaf->GetValue() - hodo_center;
            if(xposfO[fibre_nr]==0.0) xposfO[fibre_nr] = posxleaf->GetValue();
            if(yposfO[fibre_nr]==0.0) yposfO[fibre_nr] = posyleaf->GetValue();
            if(timestampfO[fibre_nr]==0.0) timestampfO[fibre_nr] = timing;
	        oldfibre_nr = fibre_nr;
	        //cout << evtleaf->GetValue() << " " << barnr << " " << particlestr << " " << poszleaf->GetValue() - 1.8685 << endl;*/
	    }
	    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7   
    }
    
    //std::cout <<  "6 " <<  std::endl;
        // Write tree to file:
    newfile->cd();
    newTree->Write("", TObject::kOverwrite);
    // Close file:
    newfile->Close();
    
    
    /*cout << "reading file as test ..."  << endl;	
    
    TFile* myFile2 = new TFile("converted_file.root");
	TTree* myTree2 = (TTree*) myFile2->Get("HbarEventTree"); 

    string* testchar;

	myTree2->SetBranchAddress("particleBGO", &testchar);
	
	int testent = myTree2->GetEntries(); 
    for (int i = 0; i < testent; i++) {
        myTree2->GetEntry(i); 
        //cout << testchar << endl;  
    }*/


	return 0;//*/
}
# endif
