#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = 'r' #'#990001ff'
cosmiccolor = 'b' # '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':

    print "HI!"
        

    pbardata_orig = np.loadtxt("amplitudes_pbar_17_dec.dat")
    
    print pbardata_orig.shape
    
    innerbar = pbardata_orig[:,1:33]
    outerbar = pbardata_orig[:,33:65]
    innerfib = pbardata_orig[:,65:128]
    
    inds1 = np.arange(128,155)
    
    inds2 = np.arange(156,228)
    
    inds = np.append(inds1,inds2)
    
    outerfib = pbardata_orig[:,inds]
    
    
    

    print outerfib.shape
 
    #print "all in file cosmic: ", len(cosmicdata_orig)
    print "all in file pbar: ", len(pbardata_orig)
    
    fig, ax = plt.subplots()
    
    range1 = 0.1
    range2 = 1.3
    binnumber = 50
    
    

    ##########################################################################  
    y, bin_edges = np.histogram(outerbar, bins=binnumber, range = [range1,range2])
    y0, bin_edges = np.histogram(innerbar, bins=binnumber, range = [range1,range2])
    
    
    """range1 = 2
    range2 = 127
    binnumber = 127 - 2
    y, bin_edges = np.histogram(outerfib, bins=binnumber, range = [range1,range2])
    y0, bin_edges = np.histogram(innerfib, bins=binnumber, range = [range1,range2])"""
    
    
    ax.set_xlim([range1, range2])
    
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm1 = 1.0*sum(np.diff(bin_edges)*y)
    err1 = np.sqrt(y)/norm1
    y = y/norm1
    
    norm0 = 1.0*sum(np.diff(bin_edges)*y0)
    err0 = np.sqrt(y0)/norm0
    y0 = y0/norm0    
    
    ax.errorbar(bin_centers, y, yerr = err1, fmt = '-o', markersize = 0.2, label = "outer bars")
    ax.errorbar(bin_centers, y0, yerr = err0, fmt = '-o', markersize = 0.2, label = "inner bars")
              
    ax.legend()   
    ax.set_xlabel("energy deposit (MeV)")
    #ax.set_xlabel("ToT (ADC bins)")
    ax.set_ylabel("norm. counts")
      
    
    ##################################################################################################################
    
    
    plt.savefig('amps_bar_data.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

