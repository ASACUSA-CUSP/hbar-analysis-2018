#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = 'r' #'#990001ff'
cosmiccolor = 'b' # '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':

    print "HI!"
        

    #pbardata_orig = np.loadtxt("sims_amps_outer_bar.dat")
    #pbardata_orig0 = np.loadtxt("sims_amps_inner_bar.dat")
    
    #pbardata_orig = np.loadtxt("../chips_ofc.dat")
    #pbardata_orig0 = np.loadtxt("../chips_ifc.dat")

    pbardata_orig = np.loadtxt("../ftfpbert_oc.dat")
    pbardata_orig0 = np.loadtxt("../ftfpbert_ic.dat")
    
    #pbardata_orig = np.loadtxt("../chips_ofc.dat")
    #pbardata_orig0 = np.loadtxt("../chips_ifc.dat")
    
    #print "all in file cosmic: ", len(cosmicdata_orig)
    print "all in file pbar: ", len(pbardata_orig)
    
    fig, ax = plt.subplots()
    
    range1 = 0.1
    range2 = 4
    binnumber = 50
    
    ax.set_xlim([range1, range2])
    
    #ax.set_ylim(0,)

    
    ##########################################################################  
    y, bin_edges = np.histogram(pbardata_orig, bins=binnumber, range = [range1,range2])
    y0, bin_edges = np.histogram(pbardata_orig0, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm1 = 1.0*sum(np.diff(bin_edges)*y)
    err1 = np.sqrt(y)/norm1
    y = y/norm1
    
    norm0 = 1.0*sum(np.diff(bin_edges)*y0)
    err0 = np.sqrt(y0)/norm0
    y0 = y0/norm0    
    
    ax.errorbar(bin_centers, y, yerr = err1, fmt = '-o', markersize = 0.2, label = "outer bars")
    ax.errorbar(bin_centers, y0, yerr = err0, fmt = '-o', markersize = 0.2, label = "inner bars")
              
    ax.legend()   
    ax.set_xlabel("energy deposit (MeV)")
    ax.set_ylabel("norm. counts")
      
    
    ##################################################################################################################
    
    
    plt.savefig('amps_bars_ftfbic.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

