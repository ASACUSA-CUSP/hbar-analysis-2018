#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = 'r' #'#990001ff'
cosmiccolor = 'b' # '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':

    print "HI!"
        

    innerf_pbar = np.loadtxt("sims_amps.dat")
    outerf_pbar = np.loadtxt("sims_amps_outer.dat")



    innerf_pbar[innerf_pbar < 0.5] = 0.0
    outerf_pbar[outerf_pbar < 0.5] = 0.0 
    
    innerf_p_c_le =  np.count_nonzero(innerf_pbar, axis = 0)
    #innerf_p_c_tot = np.count_nonzero(innerf_pbar[:,63:127], axis = 0)   
    norm2 = np.sum(innerf_p_c_le)
       
    #outerf_c_le = np.count_nonzero(outerf, axis = 0)
    #outerf_c_tot = np.count_nonzero(outerf[:,100:201], axis = 0)
    #norm3 = np.sum(outerf_c_le)
    
    outerf_p_c_le = np.count_nonzero(outerf_pbar, axis = 0)
    #outerf_p_c_tot = np.count_nonzero(outerf_pbar[:,100:201], axis = 0)
    norm4 = np.sum(outerf_p_c_le)
    
    
    outer_ch = np.arange(100)
    inner_ch = np.arange(63)
    
    
    fig, ax = plt.subplots(1,2, figsize = (10,5))
    
    #ax[0].set_ylim(0,)
    #ax[1].set_ylim(0,)
    
    
    #ax[0].errorbar(inner_ch, innerf_c_le/norm1, yerr = np.sqrt(innerf_c_le)/norm1, fmt = '-o', markersize = 0.2, label = 'counts, inner fibres, le')#
    ax[0].errorbar(inner_ch, innerf_p_c_le/norm2, yerr = np.sqrt(innerf_p_c_le)/norm2, fmt = '-o', markersize = 0.2, label = 'counts, inner fibres, le, pbar')#
    #ax[0].errorbar(inner_ch, innerf_c_tot, yerr = np.sqrt(innerf_c_tot), fmt = '-o', markersize = 0.2, label = 'counts, inner fibres, tot')#
    
    ax[0].legend()
    
    #ax[1].errorbar(outer_ch, outerf_c_le/norm3, yerr = np.sqrt(outerf_c_le)/norm3, fmt = '-o', markersize = 0.2, label = 'counts, outer fibres, le')#
    ax[1].errorbar(outer_ch, outerf_p_c_le/norm4, yerr = np.sqrt(outerf_p_c_le)/norm4, fmt = '-o', markersize = 0.2, label = 'counts, outer fibres, le, pbar')#
    #ax[1].errorbar(outer_ch, outerf_c_tot, yerr = np.sqrt(outerf_c_tot), fmt = '-o', markersize = 0.2, label = 'counts, outer fibres, tot')#
    ax[1].legend()
        
    plt.savefig("fibre_count_plots_sims.png")
       
    plt.show()  
    

