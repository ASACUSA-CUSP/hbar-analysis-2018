#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

from sys           import argv



import re
import math

import matplotlib.pyplot as plt

#################################################################################################################################


if __name__ == '__main__':

    if len(argv) < 2:
        print "usage: ", argv[0], "txt-file"
        exit(-1)

    data = np.loadtxt(argv[1])

    
    fig, ax = plt.subplots()
   
    ######################################################################################################################################################################
    ######################################################################################################################################################################
    
    range1 = 100
    range2 = 1000
    bins = 100
         
    r = np.array([range1,range2])
    
    ax.legend() 
    #ax.set_ylim(0,)
    ax.set_xlim(range1,range2)
        
    histc, edgesc = np.histogram(a=data[:,0], bins=bins, range=r)   
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.    
    ax.errorbar(bincentersc, histc)#
        
  
    plt.savefig("histogram_nr_of_iterations.pdf")
    
    ######################################################################################################################################################################
    ######################################################################################################################################################################
    
    
    fig2, ax2 = plt.subplots()
      
    range1 = 100
    range2 = 1000
    bins = 100
         
    r = np.array([range1,range2])
    
    ax2.legend() 
    #ax.set_ylim(0,)
    ax2.set_xlim(range1,range2)
        
    histc2, edgesc2 = np.histogram(a=data[:,1], bins=bins, range=r)   
    bincentersc2   = edgesc2[:-1]+(edgesc2[1]-edgesc2[0])/2.    
    ax2.errorbar(bincentersc2, histc2)#
        
  
    plt.savefig("histogram_nr_of_calls.pdf")
    
    plt.show()            
    
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
    
        #thefile.close()   

