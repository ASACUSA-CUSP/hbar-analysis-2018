#!/usr/bin/python
# -*- coding: utf-8 -*-
# abgeleitet von tracking_geant4_3dtest_new.py -- unnoetige fkten geloescht, aufgeraumt, eigene files in src for plotting etc
import numpy as np
#import Image
import math
import matplotlib.backends.backend_pdf
from matplotlib.lines import Line2D             
from matplotlib.cm import  gray_r, gray, bone
from matplotlib import lines as mpl_lines 
from matplotlib.patches import Ellipse
from matplotlib.patches import Circle
import itertools as it 
#from itertools import product, combinations
from numpy import random

from itertools import groupby
from operator import itemgetter
import itertools
import matplotlib.pyplot as plt
import re

import scipy.stats as stats

import cv2

#from matplotlib import cm
#import scipy.optimize as optimize

#from ROOT import TF3, Fit, Minuit2
#import ROOT
#from rootpy.tree   import TreeChain
#from collections import defaultdict


import sys
sys.path.append("..")


from src.trackplot import *
from src.trackfinding_new_workon_sims_plots_for_thesis import *
from src.hodoscope_trackhelper import *
from src.trackfitting2D_4 import *
from src.trackfitting3D_2 import *
from src.vertex_reconstruction import *
from src.hodoscope_tracking import * # same as just hodoscope with a few extra functions for tracking
from src.bgo import * # newest calibration
from src.scalar import *

from numpy import ndarray

cosmic_count = 0
event_count_tot = 0

#from skimage.transform import (hough_line, probabilistic_hough_line, hough_line_peaks)
#from skimage.color import rgb2gray
#from skimage.filters import gaussian_filter
#from skimage.morphology import skeletonize

##############################################################################################################        
##############################################################################################################
##############################################################################################################

##############################################################################################################     
##############################################################################################################
##############################################################################################################
def find_consecutive_nr(numbers):    
    clusters = []
    yesno = False
    for k, g in groupby(enumerate(numbers), lambda (i, x): i-x): # total clusters... also single bars
        clusters.append(map(itemgetter(1), g))
    #print "clusters::::::::::: ", clusters
    tempc = [] # save clusters with hits of 31 and 0
    nwclusters = list(clusters)  # needed for loop
        
    for c in clusters:
        if len(c) > 1:
            #print "c::::::::::", c
            yesno = True  
        if 31 in c:
            #print "yes inside!"
            tempc.append(c)
            nwclusters.remove(c)
        if 0 in c:
            tempc.append(c)
            nwclusters.remove(c)
    
    #print "clusters temp::::::::::: ", tempc                
    if len(tempc) >= 1: # if both clusters with 31 and 0 exist, merge them
        newcluster = [item for sublist in tempc for item in sublist]
        nwclusters.append(newcluster)

    return yesno, nwclusters    # bool: found cluster: yes or no? 
##############################################################################################################
def is_it_a_hit(h, cable_corr_i, cable_corr_o): 

    #print cable_corr_i
    conv_factor_i = 0.014177797004048582995951417004048582995951417004048582995 # inner to mm        
    conv_factor_o = 0.014127641127819548872180451127819548872180451127819548872 # outer to mm

    mt_cuts_inner = [112,120.5]
    mt_cuts_outer = [112,121.7]
                                    # cuts based on meantimes and zpos distributions for whole hodor with cut on bgo E of 20MeV
    z_cuts_inner = [-200,200]
    z_cuts_outer = [-300,300]
    
    t_glob_o = 1.39542395
    t_glob_i = 1.84575840
    
    # get timestamps of active bars
    new_active_bars = []
    nI, nO, I, O = h.getActiveBar()     
    numbers = np.arange(32)  
    currI = numbers[I]
    currO = numbers[O] 
    
    np.set_printoptions(precision=2, suppress=True)
    
    """print "currO vorher: ", currO
    print "currI vorher: ", currI"""
    
    ######
    t_i_u = h.i.CFU[currI] + cable_corr_i[currI][:,0] - t_glob_i 
    t_i_d = h.i.CFD[currI] + cable_corr_i[currI][:,1]    
    t_i_ud =  np.transpose([currI,t_i_u,t_i_d])
    
   # print t_i_ud
   
    #####
    t_o_u = h.o.CFU[currO] + cable_corr_o[currO][:,0]  - t_glob_o 
    t_o_d = h.o.CFD[currO] + cable_corr_o[currO][:,1]    
    t_o_ud =  np.transpose([currO,t_o_u,t_o_d])
    
    """print "#### OUTER "
    print "vorher", t_o_ud
    
    print "zpos ", (t_o_ud[:,1] - t_o_ud[:,2])/conv_factor_o
    
    print "meant",  (t_o_ud[:,1] + t_o_ud[:,2])*0.5
    
    print "#### INNER "
    print "vorher", t_i_ud
    
    print "zpos ", (t_i_ud[:,1] - t_i_ud[:,2])/conv_factor_i
    
    print "meant",  (t_i_ud[:,1] + t_i_ud[:,2])*0.5"""
        
    t_i_ud_new = t_i_ud[np.logical_and(
                    np.logical_and( (t_i_ud[:,1] - t_i_ud[:,2])/conv_factor_i > z_cuts_inner[0], (t_i_ud[:,1] - t_i_ud[:,2])/conv_factor_i < z_cuts_inner[1]),
                    np.logical_and( (t_i_ud[:,1] + t_i_ud[:,2])*0.5 > mt_cuts_inner[0], (t_i_ud[:,1] + t_i_ud[:,2])*0.5< mt_cuts_inner[1])
                    )]
    
    t_o_ud_new = t_o_ud[np.logical_and(
                    np.logical_and( (t_o_ud[:,1] - t_o_ud[:,2])/conv_factor_o > z_cuts_outer[0], (t_o_ud[:,1] - t_o_ud[:,2])/conv_factor_o < z_cuts_outer[1]),
                    np.logical_and( (t_o_ud[:,1] + t_o_ud[:,2])*0.5  > mt_cuts_outer[0], (t_o_ud[:,1] + t_o_ud[:,2])*0.5  < mt_cuts_outer[1])
                    )]      
                    
    ###########
    currI = t_i_ud_new[:,0].astype(int)
    currO = t_o_ud_new[:,0].astype(int)
    
    
    
    #print "nachher ", t_o_ud_new#
    
    
    z_o_pos = (- t_o_ud_new[:,1] + t_o_ud_new[:,2])/conv_factor_o         # # FIXME this is not a fixme. the minus is so that it is confrom with the fibre orietation
    z_o_pos = np.transpose([currO,z_o_pos])
    z_i_pos = (- t_i_ud_new[:,1] + t_i_ud_new[:,2])/conv_factor_i
    z_i_pos = np.transpose([currI,z_i_pos])
    
    
    
    #print "****** zpos_i", z_o_pos
    #print "meant ", (t_o_ud[:,1] + t_o_ud[:,2])*0.5
    
    return t_i_ud_new, t_o_ud_new, z_i_pos, z_o_pos, currI, currO      
##################################################################################################################################################################
##################################################################################################################################################################
def is_it_an_orphan(currI, currO):

    currI_c = np.array(currI)
    currO_c = np.array(currO)
    
    list_to_del = []

    for n,c in enumerate(currI):
        array_clustercheck = []
        #print " cluster: ", c 
        #c = c[0]
                
        if c == 31:
            c2 = 0
        else:
            c2 = c + 1
                
        if c == 0:
            c0 = 31
        else:
            c0 = c - 1
                                       
        array_clustercheck = [c0,c,c2]
                
        #check if this intersects with the other layer
        good_hits = list(set(array_clustercheck).intersection(currO))
                       
        if len(good_hits) == 0:
            #print "ja, inner!!!!!!!!!!!", c, n
            list_to_del.append(n)
            #print currI_c           
    
    currI_c = np.delete(currI, list_to_del)   
    
    list_to_del = []
           
    for m,c in enumerate(currO):
        array_clustercheck = []
        #print "1 cluster: ", c 
        #c = c[0]
                    
        if c == 31:
            c2 = 0
        else:
            c2 = c + 1
                    
        if c == 0:
            c0 = 31
        else:
            c0 = c - 1
                                          
        array_clustercheck = [c0,c,c2]
                    
        #check if this intersects with the other layer
        good_hits = list(set(array_clustercheck).intersection(currI))
                    
            
        if len(good_hits) == 0:
            #print "ja, outer!!!!!!!!!!!", c
            list_to_del.append(m)
            #currO_c = np.delete(currO_c, m)    


    currO_c = np.delete(currO, list_to_del)   
    #print "oprph", currI_c
    #print currO_c
    
    return currI_c, currO_c
##################################################################################################################################################################
##################################################################################################################################################################    
def calc_pvals_mtds(event_mtd):    
    # FOR THE COMBINED MTD
    # cosmics: PRELIM!!!! 
    #cosmics
    #Beta: [ 0.98275134  0.78375044  0.25303222]
    #Beta Std Error: [ 0.02289982  0.00657134  0.00429869]
    mean_c = 0.78375044
    sigma_c = 0.25303222

    # pbars: PRELIM !!!! 
    #Beta: [ 0.99472814 -0.12751322  0.252425  ]
    #Beta Std Error: [ 0.02723027  0.00780577  0.00518992]

    mean_p = -0.12751322
    sigma_p = 0.252425 


    #cosmic distribution:    
    #distr_cosmic = stats.norm(loc = mean_c, scale = sigma_c)      
    # pbar distribution:    
    #distr_pbar = stats.norm(loc = mean_p, scale = sigma_p)  
        
    z_score_c = (event_mtd - mean_c) / sigma_c
    z_score_p = (event_mtd - mean_p) / sigma_p
             
    pval_c = stats.norm.sf(abs(z_score_c))
    pval_p = stats.norm.sf(abs(z_score_p)) 
 
    #pval_p = stats.norm.cdf(event_mtd + 50, loc = mean_p, scale = sigma_p)
        
    print "z-score, p-value cosmic: ", z_score_c, pval_c      
    print "z-score, p-value pbar: ", z_score_p, pval_p  
    
    return pval_c, pval_p
##################################################################################################################################################################
##################################################################################################################################################################

def calc_angle_for_1track(trackcoll, vertex):    
    
        positionmap = getPositionMap()
        #print "Trackcoll before: "
        #for temp in trackcoll:
        #    print temp

        pos_inner = positionmap[0]
        pos_outer = positionmap[1]

        add = np.arange(32)

        pos_inner = np.column_stack((add, pos_inner))
        pos_outer = np.column_stack((add, pos_outer))
        #trackcoll_temp = list(trackcoll)
        tr = trackcoll[0]
        anglesY = []
        upwards = 0
        downwards = 0
        horizont = 0

       
        #print "n, m ", n, m
        merge_o = tr[1][1]
        
        ro_merge = pos_outer[merge_o]
        ro_new = np.mean(ro_merge[:,0:], axis = 0)
        
        vA = [(vertex[0]-ro_new[1]), (vertex[1]-ro_new[2])]
        magA = dot(vA, vA)**0.5   
                 
        vY = [0,1]
        vmY = [0,-1]
                    
        # Get dot prod
        dot_prodY = dot(vA/magA, vY)
        #print "dot_prodY ", dot_prodY
        if abs(dot_prodY) < 0.30:
            horizont += 1
            #print "horiz "
        elif dot_prodY >= 0.30:
            downwards += 1
            #print "down "
        elif dot_prodY < 0:
            upwards += 1
            #print "up "
                   
        dot_prodmY = dot(vA/magA, vmY)
        # Get magnitudes
        #magY = 1.0
        # Get cosine value
        cos_Y = dot_prodY
        cos_mY = dot_prodmY
                    
        angleY = math.acos(cos_Y)
        angleY = math.degrees(angleY)%360
        anglemY = math.acos(cos_mY)
        anglemY = math.degrees(anglemY)%360
        anglesY.append(min(angleY, anglemY))
                    
        #print "ANGLES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        #print anglesY
        #print "ANGLES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        anglesY = np.array(anglesY)
        
        return anglesY, [upwards, downwards, horizont]

###################################################################################################################################################################################

def calc_mtds_of_tracks_corr_std(trackcoll, cfs_i, cfs_o, zinner, zouter, vertex): #, rinner, router):
    
    mtds = []
    #print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    print cfs_i
    positionmap = getPositionMap()
    #print "Trackcoll before: "
    #for temp in trackcoll:
    #    print temp

    pos_inner = positionmap[0]
    pos_outer = positionmap[1]

    add = np.arange(32)

    pos_inner = np.column_stack((add, pos_inner))
    pos_outer = np.column_stack((add, pos_outer))

    angles = [999,999,999]
    #print "#################################"
    if len(trackcoll) >= 2:
    
        trackcoll_temp = list(trackcoll)
        angles = []
        anglesY = []
        upwards = 0
        downwards = 0
        horizont = 0
        for n, tr in enumerate(trackcoll_temp):            
            for m, tr2 in enumerate(trackcoll_temp):                              
                #if tr != tr2 and n < m: a
                if m == n+1 or (n == len(trackcoll)-1 and m == 0):                   
                    #print "n, m ", n, m
                    #print "tr ", tr
                    #print "tr2 ", tr2
                    merge_o = tr[1][1]
                    merge_o2 = tr2[1][1]
                    ro_merge = pos_outer[merge_o]
                    ro_merge2 = pos_outer[merge_o2]

                    ro_new = np.mean(ro_merge[:,0:], axis = 0)
                    ro_new2 = np.mean(ro_merge2[:,0:], axis = 0) 

                    vA = [(vertex[0]-ro_new[1]), (vertex[1]-ro_new[2])]
                    vB = [(vertex[0]-ro_new2[1]), (vertex[1]-ro_new2[2])]                    
                    vY = [0,1]
                    vmY = [0,-1]
                    
                    # Get dot prod
                    dot_prod = dot(vA, vB)
                    # Get magnitudes
                    magA = dot(vA, vA)**0.5
                    magB = dot(vB, vB)**0.5
                    # Get cosine value
                    cos_ = dot_prod/magA/magB
                    
                    # Get dot prod
                    dot_prodY = dot(vA/magA, vY)
                    #print "dot_prodY ", dot_prodY
                    if abs(dot_prodY) < 0.30:
                        horizont += 1
                        #print "horiz "
                    elif dot_prodY >= 0.30:
                        downwards += 1
                        #print "down "
                    elif dot_prodY < 0:
                        upwards += 1
                        #print "up "
                    
                    dot_prodmY = dot(vA/magA, vmY)
                    # Get magnitudes
                    #magY = 1.0
                    # Get cosine value
                    cos_Y = dot_prodY
                    cos_mY = dot_prodmY
                    
                    angleY = math.acos(cos_Y)
                    angleY = math.degrees(angleY)%360
                    anglemY = math.acos(cos_mY)
                    anglemY = math.degrees(anglemY)%360
                    #print "Y ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
                    #print angleY, anglemY
                    #print "Y ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
                    anglesY.append(min(angleY, anglemY))
                    
                    if abs(cos_) > 1:
                        angles.append(999)
                    else:                    
                        # Get angle in radians and then convert to degrees
                        angle = math.acos(cos_)
                        # Basically doing angle <- angle mod 360
                        angle = math.degrees(angle)%360
                        angles.append(angle)
                    #print "angle ", angle 
                    
                    
        #print "ANGLES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        #print angles 
        #print anglesY
        #print "ANGLES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        angles = np.array(angles)
        #print len(angles[angles < 30])
        #trackcoll_temp = list(trackcoll)
        for n,j in enumerate(angles):            
       
            if j < 30 and len(trackcoll) > 2:
                #print "NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOW!!! ANGLE", j
                
                n1 = n
                n2 = n + 1
                if n == len(angles)-1:
                    n2 = 0
                    
                track1 = trackcoll[n1]
                track2 = trackcoll[n2]
                                
                """if n == 0:
                    track1 = trackcoll[0]
                    track2 = trackcoll[1]
                elif n == 1:
                    track1 = trackcoll[1]               # TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! attention to the order!!!
                    track2 = trackcoll[2]
                elif n == 2:
                    track1 = trackcoll[2]
                    track2 = trackcoll[0]"""

                #print "track1 ", track1
                #print "track2 ", track2
                
                
                merge_i = track1[0][1]
                merge_i.append(track2[0][1][0])
                merge_o = track1[1][1]
                merge_o.append(track2[1][1][0])
                merge_i = list(set(merge_i))
                merge_o = list(set(merge_o))
                ##########################################################################
                merge_cfi = cfs_i[np.nonzero(np.in1d(cfs_i[:,0],np.array(merge_i)))[0]] 
                cfi_new = np.mean(merge_cfi[:,0:], axis = 0) 
                cfi_new[0] = cfi_new[0] + 999 

                cfs_i  = np.vstack([cfs_i, cfi_new])
                ##########################################################################
                merge_cfo = cfs_o[np.nonzero(np.in1d(cfs_o[:,0],np.array(merge_o)))[0]] 
                cfo_new = np.mean(merge_cfo[:,0:], axis = 0) 
                cfo_new[0] = cfo_new[0] + 999  
                cfs_o  = np.vstack([cfs_o, cfo_new])


                ##########################################################################
                merge_zi = zinner[np.nonzero(np.in1d(zinner[:,0],np.array(merge_i)))[0]] #[0][1]     
                merge_zo = zouter[np.nonzero(np.in1d(zouter[:,0],np.array(merge_o)))[0]] #[0][1]

                zi_track = np.mean(merge_zi[:,0:], axis = 0)
                zo_track = np.mean(merge_zo[:,0:], axis = 0)
                ##########################################################################
                zinner_new = np.mean(merge_zi[:,0:], axis = 0)  
                zinner_new[0] = zinner_new[0] + 999 
                zinner  = np.vstack([zinner, zinner_new])

                zouter_new = np.mean(merge_zo[:,0:], axis = 0) 
                zouter_new[0] = zouter_new[0] + 999 
                zouter  = np.vstack([zouter, zouter_new])
                ##########################################################################
                ri_merge = pos_inner[merge_i]
                ro_merge = pos_outer[merge_o]

                ri_new = np.mean(ri_merge[:,0:], axis = 0)
                ro_new = np.mean(ro_merge[:,0:], axis = 0)
                
                ri_new[0] = ri_new[0] + 999
                ro_new[0] = ro_new[0] + 999

                pos_inner  = np.vstack([pos_inner, ri_new])
                pos_outer  = np.vstack([pos_outer, ro_new])

                new_track = [[0,[cfi_new[0] - 999]], [1,[cfo_new[0] - 999]], 999]
                #print "new track ", new_track
          
                if track1 in trackcoll_temp:
                    trackcoll_temp.remove(track1)
                if track2 in trackcoll_temp:
                    trackcoll_temp.remove(track2)

                trackcoll_temp.append(new_track)           
                #break
                
        trackcoll = trackcoll_temp
        
        #print "trackcoll_temp"
        #for temp in trackcoll_temp:
        #    print temp 
    #print "lennnnnnnnnnnnn", len(pos_outer)
    
   
    #print cfs_i

    #print cfs_o

    #print pos_inner[:,0]
    #print pos_outer[:,0]

    #print "#################################"    
    
    c = 299792458.0*1000.0/1.0e9
    
    #print "trackcoll"
    #for temp in trackcoll:
    #       print temp     
    
    track_meantimes = [] 
       
    # mean loop
    for n, track in enumerate(trackcoll): 
        #print "track ", track
                
        if track[2] == 999:
            track[0][1][0] = track[0][1][0] + 999
            track[1][1][0] = track[1][1][0] + 999


        inner_bar = track[0][1]
        outer_bar = track[1][1]
        #print inner_bar
        #print outer_bar
        
        cfi_track = cfs_i[np.nonzero(np.in1d(cfs_i[:,0],np.array(inner_bar)))[0]]
        #print "cfi", cfi_track
        zi_track = zinner[np.nonzero(np.in1d(zinner[:,0],np.array(inner_bar)))[0]] #[0][1]
        ri_track = pos_inner[np.nonzero(np.in1d(pos_inner[:,0],np.array(inner_bar)))[0]] 
    
        cfi_track = np.mean(cfi_track[:,1:], axis = 0)  # make mean of columns (in case of cluster!)
        #print "cfi track mean: ", cfi_track
        zi_track = np.mean(zi_track[:,1:], axis = 0)
        ri_track = np.mean(ri_track[:,0:], axis = 0)
                   
        ri_track =  np.sqrt((ri_track[1] - vertex[0])*(ri_track[1] - vertex[0]) + (ri_track[2] - vertex[1])*(ri_track[2] - vertex[1]) )
        track_corr = np.sqrt(ri_track*ri_track + zi_track*zi_track)/c #"""
        cfi_track = np.mean(cfi_track) - track_corr
        
        cfo_track = cfs_o[np.nonzero(np.in1d(cfs_o[:,0], np.array(outer_bar)))[0]]  
        zo_track = zouter[np.nonzero(np.in1d(zouter[:,0],np.array(outer_bar)))[0]]
        ro_track = pos_outer[np.nonzero(np.in1d(pos_outer[:,0],np.array(outer_bar)))[0]] 
                
        cfo_track = np.mean(cfo_track[:,1:], axis = 0)  # make mean of columns (in case of cluster!)
        zo_track = np.mean(zo_track[:,1:], axis = 0)
        ro_track = np.mean(ro_track[:,0:], axis = 0)
        ro_track =  np.sqrt((ro_track[1] - vertex[0])*(ro_track[1] - vertex[0]) + (ro_track[2] - vertex[1])*(ro_track[2] - vertex[1]) )

        track_ocorr = np.sqrt(ro_track*ro_track + zo_track*zo_track)/c
        cfo_track = np.mean(cfo_track) - track_ocorr
        
        io_mean = 0.5*(cfo_track + cfi_track)
        
        track_meantimes.append(io_mean)


    print "track_meantimes ", track_meantimes
    event_mean = np.mean(np.array(track_meantimes))
    
    event_max = np.max(track_meantimes)
    event_min = np.min(track_meantimes)
    
    print "event mean: ", event_mean
    print "event max: ", event_max   
    print "event min: ", event_min
        
    event_mtd = 0.0
    for tr_mt in track_meantimes:
        event_mtd = event_mtd + (tr_mt - event_mean)*(tr_mt - event_mean)
        
    #print "event mtd", event_mtd    
    event_mtd = math.sqrt(event_mtd/(1.0*len(trackcoll) - 1.0))
    event_min_max = (event_max - event_min)
    print "event mtd ", event_mtd   
    print "event min max ", event_min_max    
    
    #if len(angles) < 3:
    #    angles = np.append(angles,999)
    #
    return event_mtd, angles, anglesY, event_min_max, [upwards, downwards, horizont]

##################################################################################################################################################################
##################################################################################################################################################################
##################################################################################################################################################################
def get_mixtime_from_scalar(SclT, veryOldTimeMode, oldTimeMode):

    startmixtime = 0.0
    endmixtime = 0.0
    
    for event in SclT:
            s = scalar(event)
            #print "CUSP ", s.cuspRunNumber +1
            #if s.cuspRunNumber +1 < 2800:
            #    continue
            
            #if s.cuspRunNumber+1 not in candidates_evts[:,0]:
            #    continue
            
            #if s.cuspRunNumber+1 < 2639:
            #    continue    
            
            
            
            #if s.cuspRunNumber+1 < 2059:
            #    veryOldTimeMode = True
                #continue
            #else:
            #    veryOldTimeMode = False
            #    oldTimeMode = False
                
            #if s.cuspRunNumber+1 > 1885:
            #    #return 0
            #   continue
            
            #oldTimeMode = True
            

            if s.mixingStart==1.0 and startmixtime != 0.0:
                print "######################################"
                print "# MORE THAN ONE MIXING START SIGNAL! #"
                print "######################################"
          
            if s.mixingStart==1.0:# and startmixtime == 0.0: #NOTE FIXME first mixing start signal is taken!!          
                if veryOldTimeMode:
                    startmixtime = s.midasTime
                    #mixtime.append([startmixtime, endmixtime])                
                else:
                    startmixtime = s.timeStamp 
                    #mixtime.append([startmixtime, endmixtime])
           
            if s.mixingStop>0.0:
                if veryOldTimeMode:
                    endmixtime = s.midasTime
                else:
                    endmixtime = s.timeStamp

            ## FIXME: won't work for runs without mixing stop, like capture tests
            if  startmixtime > 0.0 and endmixtime > 0.0:
                mixtime.append([startmixtime, endmixtime])
                startmixtime = 0.0
                endmixtime = 0.0
        ##########################################################################################################
        
    if printVerbose:
            print "start and end timestamp of mixing: ", startmixtime, endmixtime, " Cusp run number: ", s.cuspRunNumber
            print len(mixtime)
            #mixtime[23][1] = endmixtime
            print "//////////////////////// ", mixtime
            
            
            
    print "start and end timestamp of mixing: ", mixtime, " Cusp run number: ", s.cuspRunNumber
    #if len(mixtime)>0:                     # FIXME if length of last element of mixtime is longer than 2, then take the last one .... then this works with more than one file
    #    mixtime = [mixtime[-1]]

    return mixtime
##################################################################################################################################################################
##################################################################################################################################################################
if __name__ == "__main__":
    import ROOT
    from rootpy.tree import Tree, TreeChain
    import logging
    # Most verbose log level
    logging.basicConfig(level=logging.DEBUG)

    #from matplotlib.patches import Circle
    from matplotlib.collections import PatchCollection
    from sys import argv

    if len(argv) < 2:
        print "usage: ", argv[0], "file1.root  "
        exit(-1)

    bgoCalibData = readBGOCalibrationData("../calibrationData/gainBGOcalibration.dat")
    correctionData = readTimingCorrectionData("../calibrationData/hodoscopeTimingCorrectionTable.dat")  
    #corrtiming = np.loadtxt("./calibrationData/cable_length_corrections.dat")
    cable_corr_inner = np.loadtxt('../calibrationData/cable_corrections_inner_up_down_2_15mev.dat')
    cable_corr_outer = np.loadtxt('../calibrationData/cable_corrections_outer_up_down_2_15mev.dat')
    fibre_inner_cuts = np.loadtxt('../calibrationData/cuts_inner_fibre.dat')
    fibre_outer_cuts = np.loadtxt('../calibrationData/cuts_outer_fibre.dat')
    
  
    t_glob_o = 1.39542395
    t_glob_i = 1.84575840

    midas_run_nr = 999
    
    cand_list = [] # list of candidates, used for filling the root tree

    ####################################################################################################################################################################################
    
    print argv[1]
    
    print " "
    T = TreeChain("HbarEventTree",argv[1])  
      
    Tdummy = TreeChain("HbarEventTree", "output-run-6351_for_dummy_event.root")
    for n, dummy in enumerate(Tdummy):
        if n > 0:
            continue
        dummy_bgo   = BGO(dummy, bgoCalibData)    
        dummy_hodor = hodoscope(dummy, fibre_inner_cuts, fibre_outer_cuts) # """
    ####################################################################################################################################################################################
    ####################################################################################################################################################################################
    Timeintervallconstr = False
    
    plotyes = True  # creates event plots
    plotshow = True        # show the event plots during analysis
    
    printSummary = True    # print summary at the end. Useful for documentation
    
    writeTxt = True
    writeROOT = False
   
    simplecuts = False
    ####################################################################################################################################################################################
    ####################################################################################################################################################################################
    ####################################################################################################################################################################################
    
    PreCutBool = False
    
    BGOcut = 0.0
    
    fibrebool = True
    
    BGOon = False

    adcTimestamp = 0.0
    mixtime = []
    startmixtime = 0.0


    #list_candidates = []
    numbers = np.arange(32)

    
    cuspRunNumber = 0
    startmixAdc = None
    mtd = 999
    

    #########################################################################################################################################################################
    if writeTxt == True:

        file_mtds = open('sims_test.dat', 'w') 
    #########################################################################################################################################################################
    
    tot_events = 0
    #########################################################################################################################################################################
    #########################################################################################################################################################################    
    #########################################################################################################################################################################
    #########################################################################################################################################################################
    active = np.zeros(32)

    sim_real_map = [11,10,9,8,7,6,5,4,3,2,1,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12]

    
    for evtnumber, event in enumerate(T): 
       
        #if evtnumber < 1335:
        #    continue
           
         # LOAD SIMULATION DATA
        # XXX
        # Z -> -Z
        # X -> Y   
       
        bgo_E = event['BGOEnergy']*1.0
        BGOYPos = event['BGOXPos']*1000.0
        BGOXPos = event['BGOYPos']*1000.0
        BGOZPos = event['BGOZPos']*-1000.0
        
        
        #if BGOXPos < 15 or BGOXPos > 25:
        #    continue

        #if BGOYPos < 15 or BGOYPos > 25:
        #    continue        
        
        InnerCharge = np.array(event['ChargeInnerLayer'])[sim_real_map]
        InnerPosY = np.array(event['XPositionInnerLayer'])[sim_real_map]*1000.0
        InnerPosX = np.array(event['YPositionInnerLayer'])[sim_real_map]*1000.0
        InnerPosZ = np.array(event['ZPositionInnerLayer'])[sim_real_map]*-1000.0
        
        
        OuterCharge = np.array(event['ChargeOuterLayer'])[sim_real_map]
        OuterPosY = np.array(event['XPositionOuterLayer'])[sim_real_map]*1000.0
        OuterPosX = np.array(event['YPositionOuterLayer'])[sim_real_map]*1000.0
        OuterPosZ = np.array(event['ZPositionOuterLayer'])[sim_real_map]*-1000.0   

        InnerFibreCharge = np.array(event['ChargeInnerFibre'])
        InnerFibrePosY = np.array(event['XPositionInnerFibre'])*1000.0
        InnerFibrePosX = np.array(event['YPositionInnerFibre'])*1000.0
        InnerFibrePosZ = np.array(event['ZPositionInnerFibre'])*-1000.0  
        
        #print "-----------------", len(InnerFibreCharge)
        
        OuterFibreCharge = np.array(event['ChargeOuterFibre'])
        OuterFibrePosY = np.array(event['XPositionOuterFibre'])*1000.0
        OuterFibrePosX = np.array(event['YPositionOuterFibre'])*1000.0
        OuterFibrePosZ = np.array(event['ZPositionOuterFibre'])*-1000.0   
        
        
        print BGOXPos, BGOYPos, BGOZPos
        
        sims_fibre_inner_hits = np.array([InnerFibrePosX, InnerFibrePosY, InnerFibrePosZ])
        sims_fibre_outer_hits = np.array([OuterFibrePosX, OuterFibrePosY, OuterFibrePosZ])
        sims_BGO_hit = np.array([BGOXPos, BGOYPos, BGOZPos])
      
        for n,i in enumerate(InnerCharge):
            if i < 0.8:
                InnerCharge[n] = 0.0
                
        for n,i in enumerate(OuterCharge):
            if i < 0.8:
                OuterCharge[n] = 0.0  

        #print "-----------------", len(InnerFibreCharge)        
                   
        I = InnerCharge[:]>0.0
        nI = np.sum(I)
        O = OuterCharge[:]>0.0
        nO = np.sum(O)
        
        if nI < 1:
            continue
            
        if bgo_E < 5:
            continue
        
        for n,i in enumerate(InnerFibreCharge):
            if i < 0.5:
                InnerFibreCharge[n] = 0.0
               
        for n,i in enumerate(OuterFibreCharge):
            if i < 0.5:
                OuterFibreCharge[n] = 0.0  


        fI = InnerFibreCharge > 0
        fO = OuterFibreCharge > 0
        
        #print "-----------------", len(InnerFibreCharge)
        
       
        currI = numbers[I]
        currO = numbers[O]
        

        print "##########################################################################################"
        print "########################              NEW EVENT!             #############################"
        print "##########################################################################################"
        print "CUSP, Midas, Event:", cuspRunNumber, midas_run_nr, evtnumber
        event_count_tot = event_count_tot + 1


        print "BGO ENERGY (MeV): ", bgo_E
        print "I:", nI, currI
        print "O:", nO, currO
                
        print "currI : ", currI
        print "currO : ", currO
                

        #currI_uncut = currI
        #currO_uncut = currO
        #####################################################################################################################
        #####################################################################################################################
               
        currI_o, currO_o = is_it_an_orphan(currI, currO) # orphans excluded

        print "currI for tracking (after orphan removing and cuts):", currI_o
        print "currO for tracking (after orphan removing and cuts):", currO_o
        
        number_of_orphans_I = len(currI) - len(currI_o)
        number_of_orphans_O = len(currO) - len(currO_o)
        
        print "number of orphan hits inner: ", number_of_orphans_I
        print "number of orphan hits outer: ", number_of_orphans_O
        
  
        if nI > 0 or nO > 0:
                          
            cornerplotI, cornerplotO = getCornerMap()
            pmapI, pmapO = prepare_PosMap(I,O,nI, nO)
            
            cmap = prepare_CornerMap(I,O, nI, nO)
            #####################################################################################################################
            #####################################################################################################################         
            ynI, clusI = find_consecutive_nr(currI)
            ynO, clusO = find_consecutive_nr(currO)
                        
            clusI = [s for s in clusI if len(s) > 1]
            clusO = [s for s in clusO if len(s) > 1]
            
            #if len(clusI) < 1:
            #    continue
            
            print "Inner cluster: ", clusI
            print "Outer cluster: ", clusO
            #####################################################################################################################
            #####################################################################################################################  """       

        #exit(0)
        print " "
        #####################################################################################################################
        #####################################################################################################################
        
        nr_of_cluster = 999
        cluster_dist = 999
        polygon_hitlist = []
        vertex = [0.0,0.0]
        vertex2 = [0.0,0.0]
        
        print "VEEEEEEEEEERTEX", vertex2
        

        #####################################################################################################################
        #####################################################################################################################

        print "###########################         FIND TRACKS:        ##################################"    

        ######################################################################################
        ######################################################################################  
        plot_trackfinding = [True,True] # points, lines   # save rotation lines and their rotation points (in bgo) for plotting  
        trackcollection = 999
        linepoints = 999
        bgopoints = 999
        
        vertex_BGO = [BGOXPos, BGOYPos, BGOZPos ]

        if BGOon != True:
            tpx_length = 28.0 # mm      
            tpx_hit_bounds = [[-0.5*tpx_length,0.5*tpx_length],[-0.5*tpx_length,0.5*tpx_length]]
            ext = [(0.5*tpx_length, 0.5*tpx_length),  (-0.5*tpx_length, 0.5*tpx_length), (-0.5*tpx_length, -0.5*tpx_length), (0.5*tpx_length, -0.5*tpx_length)]            
            tpx_polygon = Polygon(ext)  
            
            bounds_for_2D_vertex = [-0.5*tpx_length, 0.5*tpx_length] 
               
            vertex2 = [0.0,0.0]
            vertex_err = [tpx_length,tpx_length]
        
        
            if nI > 0 or nO > 0:
                trackcollection, linepoints, bgopoints = track_finding_polygons_tpx(pmapI, cmap, currI, currO, tpx_polygon,
                                                         plot_trackfinding, tpx_hit_bounds, innerCFs, outerCFs, innerzPos, outerzPos) #, xm, ym)
                #hitcollection, linepoints = track_finding_circle(pmapI, cmap, currI, currO, circle, xm, ym)
        else:
            if nI > 0 or nO > 0:
                circle = Circle(xy=(BGOXPos, BGOYPos), radius=20.0,  color = 'white', lw=2, alpha = 0.5)
                
                trackcollection, linepoints, bgopoints, union_cl_I, union_cl_O =  track_finding_circle(pmapI, cmap, currI, currO, circle,BGOXPos, BGOYPos,
                                                         plot_trackfinding, InnerPosZ, OuterPosZ) #, xm, ym)
                

        
        

        nr_of_fit_lines_ = 999
        mean_d_pt_ = [999,999,999]
        av_d_pt_ = [999,999,999]
        n_cluster_ = 999
        biggest_cluster_ = [999,999,999,999,999]
        
        
        if fibrebool == True and nI > 1 and nO > 1:
            #innerzPos = -innerzPos
            #outerzPos = -outerzPos
                           
            zerrI = 59 # 2 sigma #  70.0*0.5   # half of the FWMH
            #zerrI = 70 # FWHM
            zerrO = 73 # 2 sigma # 70.0*0.5
            #zerrO = 73 # FWHM
            
            #print InnerFibreCharge
   
            nr_of_fit_lines_, mean_d_pt_, av_d_pt_, n_cluster_, biggest_cluster_ = track_finding_4layers(polygon_hitlist, dummy_hodor,
                                                                                         nO, nI, I, O,
                                                                                         InnerFibreCharge, OuterFibreCharge,
                                                                                         zerrI, zerrO, cuspRunNumber, evtnumber,
                                                                                         circle, union_cl_I, union_cl_O, vertex_BGO,
                                                                                         sims_fibre_inner_hits, sims_fibre_outer_hits, sims_BGO_hit,
                                                                                         InnerPosX,InnerPosY,InnerPosZ,
                                                                                         OuterPosX,OuterPosY,OuterPosZ)
                                                                                         
         
            #plt.show()                                                                            
            
            print "BIGGEST CLUSTER", biggest_cluster_
            print "BIGGEST CLUSTER", biggest_cluster_[0]
            print "BIGGEST CLUSTER", biggest_cluster_[1]


        print "Final Hitcollection:      ################################################################" 
        
        if trackcollection != 999:
            nr_of_tracks = len(trackcollection)
        
            if nr_of_tracks > 0:
                for i in trackcollection:
                    print i
                       
                print " "
        else:
            nr_of_tracks = 0
        print "##############################        Fitting 2D:       ##################################"
        print "Fitting", nr_of_tracks, "tracks "
        line_params = []
        #angle1 = 999
        angle = 999
        vertex = [999,999]
               
        vertex_err = [50,50]
        bounds_for_2D_vertex = [-0.5*50, 0.5*50] 
        
        #angle = do_le_anglecalc(trackcollection, I, O, xm, ym, chargeI, chargeO, bgo_E)
        if nr_of_tracks > 1:
            line_params, fitlines_points, trackscoll = do_le_2Dfitting_sims(trackcollection, I, O, vertex_BGO, vertex_err, InnerCharge, OuterCharge) 
        
        print "#######################        VERTEX 2D through fitting:       ##########################"
        
        
        angle1 = 999
        angle2 = 999
        angle3 = 999
        angleY = 999
        mean_angleY = 999 
        orientatio = [999,999,999]
        mtd3 = 999
        mtd_min_max = 999
        fitlines_points = 999
        line_paras = 999
        vertex_points = 999
        vertex_2d_lines = [999,999]
                    
        #vertex_points = 999
        """if nr_of_tracks > 1:
            vertex_points, vertex_2d_lines = determine_vertex(line_params, bounds_for_2D_vertex) # vertex is the arithm mean of vertex points

        if math.isnan(vertex[0]) or math.isnan(vertex[1]):
            vertex_2d_lines = [999,999]
        
        #mtds2, angle2 = calc_mtds_of_tracks_corr(trackcollection, innerCFs, outerCFs, innerzPos, outerzPos, vertex2) #, rinner, router)
        
        
        
        print "Vertex: ", vertex_2d_lines"""
        
    
        
        
        if nr_of_tracks == 1:
            angleY, orientatio = calc_angle_for_1track(trackcollection, vertex2)
            
            
        #orientatio = None
        """if nr_of_tracks > 1:
            mtd3, angle_all, angleY, mtd_min_max, orientatio = calc_mtds_of_tracks_corr_std(trackcollection, innerCFs, outerCFs, innerzPos, outerzPos, vertex2) #, rinner, router)
            print "all", angle_all    
            angle1 = np.amax(angle_all)
            print "ang1", angle1
            
            angle_all = np.setdiff1d(angle_all,angle1)
            print angle_all
            angle2 = 0
            angle3 = 0        
            
            if nr_of_tracks  == 2:
                angle = angle1
            
            if nr_of_tracks > 2:
                angle2 = np.amax(angle_all)
                angle_all = np.setdiff1d(angle_all,angle2)
                if len(angle_all) > 1:
                    angle3 = np.amax(angle_all)
                else:
                    angle3 = angle2
                    angle2 = angle1
                    #angle3 = angle1
                #angle_all = np.setdiff1d(angle_all,angle3)
                    
        mean_angleY = np.mean(np.array(angleY))    """           
        
        if simplecuts == True:
            if nr_of_tracks < 2:
                continue
                
            if bgo_E < BGOcut:
                continue
                              
            if nr_of_tracks == 2 and angle1 > 160:
                continue
                    
            if mtd > 0.4:
                continue 
       
        print "------------------------------------------------------------------------------------------"
        print "ANGLE: ", angle1, angle2, angle3, "degree" 
        print "Y ANGLE: ", angleY, "degree, mean: ", mean_angleY
        
        print "------------------------------------------------------------------------------------------"
        
        if orientatio:
            print "Orientation: ", orientatio

        #pval_c, pval_p = calc_pvals_mtds(mtd_combi)
            
        """if plotyes == True: # and timestampx < 0.0219999 and timestampx > 0.021000:

                bgopoints = np.array(bgopoints)
                plot_all_bars_2D(circle, union_cl_I, union_cl_O, linepoints, trackcollection, line_params, vertex_BGO) 
                
                plt.show()  """  
        
        if writeTxt == True:
        
            #for nb in noise_bars_O:
        
                    #file_mtds.write("%s " % nb)
                    #file_mtds.write("%s " % h.o.CFU[nb])
                    #file_mtds.write("%s " % h.o.CFD[nb])
                    #file_mtds.write("%s " % h.o.AmpU[nb])
                    #file_mtds.write("%s " % h.o.AmpD[nb])
                    file_mtds.write("%s " % cuspRunNumber)
                    file_mtds.write("%s " % midas_run_nr)
                    file_mtds.write("%s " % evtnumber)
                    file_mtds.write("%s " % bgo_E)    
                    file_mtds.write("%s " % nI)    
                    file_mtds.write("%s " % nO)                    
                    file_mtds.write("%s " % nr_of_tracks)  
                    file_mtds.write("%s " % angle1)
                    file_mtds.write("%s " % angle2)
                    file_mtds.write("%s " % angle3)
                    file_mtds.write("%s " % mean_angleY)
                    file_mtds.write("%s " % orientatio[0])
                    file_mtds.write("%s " % orientatio[1])
                    file_mtds.write("%s " % orientatio[2])               
                    file_mtds.write("%s " % tot_events) # 14
          
                    file_mtds.write("%s " % mtd3)
                    file_mtds.write("%s " % mtd_min_max)
                    
                    file_mtds.write("%s " % vertex_2d_lines[0])
                    file_mtds.write("%s " % vertex_2d_lines[1])

                    file_mtds.write("%s " % vertex2[0])
                    file_mtds.write("%s " % vertex2[1])
                    
                    
                    file_mtds.write("%s " % nr_of_cluster) # 21
                    file_mtds.write("%s " % cluster_dist)
                    file_mtds.write("%s " % 999)
                    file_mtds.write("%s " % 999)
                    file_mtds.write("%s " % number_of_orphans_I)
                    file_mtds.write("%s " % number_of_orphans_O)
                    
                    file_mtds.write("%s " % nr_of_fit_lines_) # 27
                    
                    file_mtds.write("%s " % mean_d_pt_[0]) # 28
                    file_mtds.write("%s " % mean_d_pt_[1])
                    file_mtds.write("%s " % mean_d_pt_[2]) 
                    
                    file_mtds.write("%s " % av_d_pt_[0]) # 31
                    file_mtds.write("%s " % av_d_pt_[1])
                    file_mtds.write("%s " % av_d_pt_[2]) 
                    
                    file_mtds.write("%s " % n_cluster_) 
                    file_mtds.write("%s " % biggest_cluster_[2])  # 35
                    file_mtds.write("%s " % biggest_cluster_[3])
                    file_mtds.write("%s " % biggest_cluster_[4]) 
                    
                    file_mtds.write("%s " % biggest_cluster_[0]) # 38
                    file_mtds.write("%s " % biggest_cluster_[1]) 

                    file_mtds.write("%s " % BGOXPos) # 40
                    file_mtds.write("%s " % BGOYPos)  
                    file_mtds.write("%s " % BGOZPos)                   
                                                             
                    if writeTxt == True:    
                            file_mtds.write("%s " % 999) 
                            file_mtds.write("\n")
                            file_mtds.flush()   
            
                
        """cand_list.append([cuspRunNumber, evtnumber, bgo_E, nI_all, nO_all, nr_of_tracks, timestampx, 0.0, mtd3, mtd_min_max, angle1, angle2, angle3,
        vertex[0], vertex[1], vertex2[0], vertex2[1], nr_of_cluster, cluster_dist, mean_angleY, orientatio[0], orientatio[1], orientatio[2], number_of_noise_hits_I,
        number_of_noise_hits_O, number_of_orphans_I, number_of_orphans_O])"""
              
        print "------------------------------------------------------------------------------------------"    
        print "Total events in run:", tot_events #, timestampx
        print "------------------------------------------------------------------------------------------"
            
    ########################################################################################################################################################################
    ############################################################################################################################################################################ 
    ############################################################################################################################################################################
    ############################################################################################################################################################################ 
    # write ROOT files if ordered
    for what in cand_list:
            print what
            
    if writeROOT and len(cand_list) > 0:
        from rootpy.tree import Tree
        from rootpy.io import root_open

        filename = "./rootfiles_hbarcands/{}.root".format(cuspRunNumber)

        f = root_open(filename, "recreate")

        outT = Tree("candidates")

        outT.create_branches(
            {'cuspRunNumber': 'I',
             'midasRunNumber': 'I',
             'eventNumber': 'I',
             'bgoEdep': 'D',
             'hitsInner': 'I',
             'hitsOuter': 'I',
             'NrofTracks': 'I',
             'startMix': 'D',
             'time': 'D',
             #'MTDCombi': 'D',
             'MTDStd': 'D',
             'MTDMinMax': 'D',
             'Angle1': 'D',
             'Angle2': 'D',
             'Angle3': 'D',
             'Vertexx': 'D',
             'Vertexy': 'D',
             'Vertexx2': 'D',
             'Vertexy2': 'D',
             'NrCluster' : 'D',
             'ClDist' : 'D',
             'meanAngleY' : 'D',
             'Nup' : 'I',             
             'Ndown' : 'I',
             'Nhoriz' : 'I',
             'NoiseHI' : 'I',
             'NoiseHO' : 'I',
             'OrphHI' : 'I',
             'OrphHO' : 'I'             
             })


        
        for n,i in enumerate(cand_list):
                outT.cuspRunNumber = cuspRunNumber
                outT.midasRunNumber = 999
                outT.eventNumber = i[1]
                outT.bgoEdep = i[2]
                outT.hitsInner = i[3]
                outT.hitsOuter = i[4]
                outT.NrofTracks = i[5]
                outT.startMix = startmixAdc
                outT.time = i[6]
               
                #outT.MTDCombi = i[7]
                outT.MTDStd= i[8]
                outT.MTDMinMax = i[9]
                
                outT.Angle1 = i[10]
                outT.Angle2 = i[11]
                outT.Angle3 = i[12]
                outT.Vertexx = i[13]
                outT.Vertexy = i[14]
                outT.Vertexx2 = i[15]
                outT.Vertexy2 = i[16]
                outT.NrCluster = i[17]
                outT.ClDist = i[18]
                outT.meanAngleY = i[19]
                outT.Nup = i[20]       
                outT.Ndown = i[21]
                outT.Nhoriz =i[22]
                outT.NoiseHI =i[23]
                outT.NoiseHO =i[24]
                outT.OrphHI =i[25]
                outT.OrphHO = i[26]

                outT.fill()
        outT.write()
        f.close()
