#!/bin/bash


python tracking_simulations_3d_2019_hbar_gshfs_molybdenum_1_ftf.py ./rootfiles/converted_g4_ftf_molybdenum_1.root > logfile_molybdenum_ftf_1.dat

python tracking_simulations_3d_2019_hbar_gshfs_molybdenum_2_ftf.py ./rootfiles/converted_g4_ftf_molybdenum_2.root > logfile_molybdenum_ftf_2.dat

python tracking_simulations_3d_2019_hbar_gshfs_molybdenum_3_ftf.py ./rootfiles/converted_g4_ftf_molybdenum_3.root > logfile_molybdenum_ftf_3.dat

python tracking_simulations_3d_2019_hbar_gshfs_molybdenum_4_ftf.py ./rootfiles/converted_g4_ftf_molybdenum_4.root > logfile_molybdenum_ftf_4.dat


    

