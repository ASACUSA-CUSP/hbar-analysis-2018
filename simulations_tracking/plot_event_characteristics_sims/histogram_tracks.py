#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':

    print "HI!"
        
    pbardata_orig = np.loadtxt("../training_data/all_pbars_data_2016_yangle.dat")
    cosmicdata_orig = np.loadtxt("../training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")

    #midas = np.unique(cosmicdata_orig[:,1])   
    #print midas    
    #print midas.shape    
    #exit(0)
       
    pbardata_orig = pbardata_orig[pbardata_orig[:,28] > - 13]  # four seconds, see pbars_arriving.pdf
    pbardata_orig = pbardata_orig[pbardata_orig[:,28] < - 9]
    #print "pbar events after cut on timestamp: ", pbardata.shape
    pbardata_orig = pbardata_orig[pbardata_orig[:,22] <= 1] # no pbar hits with several hits!
    
    
    bool_cut_vertex = True
    
    #if bool_cut_vertex == True:
    #    pbardata_orig, pbardata_out_orig = cut_on_vertex(pbardata_orig)
   
    all_actual_cosmic = 282935.0 # half break 1... hodoscope+BGO trigger
    cosmic_time = 603659.0 # in seconds, half break 1... hodoscope+BGO trigger
    #all_actual_cosmic = 314776.0 # half break 2... BGO trigger
    #cosmic_time = 197074.0 # half second break
    
    pbar_time = 4*(102.0 - 9.0) # 4 seconds, 102 runs, 9 empty
    area_hit = math.pi*45.0*0.5*25.0*0.5
    area_BGO = math.pi*45*45 #- area_hit
    
    
    print "all in file cosmic: ", len(cosmicdata_orig), all_actual_cosmic
    print "all in file pbar: ", len(pbardata_orig)
    
    BGO_cut = 0
    Track_cut = 0
   
    cosmicdata_orig = cosmicdata_orig[cosmicdata_orig[:,3]>=BGO_cut]
    cosmicdata_orig = cosmicdata_orig[cosmicdata_orig[:,6]>=Track_cut]
    
    pbardata_orig = pbardata_orig[pbardata_orig[:,3]>=BGO_cut]
    pbardata_orig = pbardata_orig[pbardata_orig[:,6]>=Track_cut]
     
    #scan_E_cut(cosmicdata_orig, pbardata_orig)
 
    estimated_cosmics = len(cosmicdata_orig)*1.0/cosmic_time*pbar_time # scale to pbar time
    # estimated_cosmics = estimated_cosmics*area_hit/area_BGO # scale to hit area
    
    print "estimated cosmics scaled to pbar time and hit area", estimated_cosmics
    
    print "estimated cosmics in test set (1/3)", estimated_cosmics*0.33
    
    
    fig, ax = plt.subplots()
    
    range1 = 0.5
    range2 = 8.5
    binnumber = 8
    
    ax.set_xlim([range1, 9])
    
    tr_pbar_plot = pbardata_orig[:,5]
    tr_cosmic_plot = cosmicdata_orig[:,5]
    
    y, bin_edges = np.histogram(tr_pbar_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm1 = 1.0*sum(np.diff(bin_edges)*y)
    
    err1 = np.sqrt(y)/norm1
    y = y/norm1
            
    y2, bin_edges = np.histogram(tr_cosmic_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm2 = 1.0*sum(np.diff(bin_edges)*y2)
    
    err2 = np.sqrt(y2)/norm2
    y2 = y2/norm2   
    
    index = np.arange(9)
    bar_width = 0.35
    error_config = {'ecolor': '0.3'}
    
    rects1 = plt.bar(bin_centers, y2, bar_width,
                 color=cosmiccolor,
                 yerr=err2,
                 #error_kw=error_config,
                 label='cosmic')

    rects2 = plt.bar(bin_centers + bar_width, y, bar_width,                 
                 color=pbarcolor,
                 yerr=err1,
                 #error_kw=error_config,
                 label='$\overline{p}$')
    
    
    plt.xticks(index + bar_width / 2, ('0', '1', '2', '3', '4', '5', '6', '7', '8'))
    
    # compare pbar and cosmics: ##########################################################################################
    #n, bins, patches = ax.hist( [tr_pbar_plot, tr_cosmic_plot], yerr= 1, histtype='bar',
    #    bins = binnumber, range=(range1, range2), align='mid', label = ["$\overline{p}$","cosmics"], normed = True, color = [pbarcolor, cosmiccolor]) 
    ######################################################################################################################
    
    ax.legend()
    
    plt.xlabel("$n_O$")
    plt.ylabel("norm. counts")
    
    ##################################################################################################################
    
    
    plt.savefig('number_of_outer_hits.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

