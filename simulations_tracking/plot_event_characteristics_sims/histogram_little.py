#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

from sys           import argv



import re
import math

import matplotlib.pyplot as plt

#################################################################################################################################


if __name__ == '__main__':

    if len(argv) < 2:
        print "usage: ", argv[0], "txt-file"
        exit(-1)

    print "HI!"

    data = np.loadtxt(argv[1])
    

    data = data[data[:,0]==-1]
    data1 = data[data[:,1] < -0.08]
    data2 = data[data[:,1] < -0.1]
    data3 = data[data[:,1] < -0.15]
    data4 = data[data[:,1] < -0.2]
    
    datac = data[data[:,0]==1]
    data1c = data[data[:,1] > 0.08]
    data2c = data[data[:,1] > 0.1]
    data3c = data[data[:,1] > 0.15]
    data4c = data[data[:,1] > 0.2]
    
        
    
    print len(data)
    
    fig, ax = plt.subplots()
    fig2, ax2 = plt.subplots()
    ######################################################################################################################################################################
    ######################################################################################################################################################################            
    ######################################################################################################################################################################
    ######################################################################################################################################################################
    range1 = 0
    range2 = 25      
    r = np.array([range1,range2])
    
    for nr, d in enumerate([data,data1,data2,data3,data4]):
        histc, edgesc = np.histogram(a=(np.array(d[:,29])), bins=25, range=r)   
        bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.    
        ax.errorbar(bincentersc, histc, label = '{0}'.format(nr))#
        
  
    ax.legend() 
    ax.set_ylim(0,)
    ax.set_xlim(range1,range2)
    
    for nr, d in enumerate([datac,data1c,data2c,data3c,data4c]):
        histc, edgesc = np.histogram(a=(np.array(d[:,29])), bins=25, range=r)   
        bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.    
        ax2.errorbar(bincentersc, histc, label = '{0}'.format(nr))#
        
  
    ax2.legend() 
    ax2.set_ylim(0,)
    ax2.set_xlim(range1,range2)
    
    plt.show()            
    
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
    
        #thefile.close()   

