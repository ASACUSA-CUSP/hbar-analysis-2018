#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import os
import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':
    
    pbardata_orig0 = [] 
    pbardata_orig1 = [] 
    
    pbar0 = "molybdenum"
    pbar1 = "carbon"
    pbar2 = "gold"
    
    sims_ = "ftfp_bert"
    #sims_ = "qgsp_bert_chips"
    #sims_ = "ftf_bic"
    
   
    #pbardata_orig0 = np.loadtxt("../data_files_results/tracking_results_g4_"+ sims_ +"_" + pbar0 + "_0_foilpos-26_8mm_detailed_tpx_newest.dat")
    #pbardata_orig1 = np.loadtxt("../data_files_results/tracking_results_g4_"+ sims_ +"_" + pbar1 + "_0_foilpos-26_8mm_detailed_tpx_newest.dat")
    #pbardata_orig2 = np.loadtxt("../data_files_results/tracking_results_g4_"+ sims_ +"_" + pbar2 + "_0_foilpos-26_8mm_detailed_tpx_newest.dat")   
           
    #pbardata_orig0 = np.loadtxt("../data_files_results/g4_50000_"+sims_+"/tracking_results_g4_"+ sims_ +"_" + pbar0 + "_all.dat")
    #pbardata_orig1 = np.loadtxt("../data_files_results/g4_50000_"+sims_+"/tracking_results_g4_"+ sims_ +"_" + pbar1 + "_all.dat")
    #pbardata_orig2 = np.loadtxt("../data_files_results/g4_50000_"+sims_+"/tracking_results_g4_"+ sims_ +"_" + pbar2 + "_all.dat")   

    pbardata_orig0 = np.loadtxt("../all_tracking_results_converted_g4_"+ sims_ +"_" + pbar0 + "_foilpos-26_8mm_all.dat")
    pbardata_orig1 = np.loadtxt("../all_tracking_results_converted_g4_"+ sims_ +"_" + pbar1 + "_foilpos-26_8mm_all.dat")
    pbardata_orig2 = np.loadtxt("../all_tracking_results_converted_g4_"+ sims_ +"_" + pbar2 + "_foilpos-26_8mm_all.dat")
    
           
    
    #BGO_cut = 0
    Track_cut = 0   
    pbardata_orig1 = pbardata_orig1[pbardata_orig1[:,4]>=Track_cut]
    pbardata_orig0 = pbardata_orig0[pbardata_orig0[:,4]>=Track_cut]
    pbardata_orig2 = pbardata_orig2[pbardata_orig2[:,4]>=Track_cut]
    
    
    
    pbardata_orig1 = pbardata_orig1[pbardata_orig1[:,3]>0]
    pbardata_orig0 = pbardata_orig0[pbardata_orig0[:,3]>0]
    pbardata_orig2 = pbardata_orig2[pbardata_orig2[:,3]>0]
    
    print "Number of events: ", len(pbardata_orig0), len(pbardata_orig1), len(pbardata_orig2)
    
    fig, ax = plt.subplots()
    
    range1 = -0.5
    range2 = 7.5
    binnumber = int(abs(range1) + abs(range2))
    
    ax.set_xlim([range1, range2 + 0.5])
    
    tr_pbar0_plot = pbardata_orig0[:,27] # 27
    tr_pbar1_plot = pbardata_orig1[:,27]
    tr_pbar2_plot = pbardata_orig2[:,27]
    
    y0, bin_edges = np.histogram(tr_pbar0_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm1 = 1.0*sum(np.diff(bin_edges)*y0)    
    err1 = np.sqrt(y0)/norm1
    y0 = y0/norm1
            
    y1, bin_edges = np.histogram(tr_pbar1_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm2 = 1.0*sum(np.diff(bin_edges)*y1)   
    err2 = np.sqrt(y1)/norm2
    y1 = y1/norm2   

    y2, bin_edges = np.histogram(tr_pbar2_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm3 = 1.0*sum(np.diff(bin_edges)*y2)   
    err3 = np.sqrt(y2)/norm3
    y2 = y2/norm3
    
    index = np.arange(range2 - 0.5)
    bar_width = 0.25
    error_config = {'ecolor': '0.3'}
    
    if pbar0 == "carbon":
        label0 = '$\overline{p}$ C'
        color0 = "dimgray"
    elif pbar0 == "molybdenum":
        label0 = '$\overline{p}$ Mo'
        color0 = "silver"
    elif pbar0 == "gold":
        label0 = '$\overline{p}$ Au'
        color0 = "goldenrod"
        
    if pbar1 == "carbon":
        label1 = '$\overline{p}$ C'
        color1 = "dimgray"
    elif pbar1 == "molybdenum":
        label1 = '$\overline{p}$ Mo'
        color1 = "silver"
    elif pbar1 == "gold":
        label1 = '$\overline{p}$ Au'
        color1 = "goldenrod"   

    if pbar2 == "carbon":
        label2 = '$\overline{p}$ C'
        color2 = "dimgray"
    elif pbar2 == "molybdenum":
        label2 = '$\overline{p}$ Mo'
        color2 = "silver"
    elif pbar2 == "gold":
        label2 = '$\overline{p}$ Au'
        color2 = "goldenrod"           
            
    rects1 = plt.bar(bin_centers - bar_width, y1, bar_width,
                 color=color1,
                 yerr=err2,
                 #error_kw=error_config,
                 label=label1 )#"""

    rects2 = plt.bar(bin_centers , y0, bar_width,                 
                 color=color0,
                 yerr=err1,
                 #error_kw=error_config,
                 label=label0 )

    rects3 = plt.bar(bin_centers + bar_width, y2, bar_width,                 
                 color=color2,
                 yerr=err3,
                 #error_kw=error_config,
                 label=label2 )    
    
    plt.xticks(index + bar_width / 2, ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'))
    
    #if sims_ == "chips":
    plt.title(sims_)

    #if sims_ == "ftf":
     
    ax.legend()
    
    plt.xlabel("number of 3D tracks")
    plt.ylabel("norm. counts")
    
    ##################################################################################################################
    
    
    plt.savefig('number_of_tracks_tpx_pbars_sims_' + sims_ + '_0.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

