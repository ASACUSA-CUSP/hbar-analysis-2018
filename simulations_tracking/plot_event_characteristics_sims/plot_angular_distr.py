#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.dates as mdates

from matplotlib.cm import coolwarm

from uncertainties import unumpy

from src.bgo       import *
from src.hodoscope import *

def loadMeasuredData(T):
    correctionData = readTimingCorrectionData("../calibrationData/hodoscopeTimingCorrectionTable.dat")
    bgoCalibData = readBGOCalibrationData("../calibrationData/gainBGOcalibration.dat")
    hododata = []
    for event in T:
        h = hodoscope(event)
        b = BGO(event, bgoCalibData)
        nI, nO, I, O  = h.getActiveBar()
        tmp = [b.getCharge()]
        tmp.extend(I.tolist())
        tmp.extend(O.tolist())
        hododata.append(tmp)

    return np.array(hododata)

def cutMeasData(data, energyCutBGO):
    BGOCut        = data[:,0]>energyCutBGO
    hodoscopeCuts = data[BGOCut,1:]
    return hodoscopeCuts

def prepareData(hodoMeas, hodoSimul, BGOEnergy, SimulHodoEnergyInner, SimulHodoEnergyOuter):
    SimulHodo = cutSimulData(hodoSimul, BGOEnergy, SimulHodoEnergyInner, SimulHodoEnergyOuter)
    MeasuHodo = cutMeasData(hodoMeas, BGOEnergy)
    numInner = [np.sum(MeasuHodo[:,:32],axis=1),np.sum(SimulHodo[:,:32],axis=1)]
    numOuter = [np.sum(MeasuHodo[:,32:],axis=1),np.sum(SimulHodo[:,32:],axis=1)]
    return numInner, numOuter


def prpareAngles(hodoMeas, BGOEnergy):
    #SimulHodo = cutSimulData(hodoSimul, BGOEnergy, SimulHodoEnergyInner, SimulHodoEnergyOuter)
    MeasuHodo = cutMeasData(hodoMeas, BGOEnergy)

    innerRaw    = np.sum(MeasuHodo[:,:32],axis=0)
    outerRaw    = np.sum(MeasuHodo[:,32:],axis=0)
    #innerRawSim = np.sum(SimulHodo[:,:32],axis=0)
    #outerRawSim = np.sum(SimulHodo[:,32:],axis=0)

    inner    = unumpy.uarray(innerRaw,    np.sqrt(innerRaw)   )/np.sum(innerRaw)
    outer    = unumpy.uarray(outerRaw,    np.sqrt(outerRaw)   )/np.sum(outerRaw)
    #innerSim = unumpy.uarray(innerRawSim, np.sqrt(innerRawSim))/np.sum(innerRawSim)
    #outerSim = unumpy.uarray(outerRawSim, np.sqrt(outerRawSim))/np.sum(outerRawSim)
    return inner, outer

def plotAngularDistribution(hodoMeas, BGOEnergy):
    inner, outer = prpareAngles(hodoMeas, BGOEnergy)

    fig, ax = plt.subplots(figsize = (10,4))
    fig.subplots_adjust(hspace=0.0,wspace=0)
    fig.patch.set_alpha(0.0)


    bluec = (36.0/255,75.0/255,166.0/255)
    redc = (176.0/255,70.0/255,35.0/255) 

    ax.set_xlim(-0.5,31.5)
    ax.errorbar(np.arange(0,32), unumpy.nominal_values(inner), label="inner", fmt='o', yerr=unumpy.std_devs(inner), ms=5., alpha = 0.5, color = redc)
    ax.errorbar(np.arange(0,32), unumpy.nominal_values(outer), label="outer", fmt='o', yerr=unumpy.std_devs(outer), ms=5., alpha = 0.5, color =  bluec)
    ax.set_ylabel('norm. counts', fontsize = 20)
    ax.legend(loc='best', fancybox=True, framealpha=0.5, fontsize = 20)

    ax.set_xlabel('hodoscope bar', fontsize = 20)

    plt.tight_layout()
    fig.subplots_adjust(hspace=0.1,wspace=0)

    """for child in ax[0].get_children():
        if isinstance(child, matplotlib.spines.Spine):
            child.set_color('#dddddd')
    for child in ax[1].get_children():
        if isinstance(child, matplotlib.spines.Spine):
            child.set_color('#dddddd')
    for child in ax[2].get_children():
        if isinstance(child, matplotlib.spines.Spine):
            child.set_color('#dddddd')"""

    name = 'cosmic_angularDist-{0}MeV'.format(BGOEnergy)
    plt.savefig(name+'.pdf', facecolor=fig.get_facecolor())
    #plt.savefig(name+'.svg', facecolor=fig.get_facecolor())
    plt.close('all')


if __name__ == '__main__':

    if len(argv) < 2:
        print "usage: ", argv[0], "rootfile1.root rootfile2.root ... rootfileN.root  "
        exit(-1)
    T = TreeChain("HbarEventTree", argv[1:])
    
    hodosMeasured = loadMeasuredData(T)
    
    del T

    BGOCut = [0]

    for i in BGOCut:
        
        plotAngularDistribution(     hodosMeasured, i)
     
        
        
        
        
        
        
        
        
