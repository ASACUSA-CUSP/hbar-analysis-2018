#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 


import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001'
cosmiccolor = '#51989f'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':

    print "HI!"
        
    pbardata_orig = []
        
    for n, filei in enumerate(argv[1:]):
        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_orig = data_orig
        else: 
            pbardata_orig = np.vstack((pbardata_orig, data_orig))
    
    ##############################################################################################
    
    #tr_pbar_plot_3D_v = pbardata_orig[:,56]
    #tr_pbar_plot_3D_v[tr_pbar_plot_3D_v == 999] = 0
       
    fig, ax = plt.subplots()
    
    range1 = -0.5
    range2 = 6.5
    binnumber = 7
    
    ax.set_xlim([range1, 7])
    
    tr_pbar_plot_2D = pbardata_orig[:,6]
    tr_pbar_plot_2D[tr_pbar_plot_2D == 999] = 0
    tr_pbar_plot_3D = pbardata_orig[:,27]
    tr_pbar_plot_3D[tr_pbar_plot_3D == 999] = 0
    
    y, bin_edges = np.histogram(tr_pbar_plot_2D, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm1 = 1.0*sum(np.diff(bin_edges)*y)
    
    err1 = np.sqrt(y)/norm1
    y = y/norm1
            
    y2, bin_edges = np.histogram(tr_pbar_plot_3D, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm2 = 1.0*sum(np.diff(bin_edges)*y2)
    
    err2 = np.sqrt(y2)/norm2
    y2 = y2/norm2   
    
    index = np.arange(7)
    bar_width = 0.35
    error_config = {'ecolor': '0.3'}
    
    rects1 = plt.bar(bin_centers + bar_width*0.5, y2, bar_width,
                 color="seagreen",
                 yerr=err2,
                 alpha = 0.7,
                 #error_kw=error_config,
                 label='$\overline{p}$ 3D')

    rects2 = plt.bar(bin_centers -  bar_width*0.5, y, bar_width,                 
                 color=pbarcolor,
                 yerr=err1,
                 alpha = 0.7,
                 #error_kw=error_config,
                 label='$\overline{p}$ 2D')
    
    
    plt.xticks(index + bar_width / 2, ('0', '1', '2', '3', '4', '5', '6'))
    
    # compare pbar and cosmics: ##########################################################################################
    #n, bins, patches = ax.hist( [tr_pbar_plot, tr_cosmic_plot], yerr= 1, histtype='bar',
    #    bins = binnumber, range=(range1, range2), align='mid', label = ["$\overline{p}$","cosmics"], normed = True, color = [pbarcolor, cosmiccolor]) 
    ######################################################################################################################
    
    ax.legend()
    
    plt.xlabel("number of 2D tracks")
    plt.ylabel("norm. counts")
    
    ##################################################################################################################
    
    
    plt.savefig('number_of_tracks_tpx_pbars.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

