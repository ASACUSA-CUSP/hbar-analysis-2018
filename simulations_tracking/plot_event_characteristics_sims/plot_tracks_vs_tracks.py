#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

from matplotlib.cm import gray_r, gray, bone, coolwarm, jet, rainbow, gnuplot, gnuplot2, gist_stern, ocean, cubehelix,flag, RdGy, BuPu, seismic, terrain, gist_stern, terrain_r


pbarcolor = '#990001'
cosmiccolor = '#51989f'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')

#################################################################################################################################

if __name__ == '__main__':

    print "HI!"
        
    #cosmicdata_orig = np.loadtxt("../test_cosmic_ceil.dat")
    #pbardata_orig = np.loadtxt("lol.dat")
    
    #pbardata_orig = np.loadtxt("./dat_files_3D/test_pbars_0_0_firstpos_gauss_oct2_covarweight_testcut_fifthFWHM.dat")
    #pbardata_orig = np.loadtxt("./dat_files_3D/test_cosmics_8to14_covarweight_testcut.dat")
    #pbardata_orig = np.loadtxt("./dat_files_3D/test_pbars_intowall_189_firstpos_gauss_oct2_covarweight_testcut.dat")
    #pbardata_orig = np.loadtxt("/home/bernadette/Dropbox/machine_learning/post_processing_2018/xgb_events_cands_tpe_new0_str30_mean_score_200_std_norm_2018.dat")
   
    pbardata_orig = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts_final/timepix_17_december_pbar_all_smaller_errors_eps90_2.dat")
    #sims = np.loadtxt("./particles_in_dets/number_tracks_pbar_ecut_2.dat")
    
    #pbardata_orig = np.loadtxt("../vertex_pbars_based_on_sims_midas681.dat")
    
    #print "all in file cosmic: ", len(cosmicdata_orig)
    print "all in file pbar: ", len(pbardata_orig)
    
    fig, ax = plt.subplots()
    
    #pbardata_orig = pbardata_orig[pbardata_orig[:,15] < 0.4]
    
    
    
    print "after cut in file pbar: ", len(pbardata_orig)
    
    range1 = -0.5
    range2 = 8.5
    binnumber = 9
    
    ax.set_xlim([range1, 9])
    
    tr_pbar_plot_2D = pbardata_orig[:,6]
    #tr_cosmic_plot_2D = cosmicdata_orig[:,6]
    
    tr_pbar_plot_3D = pbardata_orig[:,27]
    tr_pbar_plot_3D[tr_pbar_plot_3D == 999] = 0
    
    tr_pbar_plot_3D_v = pbardata_orig[:,52]
    tr_pbar_plot_3D_v[tr_pbar_plot_3D_v == 999] = 0
    #tr_cosmic_plot_3D = cosmicdata_orig[:,27] 
    
    range1 = -0.5
    range2 = 8.5
    bins = 9
    
    ax.set_xlim([range1, range2])
    from matplotlib.colors import LogNorm
    hist3D, xbins, ybins = np.histogram2d(tr_pbar_plot_3D, tr_pbar_plot_3D_v, bins=[bins,bins], range = [[range1,range2], [range1,range2]])#, normed = True)
    extent = [xbins.min(),xbins.max(),ybins.min(),ybins.max()]
    
    print hist3D.T
    
    diag = hist3D.diagonal()
    
    for i in np.arange(0,binnumber):
    
        hist3D[i,:] = hist3D[i,:]/np.sum(hist3D[i,:])
    
    print diag # """
    
    
    values = hist3D.T
    size = binnumber
    x_start = xbins.min()
    x_end = xbins.max()
    y_start = ybins.min()
    y_end = ybins.max()    #extent = [x_start, x_end, y_start, y_end]
    # Add the text
    jump_x = (x_end - x_start) / (2.0 * size)
    jump_y = (y_end - y_start) / (2.0 * size)
    x_positions = np.linspace(start=x_start, stop=x_end, num=size, endpoint=False)
    y_positions = np.linspace(start=y_start, stop=y_end, num=size, endpoint=False)

    for y_index, y in enumerate(y_positions):
        for x_index, x in enumerate(x_positions):
            label = values[y_index, x_index]
            text_x = x + jump_x
            text_y = y + jump_y
            if label > 0.0 and text_x <= 6 and text_x > 0 and text_y > 0:
                ax.text(text_x, text_y, "{:.2f}".format(label), color='black', ha='center', va='center')

    
    
    im =  ax.imshow(hist3D.T, interpolation='none', origin='lower', extent=extent,  cmap = coolwarm, aspect='equal', label = "3D")#, norm=LogNorm())
  
    #pcol = plt.pcolormesh(xbins, ybins, hist3D.T, cmap=gnuplot2, rasterized=True, linewidth=0)#,vmin=0, vmax=20)
    cbar = plt.colorbar(im)   
    
   
    ax.set_aspect('equal')
    ax.set_xlim([0.5, 6.5])
    ax.set_ylim([0.5, 6.5])    
    #ax.scatter(tr_pbar_plot_3D, tr_pbar_plot_3D_v)
    
    ax.set_ylabel("tracks in vertex", fontsize = 12) 
    ax.set_xlabel("total tracks found by algorithm", fontsize = 12) 
    plt.tight_layout()
    plt.savefig('tracks_vs_tracks_data.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

