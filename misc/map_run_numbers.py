#!/usr/bin/env python3

import sys
from pathlib import Path

''' 
Valerio, Jan 2022
-----------------

Scan a folder with pdf named .../event3D_<RUN>_<EVENT>.pdf
and return the run list with total events

Usage:
    ./map_run_numbers.py FOLDERNAME
'''

all_root_files = Path(sys.argv[1]).glob('event3D*.pdf')

runs = {}
for f in all_root_files:

    # split the filename to get run and event numbers
    *_, before_last_part, last_part = f.name.split('_')
    run   = int(before_last_part)
    event = int(last_part.rstrip('.pdf'))

    if runs.get(run,-1) == -1:
        # new run
        runs[run] = {
        'event_tot'   : 1,
        'event_list' : [event]
        }
    else:
        # add events to current run
        runs[run]['event_tot']  += 1
        runs[run]['event_list'].append(event)

# print the resulting dict in a readable way

run_list = sorted([k for k in runs.keys()])

print(f"{'Run':<8} {'tot ev.':<8} (from --> to)\n{'-'*40}")

for r in run_list:
    print(f"{r:<8} {runs[r]['event_tot']:<8} ({min(runs[r]['event_list'])} --> {max(runs[r]['event_list'])})")
