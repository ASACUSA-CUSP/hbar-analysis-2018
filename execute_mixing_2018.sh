#!/bin/bash

#file="midas_nrs_mixing.txt"

file="list_of_runs_Mo.txt"

name=$(cat "$file")

for i in $name;
do
    #scp /pcsmihbar00/2016/offlineData/"output-run-"$i".root" bernadette@193.170.93.242:~/offlineData/mixing_runs/
#    echo "/home/bernadette/offlineData/mixing_runs/output-run-"$i".root"


    #
    python tracking_data_3d_final.py "../../onlineData_2018/pbars_Mo/$i"
    
    
    #python count_events_calc_rate.py "$i"
    
    #echo "/localdata/hbarhfs_2016/offlineData/output-run-"$i".root" >> runnames_file.dat
done


#cat runnames_file.dat | xargs -i scp {} bernadette@193.170.93.242:~/offlineData/mixing_runs/
