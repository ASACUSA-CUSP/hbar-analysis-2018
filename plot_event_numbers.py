#!/usr/bin/python
# -*- coding: utf-8 -*-
# abgeleitet von tracking_geant4_3dtest_new.py -- unnoetige fkten geloescht, aufgeraumt, eigene files in src for plotting etc

import numpy as np
#import Image
import math

#from itertools import product, combinations
from numpy import random

import matplotlib.pyplot as plt

import re

from src.hodoscope_tracking import * # same as just hodoscope with a few extra functions for tracking
from src.bgo import * # newest calibration
from src.scalar import *


##############################################################################################################
##############################################################################################################
if __name__ == "__main__":

    #from matplotlib.patches import Circle
    from matplotlib.collections import PatchCollection
    from sys import argv

    #from scalar import *
    #from hodoscope import *

    evs = np.loadtxt(argv[1])   
    
    print len(evs)
    list_evt_times = []

    for evtnumber, event in enumerate(evs):
    
          if evtnumber == 0:
            first_ts = event[-1]
          
          
          #print evtnumber, event[-1] - first_ts
          
          list_evt_times.append(event[-1] -first_ts)          
          
                    
        #h = hodoscope(event)

        #nI, nO, I, O = h.getActiveBar()
        
        
        #if nI < 1:
        #    continue
            
        #if nO < 1:
        #    continue
          
    timediff =  event[-1] - first_ts    
    print first_ts, timediff, evtnumber + 1, (evtnumber+1)*1.0/timediff
    print len(np.array(list_evt_times[74:1299]))/(abs(list_evt_times[74] - list_evt_times[1299]))
    
    fig, ax = plt.subplots()
    
    range1 = 0
    range2 = timediff
    binnumber = 100
    
    ax.set_xlim([range1, range2])
    
    ##########################################################################  
    y, bin_edges = np.histogram(np.array(list_evt_times), bins=binnumber, range = [range1,range2])
    
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm1 = 1.0*sum(np.diff(bin_edges)*y)
    err1 = np.sqrt(y)/norm1
    y = y/norm1
    
    
    ax.errorbar(bin_centers, y, yerr = err1, fmt = '-o', markersize = 0.2)
   
    plt.show()
    #print "length of run: ", timediff 
    #
    #print "nr of events: ", evtnumber + 1
    #
    #print "rate: ", (evtnumber+1)*1.0/timediff
    
        
