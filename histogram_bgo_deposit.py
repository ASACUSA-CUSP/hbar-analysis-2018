#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
from rootpy.tree import TreeChain
#from rootpy.tree import TreeChain
import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from sys import argv
import sys
sys.path.append("..")
#from src.bgo import *
#from src.hodoscope import *



import matplotlib
import matplotlib.pyplot as plt

from src.bgo import *
from src.hodoscope_tracking import *

#################################################################################################################################
if __name__ == '__main__':
    if len(argv) < 2:
        print "usage: ", argv[0], "rootfile1.root rootfile2.root ... rootfileN.root  "
        exit(-1)

    correctionData = readTimingCorrectionData("./calibrationData/hodoscopeTimingCorrectionTable.dat")
    bgoCalibData = readBGOCalibrationData("./calibrationData/gainBGOcalibration.dat")
    fibre_inner_cuts = np.loadtxt('./calibrationData/cuts_inner_fibre.dat')
    fibre_outer_cuts = np.loadtxt('./calibrationData/cuts_outer_fibre.dat')
    
    data = []
    T = TreeChain("HbarEventTree", argv[1:])
    
    for event in T:  
        h = hodoscope(event, fibre_inner_cuts, fibre_outer_cuts)
        nI, nO, I, O = h.getActiveBar()
        
        if nI>1 and nO>1:
                    
            b = BGO(event, bgoCalibData)

            data.append(b.getCharge())


    r = [1, 50]
    bins = 40
    
    hist, edges = np.histogram(a=data, bins=bins, range=r)   
    hist = hist.astype(np.float)       
    norm = 1.0*sum(np.diff(edges)*hist) # area  
    bincenters   = edges[:-1]+(edges[1]-edges[0])/2.
    
    fig, ax = plt.subplots()
    
    fig.tight_layout() 
    

    redc = (176.0/255,70.0/255,35.0/255) 
    r = [1,50]
    
    ax.errorbar(bincenters, hist/norm, yerr = np.sqrt(hist)/norm, label="bgo E ", fmt='o-', ms=2., color =redc)  
 
 
 
    plt.show() 
