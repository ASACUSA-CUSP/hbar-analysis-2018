
#!/usr/bin/python
# -*- coding: utf-8 -*-
# abgeleitet von tracking_geant4_3dtest_new.py -- unnoetige fkten geloescht, aufgeraumt, eigene files in src for plotting etc

import numpy as np
#import Image
import math

#from itertools import product, combinations
from numpy import random

import matplotlib.pyplot as plt

import re

from src.hodoscope_tracking import * # same as just hodoscope with a few extra functions for tracking
from src.bgo import * # newest calibration
from src.scalar import *


##############################################################################################################
##############################################################################################################
if __name__ == "__main__":
    import ROOT
    from rootpy.tree import TreeChain

    #from matplotlib.patches import Circle
    from matplotlib.collections import PatchCollection
    from sys import argv

    #from scalar import *
    #from hodoscope import *

    if len(argv) < 2:
        print "usage: ", argv[0], "rootfile1.root rootfile2.root ... rootfileN.root  "
        #exit(-1)


    filename = str(argv[1])
    midas_run_nr = (re.findall('\d+', filename))
    
    midas_run_nr = int(midas_run_nr[0])
    #print midas_run_nr 

    #threso = np.loadtxt("outer_amp_cuts_up_down_cosmic.dat")
    thresi = np.loadtxt("./calibrationData/inner_charge_cuts_up_down_pbar_ang.dat")
    threso = np.loadtxt("./calibrationData/outer_charge_cuts_up_down_pbar_ang.dat")
    #threso = np.loadtxt("outer_amp_cuts_up_down_cosmic.dat")
    fibre_inner_cuts = np.loadtxt('./calibrationData/inner_fibre_tot_cuts_2019.dat')
    fibre_outer_cuts = np.loadtxt('./calibrationData/outer_fibre_tot_cuts_2019.dat')   

    T = TreeChain("HbarEventTree", argv[1:])
    
    
    for evtnumber, event in enumerate(T):
        #print evtnumber
        ts = event["midasMilliTimeStamp"][0]
        
        if evtnumber == 0:
            first_ts = ts
                    
        h = hodoscope(event,thresi,threso,fibre_inner_cuts, fibre_outer_cuts)

        nI, nO, I, O = h.getActiveBar()
        
        


            
        timediff =  ts - first_ts   #timediff between current and first event
        
        #print midas_run_nr, first_ts, timediff, evtnumber   #, (evtnumber+1)*1.0/timediff
        print evtnumber, ts, timediff   #, ts
    
    print "length of run: ", timediff 
    #
    print "nr of events: ", evtnumber + 1
    #timediff = 
    #print "rate: ", (evtnumber+1)*1.0/timediff
    
        
