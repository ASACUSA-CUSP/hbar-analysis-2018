# How to run the 2018 code

The code is **Python2** based and it makes use of the **rootpy** module (the ROOT python interface), which works up to a certain ROOT version (do not use newer than 6.18/04).

There are 2 ways to set up a working enviroment:
1. Using a virtualenv [instructions to be added]
2. Using a preconfigured docker image [instructions to be added]

