#!/bin/bash

## Usage:
## $0 [files...]
## Should no files be provided, all the files and folders in the current folder
## will be processed.

gnu_time_path="/usr/bin/time"

if [ -f "$gnu_time_path" ]; then
    time_cmd="$gnu_time_path"
else
    time_cmd="time"
    echo 'GNU time not found in "/usr/bin/time". Using bash built-in...' >&2
fi

cmd="$time_cmd python tracking_data_3d_final_newest.py"
#cmd="$time_cmd echo word"

parallel --noswap --eta --linebuffer --tag "$cmd" ::: $@
