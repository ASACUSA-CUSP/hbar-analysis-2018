#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':

    pbardata_orig = []
        
    for n, filei in enumerate(argv[1:]):
        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_orig = data_orig
        else: 
            pbardata_orig = np.vstack((pbardata_orig, data_orig))
            
            
    pbardata_orig  = pbardata_orig[pbardata_orig[:,6] >= 3]  
                    
    ####################################################################################################################################
    
    fig, ax = plt.subplots()
    
    range1 = 0.0
    range2 = 180.0
    bins = 80
    
    ax.set_xlim([range1, range2])
    #ax.set_ylim(0,)
    
    # max angle+ 2nd max mean
    #angle_pbar = (pbardata_orig[:,7]+ pbardata_orig[:,8])*0.5

    angle_pbar = pbardata_orig[:,10]
   
    hist, edges = np.histogram(angle_pbar, bins=bins, range=[range1, range2])
    norm = 1.0*sum(np.diff(edges)*hist)
    err  = np.sqrt(hist)
    offset = (edges[1]-edges[0])/2.
    centres = edges[:-1]+offset/2  

    ax.errorbar(centres, hist/norm, yerr = err/norm, label = '$\overline{p}$', color = pbarcolor, linewidth = 1)
    
    """hist, edges = np.histogram(angle_cosmic, bins=bins, range=[range1, range2])
    norm = 1.0*sum(np.diff(edges)*hist)
    err  = np.sqrt(hist)
    offset = (edges[1]-edges[0])/2.
    centres = edges[:-1]+offset/2  

    ax.errorbar(centres, hist/norm, yerr = err/norm, label = 'cosmics', color = cosmiccolor, linewidth = 1)"""
    
    ax.legend()
    
    plt.xlabel("max angle (degree)")
    plt.ylabel("norm. counts")
    
    ##################################################################################################################
    
    
    plt.savefig('max_angle.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

