#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 


from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import re

import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
from matplotlib.cm import coolwarm
from matplotlib.lines import Line2D
from scipy import stats
from scipy.odr import ODR, Model, RealData

from scipy.optimize import fsolve, fmin, minimize, brute


def findIntersection(func1, func2, x0):
    return fsolve(lambda x: func1(x) - func2(x), x0)


def gauss(c,X):
    #c[0]..amplitude
    #c[1]..location
    #c[2]..sigma    

    #return c[0]*(stats.cosine.pdf(x=X, loc=c[1], scale=c[2]))
    return c[0]*(stats.norm.pdf(x=X, loc=c[1], scale=c[2]))

    
def plotGaussResult2(output,X):
    return gauss(output,X)

def plotGaussResult(output,X):
    c = output.beta
    return gauss(c,X)
    

def fit(data, r):
    # data is unbinned
    # r is a list for the range
    
    
    binSize = 2*(np.percentile(data,75)- np.percentile(data,25))/(len(data)**(1./3.)) # Freedmann-Diaconis
    nBins   = (r[1]-r[0])/binSize  


    print "hier 1 "
    print nBins
    print data.shape
    #weights = np.ones_like(data)/float(len(data))
    
    nBins = 25

    hist, edges = np.histogram(a=data, bins=int(nBins), range=r)   
    hist = hist.astype(np.float) 
    #print "sum: ",
    #histsum = hist.sum()     
    
    norm = 1.0*sum(np.diff(edges)*hist) # area   
            
    
    
    
    #cut = hist > 0.	
    
    
    #print cut.shape
    print edges.shape
    print hist.shape

    
    #print "hier 1.0 "
    binCentre   = edges[:-1]+(edges[1]-edges[0])/2.
    #print bin

    #hist = hist[cut]
    #edges = edges[cut]
    #binCentre = binCentre[cut]

    model       = Model(gauss)
    #print "hier 2 "

    xErrors     = np.ones_like(binCentre)*binSize/2.
    yErrors     = np.sqrt(hist)     # Poisson error

    #yErrors[yErrors==0.] = 999999999999 # ignore everything with zero counts
    

    
    fitData     = RealData(x=binCentre, y=hist/norm, sx=xErrors, sy=yErrors/norm)

    #print "hier 3 "
    start_vals = []
    #vals1 = [0,2]
            

    #start_vals = [700,100,122,700,200,122] # bar 0 outer
    
    start_vals = [np.max(hist)/norm, binCentre[np.argmax(hist)], np.std(data)] #, np.max(hist), binCentre[np.argmax(hist)], np.std(data)]
            
    #start_vals = [700, 200, 150, 400, 50, 150]
    #1 2 
              
    
    print "....................................................................................... ", start_vals
        
    #myodr = ODR(fitData, model, beta0=np.array([np.max(hist) , binCentre[np.argmax(hist)], np.std(data), np.max(hist), binCentre[np.argmax(hist)], np.std(data)]))
    myodr = ODR(fitData, model, beta0=np.array(start_vals))
    #print "-------------------- ", barnr, start_vals_center, np.max(hist), std_val
    
    #myodr = ODR(fitData, model, beta0=np.array([np.max(hist), start_vals_center[0] , std_val, np.max(hist), start_vals_center[1] , std_val]))
    #myodr.set_job(fit_type=2)
    myoutput = myodr.run()
    myoutput.pprint()
    

    print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"     #myoutput.pprint() # optional
    
    #"Quasi-chi-squared" is defined to be the [total weighted sum of squares]/dof
    #	i.e. same as numpy.sum((residual/adjusted_err)**2)/dof or
    #       numpy.sum(((output.xplus-x)/x_sigma)**2
    #                                +((y_data-output.y)/y_sigma)**2)/dof
    #	Converges to the conventional chi-squared for zero x uncertainties.
      
    return myoutput, hist, binCentre, norm
    #plt.errorbar(binCentre[:-1], hist,xerr=xErrors, yerr=np.sqrt(hist),fmt='o')
    #x=np.linspace(r[0], r[1], 20000)
    #plt.plot( x,plotGaussResult(myoutput,x))



#################################################################################################################################


if __name__ == '__main__':

            if len(argv) < 2:
                print "usage: ", argv[0], "txt file"
                exit(-1)
        
            print "HI!"
        
            #test_array = np.loadtxt(argv[1])[:,1]
            
            cosmicdata_orig = np.loadtxt(argv[1])   
                    
            #cosmics1 = cosmics1[cosmics1[:,6]==3]
            #print  + cosmicdata_orig[:,-1] - cosmicdata_orig[0,-1]
            fist_ts_cosmic = cosmicdata_orig[0,-1]
            print cosmicdata_orig[:,-1] - cosmicdata_orig[0,-1]
            print " "
            cosmicdata_orig = cosmicdata_orig[np.logical_and( np.absolute(cosmicdata_orig[:,-1] - fist_ts_cosmic) < 61, np.absolute(cosmicdata_orig[:,-1] - fist_ts_cosmic) > 35)]
            cosmicdata_orig = cosmicdata_orig[cosmicdata_orig[:,6]> 2] 
            
            ###############################################################################################
            
            pbardata_orig = np.loadtxt(argv[2])         
               
            #print np.absolute(pbardata_orig[:,-1] - pbardata_orig[0,-1])
            fist_ts_pbar = pbardata_orig[0,-1]
            print pbardata_orig[:,-1] - pbardata_orig[0,-1]
            pbardata_orig = pbardata_orig[np.logical_and( np.absolute(pbardata_orig[:,-1] -fist_ts_pbar) < 61, np.absolute(pbardata_orig[:,-1] - fist_ts_pbar) > 35)]  # for seconds, see pbars_arriving.pdf
            pbardata_orig = pbardata_orig[pbardata_orig[:,6]> 2] 
            
            #print "pbar events after cut on timestamp: ", pbardata.shape
            #pbardata_orig = pbardata_orig[pbardata_orig[:,21] <= 1] # no pbar hits with several hits!   
            #pbardata_orig, pbardata_out_orig = cut_on_vertex(pbardata_orig)
         
            mtds_pbar_plot = pbardata_orig[:,15]
            mtds_cosmic_plot = cosmicdata_orig[:,15]
            
            
            
            print "---------------------------------" , len(mtds_pbar_plot)
            
            range1 = 0.0
            range2 = 0.8
        
            
           # outfit, hist, bincenters = fit(np.array(test_array), np.array([range1,range2]))
            outfitp, histp, bincentersp, normp = fit(np.array(mtds_pbar_plot), np.array([range1,range2]))
            
            

            print "#########################################"
            #print "cosmics: "
            #print outfit.beta[0], outfit.sd_beta[0]
            #print outfit.beta[1], outfit.sd_beta[1]
            #print outfit.beta[2], outfit.sd_beta[2]
            print "pbars: "
            print outfitp.beta[0], outfitp.sd_beta[0]
            print outfitp.beta[1], outfitp.sd_beta[1]
            print outfitp.beta[2], outfitp.sd_beta[2]

            print " "
            print "FWHM: ", outfitp.beta[2]*2.35*1000, "ps"
            #print outfit.res_var, outfitp.res_var
            print "#########################################"
            
            
          
            fig, ax = plt.subplots()
            x = np.linspace(range1,range2,100)
            #y = plotGaussResult(outfit,x)
            yp = plotGaussResult(outfitp,x)
            #ax.plot(x,y)
            ax.plot(x,yp, c = 'black')
            #ax.errorbar(bincenters, hist, yerr = np.sqrt(hist))
            
            redc = (176.0/255,70.0/255,35.0/255)
            
            
            ax.errorbar(bincentersp, histp/normp, yerr = np.sqrt(histp)/normp, label = 'pbars', c = redc)
            plt.xlabel("tof (ns)")
            plt.ylabel("counts")
            
            
           
            
            ##################################################################################################################
            
            range1 = 0.0
            range2 = 0.8
            
            outfitc, histc, bincentersc, normc = fit(np.array(mtds_cosmic_plot), np.array([range1,range2]))

            print "#########################################"
            #print "cosmics: "
            #print outfit.beta[0], outfit.sd_beta[0]
            #print outfit.beta[1], outfit.sd_beta[1]
            #print outfit.beta[2], outfit.sd_beta[2]
            print "cosmics: "
            print outfitc.beta[0], outfitc.sd_beta[0]
            print outfitc.beta[1], outfitc.sd_beta[1]
            print outfitc.beta[2], outfitc.sd_beta[2]

            print " "
            print "FWHM: ", outfitc.beta[2]*2.35*1000, "ps"
            #print outfit.res_var, outfitp.res_var
            print "#########################################"
            
            
            x = np.linspace(range1,range2,100)
            #y = plotGaussResult(outfit,x)
            yc = plotGaussResult(outfitc,x)
            #ax.plot(x,y)
            ax.plot(x,yc, c = 'black')
            #ax.errorbar(bincenters, hist, yerr = np.sqrt(hist))
            
            
            bluec = (36.0/255,75.0/255,166.0/255)
            
            ax.errorbar(bincentersc, histc/normc, yerr = np.sqrt(histc)/normc, label = 'cosmics', c = bluec)
            plt.xlabel("ToF (ns)", fontsize = 15)
            plt.ylabel("norm. counts", fontsize = 15)
            legend = ax.legend(loc='upper center', shadow=True)
            
            
            textstrp = 'pbars:\n$\mu=%.3f$ ns\n$\sigma=%.3f$ ns'%(outfitp.beta[1],  outfitp.beta[2])
            textstrc = 'cosmics:\n$\mu=%.3f$ ns\n$\sigma=%.3f$ ns'%(outfitc.beta[1],  outfitc.beta[2])

            # these are matplotlib.patch.Patch properties
            props = dict(boxstyle='round', facecolor='white', alpha=0.5)

            # place a text box in upper left in axes coords
            start_c = 0.67
            ax.text(start_c + 0.05, 0.95, textstrp, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)
            ax.text(start_c + 0.05, 0.72, textstrc, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)
            
            ax.set_xlim(range1,range2)
            ax.set_ylim(0,)
            
            plt.savefig('ToF_mtd_via_std.pdf', facecolor=fig.get_facecolor())
            #fig,close()
            plt.show()
    
        #thefile.close()   

