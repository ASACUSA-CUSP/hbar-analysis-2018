#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import os
import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################

if __name__ == '__main__':
    
    barplot_ = True
    errorbars_ = False
    
    3Dtracks = False
    NrHits_inner = True
    NrHits_outer = False

    #####################################################################################
    
    pbardata_Mo = []    
    pbardata_C = [] 
    pbardata_Au = [] 
    filenames_Mo = []
    filenames_C = []
    filenames_Au = []
    
    #####                
    basepath = "/home/bernadette/Dropbox/hbar_cuts_final/data_files_tpx_Gold/"
    for entry in os.listdir(basepath):
        if os.path.isfile(os.path.join(basepath, entry)):
            filenames_Au.append(basepath + entry)
              
    for n, filei in enumerate(filenames_Au):        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        #print filei
        #print data_orig.shape
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_Au = data_orig
        else: 
            pbardata_Au = np.vstack((pbardata_Au, data_orig))

    #####
    basepath = "/home/bernadette/Dropbox/hbar_cuts_final/data_files_tpx_Molybdenum/"
    for entry in os.listdir(basepath):
        if os.path.isfile(os.path.join(basepath, entry)):
            filenames_Mo.append(basepath + entry)
              
    for n, filei in enumerate(filenames_Mo):        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        #print filei
        #print data_orig.shape
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_Mo = data_orig
        else: 
            pbardata_Mo = np.vstack((pbardata_Mo, data_orig))
            
            
    pbardata_C = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts_final/data_file_tpx_Carbon/timepix_17_december_pbar_all_smaller_errors_eps90_2.dat")    
    ts_0 = pbardata_C[0][-1]
    print "ts_0", ts_0        
    pbardata_C = pbardata_C[np.logical_and(np.absolute(pbardata_C[:,-1] - ts_0) < 50, np.absolute(pbardata_C[:,-1] - ts_0) > 30)]
          
      
    
    #####################################################################################    
    #BGO_cut = 0
    Track_cut = 0   
    pbardata_C = pbardata_C[pbardata_C[:,6]>=Track_cut]
    pbardata_Au = pbardata_Au[pbardata_Au[:,6]>=Track_cut]
    pbardata_Mo = pbardata_Mo[pbardata_Mo[:,6]>=Track_cut]

     
    
    fig, ax = plt.subplots()
    
    plt.title("measured antiproton annihilations")
    
    range1 = -0.5
    range2 = 6.5
    binnumber = 7
    
    ax.set_xlim([range1, 7])
    
    if 3Dtracks == True:        
        tr_pbarC_plot = pbardata_C[:,27]
        tr_pbarMo_plot = pbardata_Mo[:,27]
        tr_pbarAu_plot = pbardata_Au[:,27]

    elif NrHits_inner == True:        
        tr_pbarC_plot = pbardata_C[:,4]
        tr_pbarMo_plot = pbardata_Mo[:,4]
        tr_pbarAu_plot = pbardata_Au[:,4]

    elif NrHits_outer == True:        
        tr_pbarC_plot = pbardata_C[:,5]
        tr_pbarMo_plot = pbardata_Mo[:,5]
        tr_pbarAu_plot = pbardata_Au[:,5]


    
    y_C, bin_edges = np.histogram(tr_pbarC_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm_C = 1.0*sum(np.diff(bin_edges)*y_C)    
    err_C = np.sqrt(y_C)/norm_C
    #y_C = y_C/norm_C
            
    y_Mo, bin_edges = np.histogram(tr_pbarMo_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm_Mo = 1.0*sum(np.diff(bin_edges)*y_Mo)    
    err_Mo = np.sqrt(y_Mo)/norm_Mo
    #y_Mo = y_Mo/norm_Mo   
    
    y_Au, bin_edges = np.histogram(tr_pbarAu_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm_Au = 1.0*sum(np.diff(bin_edges)*y_Au)    
    err_Au = np.sqrt(y_Au)/norm_Au
    #y_Au = y_Au/norm_Au 
    
    if barplot_ == True:     
        index = np.arange(7)
        bar_width = 0.25
        error_config = {'ecolor': '0.3'}
        
        rects_C = plt.bar(bin_centers - bar_width, y_C/norm_C, bar_width,
                    color="slategray",
                     yerr=err_C,
                     #error_kw=error_config,
                     label='C' )#"""

        rects_Mo = plt.bar(bin_centers , y_Mo/norm_Mo, bar_width,                 
                     color="silver",
                     yerr=err_Mo,
                     #error_kw=error_config,
                     label='Mo' )

        rects_Au = plt.bar(bin_centers + bar_width, y_Au/norm_Au, bar_width,                 
                     color="gold",
                     yerr=err_Au,
                     #error_kw=error_config,
                     label='Au' )    
        
        plt.xticks(index + bar_width, ('0', '1', '2', '3', '4', '5', '6'))
    
    if errorbars_ == True:

        plt.errorbar(bin_centers, y_C/norm_C, yerr=np.sqrt(y_C)/norm_C, lw = 1, label = 'C', color =  "slategray")
        plt.errorbar(bin_centers, y_Mo/norm_Mo, yerr=np.sqrt(y_Mo)/norm_Mo, lw = 1, label = 'Mo', color =  "silver")
        plt.errorbar(bin_centers, y_Au/norm_Au, yerr=np.sqrt(y_Au)/norm_Au, lw = 1, label = 'Au', color =  "gold")
    
    # compare pbar and cosmics: ##########################################################################################
    #n, bins, patches = ax.hist( [tr_pbarC_plot, tr_pbarMo_plot], yerr= 1, histtype='bar',
    #    bins = binnumber, range=(range1, range2), align='mid', label = ["$\overline{p}$","cosmics"], normed = True, color = [pbarcolor, cosmiccolor]) 
    ######################################################################################################################
    
    ax.legend()
    if 3Dtracks == True:
        plt.xlabel("number of 3D tracks")
    if NrHits_inner == True:
        plt.xlabel("number of hits, inner hodoscope")
    if NrHits_outer == True:
        plt.xlabel("number of hits, outer hodoscope")
        
    plt.ylabel("norm. counts")
    
    ##################################################################################################################

    if 3Dtracks == True:
        name_ = "number_of_tracks"
    if NrHits_inner == True:
        name_ = "number_hits_inner"
    if NrHits_outer == True:
        name_ = "number_hits_outer"
    
    if barplot_ == True:        
        plt.savefig(name_ + '_tpx_pbars_2019_barplot_data.pdf') #, facecolor=fig.get_facecolor())
    if errorbars_ == True:
        plt.savefig(name_ + '_tpx_pbars_2019_data.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

