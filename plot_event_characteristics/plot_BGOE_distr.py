#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.dates as mdates


from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

from matplotlib.cm import coolwarm

from uncertainties import unumpy

import math



if __name__ == '__main__':

    #print plotcosmic
    #print plotcosmic.shape
    
    #print plotpbar
    #print plotpbar.shape
    
    plotcosmic = np.loadtxt("/home/bernadette/Dropbox/machine_learning/training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")
    
    cosmicg4 = np.loadtxt("/home/bernadette/rootAnalyser_G4_paper/cosmics_g4_3m.dat")
    #cosmicg4 = np.loadtxt("/home/bernadette/g4_pbar_cosmic_files_fibre_tracking/dat_files_3D/test_cosmics_8to17_covarweight_testcut.dat")
    #print cosmicg4.shape
    bgo_E = cosmicg4[:,0]
    cosmicg4_i = cosmicg4[:,1:33]
    cosmicg4_o = cosmicg4[:,33:65]    

    count_cut_i = cosmicg4_i > 0.1  # energy cut of bars! 0.1 MeV
    mult_i = np.sum(count_cut_i[:],axis=1)

    count_cut_o = cosmicg4_o > 0.1  # energy cut of bars! 0.1 MeV
    mult_o = np.sum(count_cut_o[:],axis=1)
    
    
    plotcosmic_sims = bgo_E[np.logical_and(mult_i >2, mult_o >0)]#"""
    
    #plotcosmic_sims = cosmicg4[:,3]
    
    
    
    #############################################################
    plotcosmic = plotcosmic[np.logical_and(plotcosmic[:,4] > 2, plotcosmic[:,5] > 0)]    
    plotcosmic = plotcosmic[:,3]
    #print "cosmics under 10", plotcosmic[plotcosmic<10].shape    
    #print "cosmics over 10", plotcosmic[plotcosmic>10].shape 
    
    #print "pbars under 10", plotpbar[plotpbar[:]<10].shape    
    #print "pbar over 10", plotpbar[plotpbar[:]>10].shape    
    
    fig, ax = plt.subplots()
    plt.tight_layout()
    #plt.tight_layout()
    fig.subplots_adjust(hspace=0.0,wspace=0)
    fig.patch.set_alpha(0.0)

    bluec = (36.0/255,75.0/255,166.0/255)
    redc = (176.0/255,70.0/255,35.0/255) 
    
    bins = 81
    
    r = [0.5,50]
    histc, edgesc = np.histogram(a=plotcosmic, bins=bins, range=r)   
    histc = histc.astype(np.float)       
    normc = 1.0*sum(np.diff(edgesc)*histc) # area 
    
    norm_fac_1 = 0.06 #1.0 #0.05*1.16 # 1.0 #0.062
     
    normc =  normc*norm_fac_1

    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.
    #bincentersp   = edgesp[:-1]+(edgesp[1]-edgesp[0])/2.    
    

    ax.set_xlim(r[0],r[1])
    #ax.set_ylim(0,0.2)
    ax.set_yscale('log')
    ax.errorbar(bincentersc, histc/normc, yerr = np.sqrt(histc)/normc, label="measured cosmics", fmt='o-', ms=2., color = bluec)   
    
    
    histc, edgesc = np.histogram(a=plotcosmic_sims, bins=bins, range=r)   
    histc = histc.astype(np.float)       
    
    normc = 1.0*sum(np.diff(edgesc)*histc) # area  
    
    norm_fac_2 = 1.09*0.07 #1.0 #0.06 # 1.0 # 0.081
     
    normc =  normc*norm_fac_2

    
    #normc =  1.0

    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.
    
    ax.errorbar(bincentersc, histc/normc, yerr = np.sqrt(histc)/normc, label="simulated cosmics", fmt='o-', ms=2., color = redc)   
    
    ax.set_ylim(0.00, 1.9)
     
    #ax.errorbar(bincentersp, histp/normp, yerr = np.sqrt(histp)/normp, label="pbars", fmt='o-', ms=2., color = redc)
    fig.tight_layout() 
    ax.set_ylabel('norm. counts', fontsize = 12)
    ax.legend(loc='best', fancybox=True, framealpha=0.5, fontsize = 12)
    plt.tight_layout()
    plt.gcf().subplots_adjust(bottom=0.15)
    ax.set_xlabel('BGO energy deposit (MeV)', fontsize = 12)
    plt.gcf().subplots_adjust(bottom=0.15)
    #plt.tight_layout()
    #fig.subplots_adjust(hspace=0.1,wspace=0)
    plt.tight_layout()

    """for child in ax[0].get_children():
        if isinstance(child, matplotlib.spines.Spine):
            child.set_color('#dddddd')
    for child in ax[1].get_children():
        if isinstance(child, matplotlib.spines.Spine):
            child.set_color('#dddddd')
    for child in ax[2].get_children():
        if isinstance(child, matplotlib.spines.Spine):
            child.set_color('#dddddd')"""

    
    plt.tight_layout()
    plt.savefig('bgo_e.pdf', facecolor=fig.get_facecolor())
    
    #plt.savefig(name+'.svg', facecolor=fig.get_facecolor())
    #plt.tight_layout()
    plt.show()


        
        
        
        
        
