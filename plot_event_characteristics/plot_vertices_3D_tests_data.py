#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np

import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 

import sys
sys.path.append("..")

import math

#from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import matplotlib.pyplot as plt
from scipy import stats

from scipy.odr import ODR, Model, RealData

from scipy.optimize import fsolve, fmin, minimize, brute

from mpl_toolkits.mplot3d import Axes3D

from matplotlib.colors import LogNorm

from matplotlib.cm import gray_r, gray, bone, coolwarm, jet, rainbow, gnuplot, gnuplot2, gist_stern, ocean, cubehelix,flag, RdGy, BuPu, seismic, terrain, gist_stern

pbarcolor = '#990001'
cosmiccolor = '#51989f'




#################################################################################################################################
#################################################################################################################################


def gauss(c,X):
    #c[0]..amplitude
    #c[1]..location
    #c[2]..sigma    

    #return c[0]*(stats.cosine.pdf(x=X, loc=c[1], scale=c[2]))
    return c[0]*(stats.norm.pdf(x=X, loc=c[1], scale=c[2])) #+ c[3]*(stats.norm.pdf(x=X, loc=c[4], scale=c[5]))

    
def plotGaussResult2(output,X):
    return gauss(output,X)

def plotGaussResult(output,X):
    c = output.beta
    return gauss(c,X)
    

def fit(bins, data, r):
    # data is unbinned
    # r is a list for the range
    
    
    #binSize = 2*(np.percentile(data,75)- np.percentile(data,25))/(len(data)**(1./3.)) # Freedmann-Diaconis
    nBins   = bins

    #print "hier 1 "
    #print nBins
    #print data.shape
    #weights = np.ones_like(data)/float(len(data))

    hist, edges = np.histogram(a=data, bins=int(nBins)+1, range=r)   
    hist = hist.astype(np.float) 
    #print "sum: ",
    #histsum = hist.sum()     
    
    norm = 1.0*sum(np.diff(edges)*hist) # area   
            
    
    cut = hist > 0.	

    
    #print "hier 1.0 "
    binCentre   = edges[:-1]+(edges[1]-edges[0])/2.
    #print bin

    hist = hist[cut]
    #edges = edges[cut]
    binCentre = binCentre[cut]

    model       = Model(gauss)
    #print "hier 2 "

    xErrors     = np.ones_like(binCentre)*(abs(r[0] - r[1])/nBins*0.5)
    yErrors     = np.sqrt(hist)     # Poisson error
    
    fitData     = RealData(x=binCentre, y=hist/norm, sx=xErrors, sy=yErrors/norm)

    #print "hier 3 "
    start_vals = []
    #vals1 = [0,2]

    start_vals = [np.max(hist)/norm, binCentre[np.argmax(hist)], np.std(data)*1.1]#*0.40, np.max(hist) +10, binCentre[np.argmax(hist)], np.std(data)]

    #print "....................................................................................... ", start_vals
        
    #myodr = ODR(fitData, model, beta0=np.array([np.max(hist) , binCentre[np.argmax(hist)], np.std(data), np.max(hist), binCentre[np.argmax(hist)], np.std(data)]))
    myodr = ODR(fitData, model, beta0=np.array(start_vals))

    myoutput = myodr.run()
    myoutput.pprint()
    

    return myoutput, hist, binCentre, norm

#################################################################################################################################
#################################################################################################################################


def plot_3D_vertex(data_, data__):

    fig_3d2 = plt.figure()
    ax_3d2 = fig_3d2.add_subplot(111, projection='3d')
    
    ax_3d2.set_xlim(-150, 150)
    ax_3d2.set_ylim(-150, 150)        
    ax_3d2.set_zlim(-150, 150) 
    
    xyz = data_[:,35:38]
    xyz2 = data__[:,35:38]

    ax_3d2.plot(xyz2[:, 0], xyz2[:, 1],  xyz2[:,2],'o', markerfacecolor=pbarcolor,
            markersize=3)

    ax_3d2.plot(xyz[:, 0], xyz[:, 1],  xyz[:,2],'o', markerfacecolor=cosmiccolor,
            markersize=3)
            
            
            
    plt.show()        

#################################################################################################################################
#################################################################################################################################
def plot_2Dhist_vertex(cosmics_, pbars_):

    #mdp_c = cosmics_[:,31:34]
    #mdp_p = pbars_[:,31:34]
    
    cluster_c = cosmics_[:,35:38]
    cluster_p = pbars_[:,35:38]
    
    #cluster_c = cosmics_[:,31:34]
    #cluster_p = pbars_[:,31:34]
    
    fig, ax = plt.subplots(1,2, figsize = (15,5))
   
    rangeval = 200
   
    histxyp, xbins, ybins = np.histogram2d(cluster_p[:,0], cluster_p[:,1], bins=[50,50], range = [[-rangeval,rangeval], [-rangeval,rangeval]])
    histxyc, xbins, ybins = np.histogram2d(cluster_c[:,0], cluster_c[:,1], bins=[50,50], range = [[-rangeval,rangeval], [-rangeval,rangeval]])
    
    extent = [xbins.min(),xbins.max(),ybins.min(),ybins.max()]
    #histxy = histxy
    im =  ax[0].imshow(histxyp.T, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='auto', label = "antiprotons", norm=LogNorm())
    im1 =  ax[1].imshow(histxyc.T, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='auto',label = "cosmics",norm=LogNorm())
    
    #ax[0].set_xticks( fontsize = 13)
    #ax[0].set_xticks( fontsize = 13)
        
    #ax[1].yticks( fontsize = 13)
    #ax[1].yticks( fontsize = 13)
        
    ax[0].patch.set_facecolor='w'    
    ax[0].set_xlabel("x (mm)", fontsize = 13)
    ax[0].set_ylabel("y (mm)", fontsize = 13)   
    
    ax[1].patch.set_facecolor='w'    
    ax[1].set_xlabel("x (mm)", fontsize = 13)
    ax[1].set_ylabel("y (mm)", fontsize = 13)   
   
    cbar = plt.colorbar(im)
    #cbar.ax.tick_params(labelsize=13)
    #cbar.ax[1].tick_params(labelsize=13)  
    
    ax[0].legend()
    ax[1].legend()
    #plt.savefig("cosmic_ToF_vs_E_{0}_tracks.png".format(nr_of_tracks)) 
    #plt.savefig("cosmic_angl_vs_tof_{0}_tracks_pvalcut.pdf".format(nr_of_tracks)) 
    
    plt.savefig("test_2Dhist_xy_pbar_all.png")
    
    plt.show()





def forceAspect(ax,aspect=1):
    im = ax.get_images()
    extent =  im[0].get_extent()
    ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)
#################################################################################################################################
#################################################################################################################################
def plot_simulation_vertex_2D():


    """pbar11 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_0_0_firstpos_gauss_oct2_covarweight_justcarbon_smi_trigger_new_1.dat")
    pbar22 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_0_0_firstpos_gauss_oct2_covarweight_justcarbon_smi_trigger_new_2.dat")
    
    pbar00 = np.vstack((pbar11, pbar22))#"""
    
    """pbar11 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_source1_90_novacuum.dat")
    pbar22 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_source1_90_novacuum_2.dat")
    pbar33 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_source1_90_novacuum_3.dat")
    pbar44 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_source1_90_novacuum_4.dat")
    
    pbar00 = np.vstack((pbar11, pbar22, pbar33, pbar44))#"""
    
    pbar11 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_chips_1.dat")
    pbar22 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_chips_2.dat")
    pbar33 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_chips_3.dat")
    pbar44 = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts/simulations_fibre/test_pbars_00_carbonfoil_fwhm2_4cm_chips_4.dat") #"""
    pbar00 = np.vstack((pbar11, pbar22, pbar33, pbar44))#"""
   
        
    print len(pbar00)    
    #pbar00 = pbar00[pbar00[:,3]>0]
    #pbar00 = pbar00[pbar00[:,27]>1]
    print len(pbar00)
    
    vertex2D = pbar00[:,40:43]
    
    fig, ax = plt.subplots()
    
   
    ax.set_title("simulated $\overline{p}$s, vertices")
    
    bins = 32
    #hist3D, xbins, ybins = np.histogram2d(cluster[:,0], cluster[:,1], bins=[50,50], range = [[-100,100], [-100,100]])
    hist2D, xbins, ybins = np.histogram2d(vertex2D[:,0], vertex2D[:,1], bins=[bins,bins], range = [[-20,20], [-20,20]])
    
    extent = [xbins.min(),xbins.max(),ybins.min(),ybins.max()]
    #histxy = histxy
    #im =  ax[0].imshow(hist3D.T, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='auto', label = "3D", norm=LogNorm())
    im =  ax.imshow(hist2D.T, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='auto',label = "Geant4")#,norm=LogNorm())
    
    plt.xticks( fontsize = 11)
    plt.yticks( fontsize = 11)
    ax.patch.set_facecolor='w'    
    ax.set_xlabel("x (mm)", fontsize = 12)
    ax.set_ylabel("y (mm)", fontsize = 12)   
     
    cbar = plt.colorbar(im)
    #cbar.ax.tick_params(labelsize=13)
    cbar.ax.tick_params(labelsize=12)  
    
    ax.legend()
        
    from matplotlib.patches import Circle
    from matplotlib.collections import PatchCollection
    r = 45.0
    circ = Circle([0,0], r)
    p = PatchCollection([circ], alpha=1.0)
    p.set_edgecolor("black")
    p.set_facecolor("None")
    ax.add_collection(p   )

    forceAspect(ax,aspect=1)
    #plt.savefig("cosmic_ToF_vs_E_{0}_tracks.png".format(nr_of_tracks)) 
    #plt.savefig("cosmic_angl_vs_tof_{0}_tracks_pvalcut.pdf".format(nr_of_tracks)) 
    
    plt.savefig("test_2Dhist_sims_pbars_chips.pdf")
    
    plt.show()



#################################################################################################################################
#################################################################################################################################

def plot_2Dhist_vertex_compare_cos(data_):


    plot = "xy"

    #data_ = data_[data_[:,3]>1]

    cluster = data_[:,35:38]
    
    vertex2D = data_[:,40:43]
    
    #print vertex2D[:,1]
    

    fig, ax = plt.subplots()
    
    #ax[0].set_title("cluster")
    
    #ax[1].set_title("simulation")
    
    colormap = gnuplot
    
    
    #print vertex2D[:,2]
    
    if plot == "xy":
        rang = 175
        bins = 100
    
        hist3D, xbins, ybins = np.histogram2d(cluster[:,0], cluster[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])#, normed = True)
        #hist2D, xbins, ybins = np.histogram2d(vertex2D[:,0], vertex2D[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        #gray_r, gray, bone, coolwarm, jet, rainbow, gnuplot, gnuplot2, gist_stern, ocean, cubehelix,flag, RdGy, BuPu, seismic, terrain, gist_stern

        pcol = plt.pcolormesh(xbins, ybins, hist3D.T, cmap=colormap, rasterized=True, linewidth=0,vmin=0)#, vmax=20)
        #pcol.set_edgecolor('face')
        #cb.solids.set_rasterized(True)
        

        ax.patch.set_facecolor='w'    
        ax.set_xlabel("x (mm)", fontsize = 13)
        ax.set_ylabel("y (mm)", fontsize = 13)   
        
        #ax[1].patch.set_facecolor='w'    
        #ax[1].set_xlabel("x (mm)", fontsize = 13)
        #ax[1].set_ylabel("y (mm)", fontsize = 13) 
        
        from matplotlib.patches import Circle
        from matplotlib.collections import PatchCollection
        r = 45.0
        circ = Circle([0,0], r)
        p = PatchCollection([circ], alpha=1.0)
        p.set_edgecolor("white")
        p.set_facecolor("None")
        ax.add_collection(p   )
        
        ax.scatter(0,0, marker = '8', facecolor = 'none', edgecolor = 'white', s = 20700*1.30)
        ax.scatter(0,0, marker = '8', facecolor = 'none', edgecolor = 'white', s = 62700*1.30)
        #ax.scatter(0,0, marker = '8', facecolor = 'none', edgecolor = 'white', s = 20700*2.75)
        
        #ax[1].add_collection(p   ) # """  
        
        #ax.scatter(20, 20, color = "white", marker = 'x') 
        #ax[1].scatter(20, 20, color = "black", marker = 'x') 
        
        
    
    if plot == "zy":
        rang = 150
        bins = 80
    
        hist3D, xbins, ybins = np.histogram2d(cluster[:,2], cluster[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        #hist2D, xbins, ybins = np.histogram2d(vertex2D[:,2], vertex2D[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        
        pcol = plt.pcolormesh(xbins, ybins, hist3D.T, cmap=colormap, rasterized=True, linewidth=0)#,vmin=0, vmax=20)
        #pcol.set_edgecolor('face')
        
        ax.patch.set_facecolor='w'    
        ax.set_xlabel("z (mm)", fontsize = 13)
        ax.set_ylabel("y (mm)", fontsize = 13)   
        
        #ax[1].patch.set_facecolor='w'    
        #ax[1].set_xlabel("z (mm)", fontsize = 13)
        #ax[1].set_ylabel("y (mm)", fontsize = 13)   
         
        #ax.scatter(2.5, 20, color = "white", marker = 'x')         
        
        ax.axhline(50, color = 'white', alpha = 0.7, linestyle = '--')
        ax.axhline(-50, color = 'white', alpha = 0.7, linestyle = '--')
        from matplotlib.patches import Rectangle
        ax.add_patch(Rectangle((2.5, -45.0), 5, 90,
                      alpha=1, edgecolor = 'white',facecolor = 'None'))
        
    if plot == "zx":
        rang = 150
        bins = 82
        hist3D, xbins, ybins = np.histogram2d(cluster[:,2], cluster[:,0], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        #hist2D, xbins, ybins = np.histogram2d(vertex2D[:,2], vertex2D[:,0], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        
        pcol = plt.pcolormesh(xbins, ybins, hist3D.T, cmap=colormap, rasterized=True, linewidth=0)#,vmin=0, vmax=20)
        #pcol.set_edgecolor('face')
        
        ax.patch.set_facecolor='w'    
        ax.set_xlabel("z (mm)", fontsize = 13)
        ax.set_ylabel("x (mm)", fontsize = 13)   
        
        #ax[1].patch.set_facecolor='w'    
        #ax[1].set_xlabel("z (mm)", fontsize = 13)
        #ax[1].set_ylabel("x (mm)", fontsize = 13)   
        
        #ax.scatter(2.5, 20, color = "white", marker = 'x')  
               
        ax.axhline(50, color = 'white', alpha = 0.7, linestyle = '--')
        ax.axhline(-50, color = 'white', alpha = 0.7, linestyle = '--')
        from matplotlib.patches import Rectangle
        ax.add_patch(Rectangle((2.5, -45.0), 5, 90,
                      alpha=1, edgecolor = 'white',facecolor = 'None'))
         
        
        
    #extent = [xbins.min(),xbins.max(),ybins.min(),ybins.max()]
    print np.max(hist3D)
    
    plt.colorbar()
    plt.tight_layout()
    ax.set_aspect('equal')
    
    #im =  ax.imshow(hist3D.T/histmax, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='equal', label = "3D")#, norm=LogNorm())
    #im1 =  ax[1].imshow(hist2D.T, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='equal',label = "2D",norm=LogNorm())
    
    plt.xticks( fontsize = 13)
    plt.yticks( fontsize = 13)
    
   
    #cbar = plt.colorbar(im)
    #cbar.ax.tick_params(labelsize=13)
    #cbar.ax.tick_params(labelsize=13)  
    
    #ax[0].legend()
    #ax[1].legend()
    
    
    
    

    
    

    
    #plt.savefig("cosmic_ToF_vs_E_{0}_tracks.png".format(nr_of_tracks)) 
    #plt.savefig("cosmic_angl_vs_tof_{0}_tracks_pvalcut.pdf".format(nr_of_tracks)) 
    if plot == "xy":
        plt.savefig("test_2Dhist_xy_cosmic.pdf")

    if plot == "zy":
        plt.savefig("test_2Dhist_zy_cosmic.pdf")
        
    if plot == "zx":
        plt.savefig("test_2Dhist_zx_cosmic.pdf")    
    plt.show()

#################################################################################################################################
#################################################################################################################################
#################################################################################################################################

def plot_2Dhist_vertex_compare_pbar(data_):

    
    plot = "zy"


    cluster = data_[:,35:38]
    
    vertex2D = data_[:,40:43]
    
    #print vertex2D[:,1]
    

    fig, ax = plt.subplots()
    
    #ax[0].set_title("cluster")
    
    #ax[1].set_title("simulation")
    
    colormap = gnuplot
    
    
    #print vertex2D[:,2]
    
    if plot == "xy":
        rang = 175
        bins = 90
    
        hist3D, xbins, ybins = np.histogram2d(cluster[:,0], cluster[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])#, normed = True)
        #hist2D, xbins, ybins = np.histogram2d(vertex2D[:,0], vertex2D[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        #gray_r, gray, bone, coolwarm, jet, rainbow, gnuplot, gnuplot2, gist_stern, ocean, cubehelix,flag, RdGy, BuPu, seismic, terrain, gist_stern

        pcol = plt.pcolormesh(xbins, ybins, hist3D.T, cmap=colormap, rasterized=True, linewidth=0)#,vmin=0, vmax=500)
        #pcol.set_edgecolor('face')
        #cb.solids.set_rasterized(True)
        

        ax.patch.set_facecolor='w'    
        ax.set_xlabel("x (mm)", fontsize = 13)
        ax.set_ylabel("y (mm)", fontsize = 13)   
        
        #ax[1].patch.set_facecolor='w'    
        #ax[1].set_xlabel("x (mm)", fontsize = 13)
        #ax[1].set_ylabel("y (mm)", fontsize = 13) 
        
        from matplotlib.patches import Circle
        from matplotlib.collections import PatchCollection
        r = 45.0
        circ = Circle([0,0], r)
        p = PatchCollection([circ], alpha=1.0)
        p.set_edgecolor("white")
        p.set_facecolor("None")
        ax.add_collection(p   )
        
        ax.scatter(0,0, marker = '8', facecolor = 'none', edgecolor = 'white', s = 20700*1.30)
        ax.scatter(0,0, marker = '8', facecolor = 'none', edgecolor = 'white', s = 62700*1.30)
        #ax.scatter(0,0, marker = '8', facecolor = 'none', edgecolor = 'white', s = 20700*2.75)
        
        #ax[1].add_collection(p   ) # """  
        
        #ax.scatter(20, 20, color = "white", marker = 'x') 
        #ax[1].scatter(20, 20, color = "black", marker = 'x') 
        
        
    
    if plot == "zy":
        rang = 150
        bins = 80
    
        hist3D, xbins, ybins = np.histogram2d(cluster[:,2], cluster[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        #hist2D, xbins, ybins = np.histogram2d(vertex2D[:,2], vertex2D[:,1], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        
        pcol = plt.pcolormesh(xbins, ybins, hist3D.T, cmap=colormap, rasterized=True, linewidth=0)#,vmin=0, vmax=30)
        #pcol.set_edgecolor('face')
        
        ax.patch.set_facecolor='w'    
        ax.set_xlabel("z (mm)", fontsize = 13)
        ax.set_ylabel("y (mm)", fontsize = 13)   
        
        #ax[1].patch.set_facecolor='w'    
        #ax[1].set_xlabel("z (mm)", fontsize = 13)
        #ax[1].set_ylabel("y (mm)", fontsize = 13)   
         
        #ax.scatter(2.5, 20, color = "white", marker = 'x')         
        
        ax.axhline(50, color = 'white', alpha = 0.7, linestyle = '--')
        ax.axhline(-50, color = 'white', alpha = 0.7, linestyle = '--')
        from matplotlib.patches import Rectangle
        ax.add_patch(Rectangle((2.5, -45.0), 5, 90,
                      alpha=1, edgecolor = 'white',facecolor = 'None'))
        
    if plot == "zx":
        rang = 150
        bins = 82
        hist3D, xbins, ybins = np.histogram2d(cluster[:,2], cluster[:,0], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        #hist2D, xbins, ybins = np.histogram2d(vertex2D[:,2], vertex2D[:,0], bins=[bins,bins], range = [[-rang,rang], [-rang,rang]])
        
        pcol = plt.pcolormesh(xbins, ybins, hist3D.T, cmap=colormap, rasterized=True, linewidth=0)#,vmin=0, vmax=20)
        #pcol.set_edgecolor('face')
        
        ax.patch.set_facecolor='w'    
        ax.set_xlabel("z (mm)", fontsize = 13)
        ax.set_ylabel("x (mm)", fontsize = 13)   
        
        #ax[1].patch.set_facecolor='w'    
        #ax[1].set_xlabel("z (mm)", fontsize = 13)
        #ax[1].set_ylabel("x (mm)", fontsize = 13)   
        
        #ax.scatter(2.5, 20, color = "white", marker = 'x')  
               
        ax.axhline(50, color = 'white', alpha = 0.7, linestyle = '--')
        ax.axhline(-50, color = 'white', alpha = 0.7, linestyle = '--')
        from matplotlib.patches import Rectangle
        ax.add_patch(Rectangle((2.5, -45.0), 5, 90,
                      alpha=1, edgecolor = 'white',facecolor = 'None'))
         
        
        
    #extent = [xbins.min(),xbins.max(),ybins.min(),ybins.max()]
    print np.max(hist3D)
    
    plt.colorbar()
    plt.tight_layout()
    ax.set_aspect('equal')
    
    #im =  ax.imshow(hist3D.T/histmax, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='equal', label = "3D")#, norm=LogNorm())
    #im1 =  ax[1].imshow(hist2D.T, interpolation='none', origin='lower', extent=extent,  cmap = gnuplot, aspect='equal',label = "2D",norm=LogNorm())
    
    plt.xticks( fontsize = 13)
    plt.yticks( fontsize = 13)
    
   
    #cbar = plt.colorbar(im)
    #cbar.ax.tick_params(labelsize=13)
    #cbar.ax.tick_params(labelsize=13)  
    
    #ax[0].legend()
    #ax[1].legend()
    
    
    
    

    
    

    
    #plt.savefig("cosmic_ToF_vs_E_{0}_tracks.png".format(nr_of_tracks)) 
    #plt.savefig("cosmic_angl_vs_tof_{0}_tracks_pvalcut.pdf".format(nr_of_tracks)) 
    if plot == "xy":
        plt.savefig("test_2Dhist_xy_pbar.pdf")

    if plot == "zy":
        plt.savefig("test_2Dhist_zy_pbar.pdf")
        
    if plot == "zx":
        plt.savefig("test_2Dhist_zx_pbar.pdf")    
    plt.show()

#################################################################################################################################


def nmoment(x, counts, c, n):
    return np.sum(counts*(x-c)**n) / np.sum(counts)
#################################################################################################################################

def plot_2D_vertex_distributions_old(pbars__):

    minco = -200
    maxco =200

    rx = [minco,maxco]
    ry = [minco,maxco]
    rz = [minco,maxco]
    
    rx2 = rx
    ry2 = ry
    rz2 = rz
    
    #mask_bias = np.logical_and(np.logical_and(pbars_[:,35] < 5, pbars_[:,35] > -5),np.logical_and(pbars_[:,36] < 5, pbars_[:,36] > -5))
    
    #print pbars_[mask_bias]
    
    #np.savetxt("lol.dat", pbars_[mask_bias])

    #mdp_p = pbars_[:,31:34]
    
    print "len pbars before cut: ", len(pbars__)
    
    #pbars__ = pbars__[pbars__[:,38] > 1]
    
    print "len pbars after cut: ", len(pbars__)
  
    #mdp_p = pbars__[:,40:43] # simulation
    
    
    
    mdp_p__ = pbars__[:,35:38] # largest cluster
    #mdp_p = pbars__[pbars__[:,4]==2][:,35:38]
    
    #mdp_p__ = pbars__[:,43:46] # vertex fit

   
    #diff_p = pbars__[:,43:46] - pbars__[:,40:43]
    diff_p = pbars__[:,35:38] - pbars__[:,40:43]
    #diff_p = pbars__[:,31:34] - pbars__[:,40:43]
    #diff_p = pbars__[:,28:31] - pbars__[:,40:43]
    
    #print len(mdp_c__[mdp_c__[:,0] != 0]) 
    
    
    
    
    #print mdp_c__[:,0]
    
    #exit(0)
    bins= 200
    
    hist_pz2, edges_pz2 = np.histogram(a=mdp_p[:,2], bins=bins, range=rz)    
    hist_pz, edges_pz = np.histogram(a=mdp_p__[:,2], bins=bins, range=rz)     
    bincenterspz2   = edges_pz2[:-1]+(edges_pz2[1]-edges_pz2[0])/2.
    bincenterspz   = edges_pz[:-1]+(edges_pz[1]-edges_pz[0])/2.
    norm_pz2 = 1.0*sum(np.diff(edges_pz2)*hist_pz2)
    norm_pz = 1.0*sum(np.diff(edges_pz)*hist_pz)
    
    
    hist_px2, edges_px2 = np.histogram(a=mdp_p[:,0], bins=bins, range=rx)   
    hist_px, edges_px = np.histogram(a=mdp_p__[:,0], bins=bins, range=rx)
    bincenterspx2   = edges_px2[:-1]+(edges_px2[1]-edges_px2[0])/2.
    bincenterspx   = edges_px[:-1]+(edges_px[1]-edges_px[0])/2.
    norm_px2 = 1.0*sum(np.diff(edges_px2)*hist_px2)
    norm_px = 1.0*sum(np.diff(edges_px)*hist_px)
    
    
    
    
    #FWHM_y = 
    
    #FWHM_z = 
    
    
    hist_py2, edges_py2 = np.histogram(a=mdp_p[:,1], bins=bins, range=ry)    
    hist_py, edges_py = np.histogram(a=mdp_p__[:,1], bins=bins, range=ry)     
    bincenterspy2   = edges_py2[:-1]+(edges_py2[1]-edges_py2[0])/2.
    bincenterspy   = edges_py[:-1]+(edges_py[1]-edges_py[0])/2.
    norm_py2 = 1.0*sum(np.diff(edges_py2)*hist_py2)
    norm_py = 1.0*sum(np.diff(edges_py)*hist_py) 
    
    
    ################################
    
    diff_x = max(hist_px) - min(hist_px)
    H_M = diff_x/2.0
    binmask = hist_px> H_M
    firstbinabove = bincenterspx[binmask][0]
    lastbinabove = bincenterspx[binmask][-1]
    
    FWHM_x = lastbinabove - firstbinabove
    
    print firstbinabove
    print lastbinabove
    
    print "FWHM X", FWHM_x
    
    ####################################
    
    diff_y = max(hist_py) - min(hist_py)
    H_M = diff_y/2.0
    binmask = hist_py> H_M
    firstbinabove = bincenterspy[binmask][0]
    lastbinabove = bincenterspy[binmask][-1]
    
    FWHM_y = lastbinabove - firstbinabove
    
    print firstbinabove
    print lastbinabove
    
    print "FWHM Y", FWHM_y
    
    diff_z = max(hist_pz) - min(hist_pz)
    H_M = diff_z/2.0
    binmask = hist_pz> H_M
    firstbinabove = bincenterspz[binmask][0]
    lastbinabove = bincenterspz[binmask][-1]
    
    FWHM_z = lastbinabove - firstbinabove
    
    print firstbinabove
    print lastbinabove
    
    print "FWHM Z", FWHM_z
    
    
    
    from scipy.stats import moment
    
    print "MOMENTS OF THE DISTRIBUTION Z -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,2], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,2], moment=2))
    
    #print "own fct "
    meanz = nmoment(bincenterspz, hist_pz, 0.0, 1)
    print meanz
    momentz = math.sqrt(nmoment(bincenterspz, hist_pz, meanz, 2))
    print "sqrt(second moment) ", momentz
    print "-----------------------------------------------------------"
    print "MOMENTS OF THE DISTRIBUTION X -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,0], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,0], moment=2))
    
    ##print "own fct "
    meanx = nmoment(bincenterspx, hist_px, 0.0, 1)
    momentx = math.sqrt(nmoment(bincenterspx, hist_px, meanx, 2))
    print meanx
    print "sqrt(second moment) ", momentx
    print "-----------------------------------------------------------"        
    print "MOMENTS OF THE DISTRIBUTION Y -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,1], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,1], moment=2))
    
    #print "own fct "
    meany = nmoment(bincenterspy, hist_py, 0.0, 1)
    momenty = math.sqrt(nmoment(bincenterspy, hist_py, meany, 2))
    print meany
    print "sqrt(second moment) ", momenty
    print "-----------------------------------------------------------"    
    
    
  
    
    ###########
    
    """hist_diffx, edges_diffx = np.histogram(a=diff_p[:,0], bins=bins, range=rx)      
    bincentersdiffx   = edges_diffx[:-1]+(edges_diffx[1]-edges_diffx[0])/2.   
    norm_diffx = 1.0*sum(np.diff(edges_diffx)*hist_diffx)
  
    
    hist_diffy, edges_diffy = np.histogram(a=diff_p[:,1], bins=bins, range=ry)      
    bincentersdiffy   = edges_diffy[:-1]+(edges_diffy[1]-edges_diffy[0])/2.
    norm_diffy = 1.0*sum(np.diff(edges_diffy)*hist_diffy)
   
    
    hist_diffz, edges_diffz = np.histogram(a=diff_p[:,2], bins=bins, range=rz)       
    bincentersdiffz   = edges_diffz[:-1]+(edges_diffz[1]-edges_diffz[0])/2.    
    norm_diffz = 1.0*sum(np.diff(edges_diffz)*hist_diffz)
   
    ########
    
    print " "
    print " "
    
    print "MOMENTS OF THE DIFFERENCE DISTRIBUTION Z -------------------------------" 
    
    print "first moment ", moment(diff_p[:,2], moment=1)
    print "sqrt(second moment) ", math.sqrt(moment(diff_p[:,2], moment=2))
    
    print "own fct "
    meanz2 = nmoment(bincentersdiffz, hist_diffz, 0.0, 1)
    print meanz2
    print "sqrt(second moment) ", math.sqrt(nmoment(bincentersdiffz, hist_diffz, meanz2, 2))
    print "-----------------------------------------------------------"
    print "MOMENTS OF THE DIFFERENCE DISTRIBUTION X -------------------------------" 
    
    print "first moment ", moment(diff_p[:,0], moment=1)
    print "sqrt(second moment) ", math.sqrt(moment(diff_p[:,0], moment=2))
    
    print "own fct "
    meanx2 = nmoment(bincentersdiffx, hist_diffx, 0.0, 1)
    print meanx2
    print "sqrt(second moment) ", math.sqrt(nmoment(bincentersdiffx, hist_diffx, meanx2, 2))
    print "-----------------------------------------------------------"        
    print "MOMENTS OF THE DIFFERENCE DISTRIBUTION Y -------------------------------" 
    
    print "first moment ", moment(diff_p[:,1], moment=1)
    print "sqrt(second moment) ", math.sqrt(moment(diff_p[:,1], moment=2))
    
    print "own fct "
    meany2 = nmoment(bincentersdiffy, hist_diffy, 0.0, 1)
    print meany2
    print "sqrt(second moment) ", math.sqrt(nmoment(bincentersdiffy, hist_diffy, meany2, 2))
    print "-----------------------------------------------------------"    #"""

    #norm_c = len(cosmics_)
    #norm_p = len(pbars_) # = 1.0
    
    fig, ax = plt.subplots(1,3, figsize = (17,5))
    
    """rz_fit = [-100,100]
    outfitp, histp, bincentersp, normp = fit(bins, mdp_p__[:,2], np.array(rz_fit)) 
    x = np.linspace(rz_fit[0],rz_fit[1],100)
    yp = plotGaussResult(outfitp,x)
    ax[0][2].plot(x,yp, color = 'black')

    rx_fit = [-100,100]
    outfitp, histp, bincentersp, normp = fit(bins, mdp_p__[:,0], np.array(rx_fit)) 
    x = np.linspace(rz_fit[0],rz_fit[1],100)
    yp = plotGaussResult(outfitp,x)
    ax[0][0].plot(x,yp, color = 'black')
    
    
    ry_fit = [-100,100]
    outfitp, histp, bincentersp, normp = fit(bins, mdp_p__[:,1], np.array(ry_fit)) 
    x = np.linspace(rz_fit[0],rz_fit[1],100)
    yp = plotGaussResult(outfitp,x)
    ax[0][1].plot(x,yp, color = 'black')   """ 
  
    #norm_px22 = 1
    #norm_cy2  = 1
    #norm_cz2 = 1
    
    #ax[0][0].set_title("compare pbars")    
       
   
    ax[0].errorbar(bincenterspx, hist_px/norm_px, yerr = np.sqrt(hist_px)/norm_px, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = pbarcolor)#
    ax[0].set_xlabel("x (mm)", fontsize = 11)
        
    ax[1].errorbar(bincenterspy, hist_py/norm_py, yerr = np.sqrt(hist_py)/norm_py, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = pbarcolor)#
    ax[1].set_xlabel("y (mm)", fontsize = 11)
                
    ax[2].errorbar(bincenterspz, hist_pz/norm_pz, yerr = np.sqrt(hist_pz)/norm_pz, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = pbarcolor)#
    ax[2].set_xlabel("z (mm)", fontsize = 11)
        
    #ax[0][2].axvline(x=-5,color='k', linestyle='--')
    #ax[0][2].axvline(x=5,color='k', linestyle='--')
    #ax[0][2].axvline(x=-50,color='k', linestyle='--')
    #ax[0][2].axvline(x=50,color='k', linestyle='--')
    #ax[0][2].text(-50, 0.03, "beampipe", rotation=90, verticalalignment='center')
    #ax[0][2].text(50, 0.03, "beampipe", rotation=90, verticalalignment='center')
       
    for i in range(0,2):
        ax[i].axvline(x=-45,color='k', linestyle='--')
        ax[i].axvline(x=45,color='k', linestyle='--')
        #ax[i].text(0.25, 0.25, "bgo", transform=ax[0].transAxes, rotation=90, verticalalignment='center')
        #ax[i].text(0.75, 0.25, "bgo", transform=ax[1].transAxes, rotation=90, verticalalignment='center')
        #ax[0][i].axvline(x=-50,color='k', linestyle='--')
        #ax[0][i].axvline(x=50,color='k', linestyle='--')
        #ax[0][i].text(-50, 0.03, "beampipe", rotation=90, verticalalignment='center')
        #ax[0][i].text(50, 0.03, "beampipe", rotation=90, verticalalignment='center')
        
        
   
    ax[2].axvline(x=-5,color='k', linestyle='--')
    ax[2].axvline(x=5,color='k', linestyle='--')       
   
    #ax[1][0].set_title("compare cosmics")
    
  
    ax[0].errorbar(bincenterspx2, hist_px2/norm_px2, yerr = np.sqrt(hist_px2)/norm_px2, fmt = '-o', markersize = 0.2, label = "simulation", c = cosmiccolor)#
    ax[1].errorbar(bincenterspy2, hist_py2/norm_py2, yerr = np.sqrt(hist_py2)/norm_py2, fmt = '-o', markersize = 0.2, label = "simulation", c = cosmiccolor)#
    ax[2].errorbar(bincenterspz2, hist_pz2/norm_pz2, yerr = np.sqrt(hist_pz2)/norm_pz2, fmt = '-o', markersize = 0.2, label = "simulation", c = cosmiccolor)#    
 

    for i in range(0,3):    
        ax[i].legend(loc = 'upper right', fontsize = 11)          
        ax[i].set_ylabel("norm.counts" , fontsize = 11)
 
        
    """ax[1][1].set_xlabel('y - y$_{sim}$ (mm)')  
    ax[1][2].set_xlabel('z - z$_{sim}$ (mm)')       
    ax[1][0].set_xlabel('x - x$_{sim}$ (mm)')
        
    ax[1][0].errorbar(bincentersdiffx, hist_diffx/norm_diffx, yerr = np.sqrt(hist_diffx)/norm_diffx, fmt = '-o', markersize = 0.2,  c = cosmiccolor)#  
    ax[1][1].errorbar(bincentersdiffy, hist_diffy/norm_diffy, yerr = np.sqrt(hist_diffy)/norm_diffy, fmt = '-o', markersize = 0.2,  c = cosmiccolor)# 
    ax[1][2].errorbar(bincentersdiffz, hist_diffz/norm_diffz, yerr = np.sqrt(hist_diffz)/norm_diffz, fmt = '-o', markersize = 0.2,  c = cosmiccolor)#
    
    #ax[1][2].axvline(x=-50,color='k', linestyle='--')
    #ax[1][2].axvline(x=50,color='k', linestyle='--')
    #ax[1][2].text(-50, 0.03, "beampipe", rotation=90, verticalalignment='center')
    #ax[1][2].text(50, 0.03, "beampipe", rotation=90, verticalalignment='center') #"""
    
    
    textstrx = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanx,  momentx)
    textstry = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meany,  momenty)
    textstrz = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz,  momentz)
    
    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='white', alpha=1)

    
    # place a text box in upper left in axes coords
    ax[0].text(0.38, 0.97,textstrx, transform=ax[0].transAxes, fontsize=11, verticalalignment='top', bbox=props, ha = 'right')
    ax[1].text(0.38, 0.97, textstry, transform=ax[1].transAxes, fontsize=11, verticalalignment='top', bbox=props, ha = 'right')     
    ax[2].text(0.38, 0.97, textstrz, transform=ax[2].transAxes, fontsize=11, verticalalignment='top', bbox=props, ha = 'right')
    
    for i in range(0,3):    
        #ax[0][i].legend(loc = 'upper right', fontsize = 8)  
        ax[i].set_ylim(0,0.05)  

    plt.savefig("data_vertex_2D_dists_test_av_cosmic.pdf")
  
#############################################################################################################################################
#############################################################################################################################################    
    
def plot_2D_vertex_distributions_pbar(pbars__):


    print "data after cuts:", len(pbars__)
   
 

    ###################################################################################

    minco = -200
    maxco = 200

    rx = [minco,maxco]
    ry = [minco,maxco]
    rz = [minco,maxco]
    
    rx2 = rx
    ry2 = ry
    rz2 = rz
    
    bins00= 155 # 101
    bins = bins00
    
    
    mdp_p__ = pbars__[:,35:38]  # largest cluster

    mdp_p__[:,2] = mdp_p__[:,2]# + 27.060631001371764#+ 5.86916051985#+ 6.17682583139 - 2.096
    #exit(0)

    
    #hist_pz2, edges_pz2 = np.histogram(a=mdp_p[:,2], bins=bins, range=rz)    
    hist_pz, edges_pz = np.histogram(a=mdp_p__[:,2], bins=bins, range=rz)     
    #bincenterspz2   = edges_pz2[:-1]+(edges_pz2[1]-edges_pz2[0])/2.
    bincenterspz   = edges_pz[:-1]+(edges_pz[1]-edges_pz[0])/2.
    #norm_pz2 = 1.0*sum(np.diff(edges_pz2)*hist_pz2)
    norm_pz = 1.0*sum(np.diff(edges_pz)*hist_pz)
    
   
    #hist_px2, edges_px2 = np.histogram(a=mdp_p[:,0], bins=bins, range=rx)   
    hist_px, edges_px = np.histogram(a=mdp_p__[:,0], bins=bins, range=rx)
    #bincenterspx2   = edges_px2[:-1]+(edges_px2[1]-edges_px2[0])/2.
    bincenterspx   = edges_px[:-1]+(edges_px[1]-edges_px[0])/2.
    #norm_px2 = 1.0*sum(np.diff(edges_px2)*hist_px2)
    norm_px = 1.0*sum(np.diff(edges_px)*hist_px)
    
    #hist_py2, edges_py2 = np.histogram(a=mdp_p[:,1], bins=bins, range=ry)    
    hist_py, edges_py = np.histogram(a=mdp_p__[:,1], bins=bins, range=ry)     
    #bincenterspy2   = edges_py2[:-1]+(edges_py2[1]-edges_py2[0])/2.
    bincenterspy   = edges_py[:-1]+(edges_py[1]-edges_py[0])/2.
    #norm_py2 = 1.0*sum(np.diff(edges_py2)*hist_py2)
    norm_py = 1.0*sum(np.diff(edges_py)*hist_py) 
    
    
    ###########################################################################################
    
    diff_x = max(hist_px) - min(hist_px)
    H_M = diff_x/2.0
    binmask = hist_px> H_M
    firstbinabove = bincenterspx[binmask][0]
    lastbinabove = bincenterspx[binmask][-1]
    
    FWHM_x = lastbinabove - firstbinabove
    
    #print firstbinabove
    #print lastbinabove
    
    #print "FWHM X", FWHM_x
    
    ####################################
    
    diff_y = max(hist_py) - min(hist_py)
    H_M = diff_y/2.0
    binmask = hist_py> H_M
    firstbinabove = bincenterspy[binmask][0]
    lastbinabove = bincenterspy[binmask][-1]
    
    FWHM_y = lastbinabove - firstbinabove
    
    #print firstbinabove
    #print lastbinabove
    
    #print "FWHM Y", FWHM_y
    
    diff_z = max(hist_pz) - min(hist_pz)
    H_M = diff_z/2.0
    binmask = hist_pz> H_M
    firstbinabove = bincenterspz[binmask][0]
    lastbinabove = bincenterspz[binmask][-1]
    
    FWHM_z = lastbinabove - firstbinabove

    ###########################################################################################

    from scipy.stats import moment
    
    meanz = nmoment(bincenterspz, hist_pz, 0.0, 1)
    momentz = math.sqrt(nmoment(bincenterspz, hist_pz, meanz, 2))

    meanx = nmoment(bincenterspx, hist_px, 0.0, 1)
    momentx = math.sqrt(nmoment(bincenterspx, hist_px, meanx, 2))

    meany = nmoment(bincenterspy, hist_py, 0.0, 1)
    momenty = math.sqrt(nmoment(bincenterspy, hist_py, meany, 2))

    
    
    
    print meanx, meany, meanz, momentx, momenty, momentz #, FWHM_x, FWHM_y, FWHM_z
  
 
  
    #print " "
    ###########
    
    #norm_c = len(cosmics_)
    #norm_p = len(pbars_) # = 1.0
    
    #fig, ax = plt.subplots(1,3, figsize = (17,5))
    import matplotlib.gridspec as gridspec
    
    fig = plt.figure(figsize = (10,9))
    
    gs = gridspec.GridSpec(2, 2)

    
    ax0 = fig.add_subplot(gs[0, 0]) # row 0, col 0


    ax1 = fig.add_subplot(gs[0, 1]) # row 0, col 1


    ax2 = fig.add_subplot(gs[1, :]) # row 1, span all columns
    
    ############################################################
     
    ax0.errorbar(bincenterspx, hist_px/norm_px, yerr = np.sqrt(hist_px)/norm_px, fmt = '-o', markersize = 0.2, label = "measurements", c = pbarcolor)#
    ax0.set_xlabel("x (mm)", fontsize = 11)
        
    ax1.errorbar(bincenterspy, hist_py/norm_py, yerr = np.sqrt(hist_py)/norm_py, fmt = '-o', markersize = 0.2, label = "measurements", c = pbarcolor)#
    ax1.set_xlabel("y (mm)", fontsize = 11)
                
    ax2.errorbar(bincenterspz, hist_pz/norm_pz, yerr = np.sqrt(hist_pz)/norm_pz, fmt = '-o', markersize = 0.2, label = "measurements", c = pbarcolor)#
    ax2.set_xlabel("z (mm)", fontsize = 11)


    ax2.axvline(x=0,color='k', linestyle='--', alpha = 0.6)


    ax0.axvline(x=-10,color='k', linestyle='--', alpha = 0.6)
    ax0.axvline(x=10,color='k', linestyle='--', alpha = 0.6)
    
    ax0.axvline(x=-100,color='k', linestyle='--', alpha = 0.6)
    ax0.axvline(x=100,color='k', linestyle='--', alpha = 0.6)
    #ax0.axvline(x=-175,color='k', linestyle='--', alpha = 0.6)
    #ax0.axvline(x=175,color='k', linestyle='--', alpha = 0.6)        

    #ax0.text(-44, 0.02, "BGO", rotation=90, verticalalignment='center')
    #ax0.text(28, 0.02, "BGO", rotation=90, verticalalignment='center')
    
    ax0.text(-99, 0.007, "inner bars", rotation=90, verticalalignment='center')
    ax0.text(88, 0.007, "inner bars", rotation=90, verticalalignment='center')
    
    #ax0.text(-172, 0.006, "outer bars", rotation=90, verticalalignment='center')
    #ax0.text(160, 0.006, "outer bars", rotation=90, verticalalignment='center')
    
    
    ax1.axvline(x=-10,color='k', linestyle='--', alpha = 0.6)
    ax1.axvline(x=10,color='k', linestyle='--', alpha = 0.6)
    
    ax1.axvline(x=-100,color='k', linestyle='--', alpha = 0.6)
    ax1.axvline(x=100,color='k', linestyle='--', alpha = 0.6)
    #ax1.axvline(x=-175,color='k', linestyle='--', alpha = 0.6)
    
    ax1.axvline(x=175,color='k', linestyle='--', alpha = 0.6)        

    #ax1.text(-44, 0.02, "BGO", rotation=90, verticalalignment='center')
    #ax1.text(28, 0.02, "BGO", rotation=90, verticalalignment='center')
    
    ax1.text(-99, 0.007, "inner bars", rotation=90, verticalalignment='center')
    ax1.text(88, 0.007, "inner bars", rotation=90, verticalalignment='center')
    
    #ax1.text(-172, 0.006, "outer bars", rotation=90, verticalalignment='center')
    #ax1.text(160, 0.006, "outer bars", rotation=90, verticalalignment='center')
        #ax[0][i].text(50, 0.03, "beampipe", rotation=90, verticalalignment='center')
        
    #ax1.axvline(x=20,color='olivedrab', linestyle='-')
    #ax0.axvline(x=20,color='olivedrab', linestyle='-')         
   
    #ax2.axvline(x=-2.5,color='k', linestyle='--')
    #ax2.axvline(x=2.5,color='k', linestyle='--') 
    #ax2.axvline(x=-150,color='k', linestyle='--')  
    #ax2.axvline(x=150,color='k', linestyle='--')       
    #ax2.text(-148, 0.004, "inner bars", rotation=90, verticalalignment='center')
    #ax2.text(140, 0.004, "inner bars", rotation=90, verticalalignment='center')  
    #ax2.text(5, 0.001, "BGO", rotation=90, verticalalignment='center')
    #ax[i].text(30, 0.0015, "BGO", rotation=90, verticalalignment='center') 
    #ax[1][0].set_title("compare cosmics")
    
    ###########################################################################################
    
    #ax0.plot(x,yp, color = 'black')
    
    #ax0.errorbar(bincentersp, histp/normp, yerr = np.sqrt(histp)/normp, label = 'pbars', color = "red")

    ############################################################################################

    #ax[i].legend(loc = 'upper right', fontsize = 11)          
    ax0.set_ylabel("norm.counts" , fontsize = 11)
    #ax1.set_ylabel("norm.counts" , fontsize = 11)
    ax2.set_ylabel("norm.counts" , fontsize = 11)

       
    textstrx = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanx,  momentx)
    textstry = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meany,  momenty)
    textstrz = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz,  momentz)
    
    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='white', alpha=1)
    
    # place a text box in upper left in axes coords
    ax0.text(0.38, 0.97,textstrx, transform=ax0.transAxes, fontsize=11, color = pbarcolor, verticalalignment='top', bbox=props, ha = 'right')
    ax1.text(0.38, 0.97, textstry, transform=ax1.transAxes, fontsize=11, color = pbarcolor,verticalalignment='top', bbox=props, ha = 'right')     
    ax2.text(0.175, 0.97, textstrz, transform=ax2.transAxes, fontsize=11, color = pbarcolor,verticalalignment='top', bbox=props, ha = 'right')
        
    textstrx = '$\sqrt{var}=%.1f$ mm'%(  momentx)
    textstry = '$\sqrt{var}=%.1f$ mm'%(  momenty)
    textstrz = '$\sqrt{var}=%.1f$ mm'%(  momentz)
    
    # place a text box in upper left in axes coords
    ax0.text(0.38, 0.87,textstrx, transform=ax0.transAxes, fontsize=11, color = pbarcolor, verticalalignment='top', bbox=props, ha = 'right')
    ax1.text(0.38, 0.87, textstry, transform=ax1.transAxes, fontsize=11, color = pbarcolor,verticalalignment='top', bbox=props, ha = 'right')     
    ax2.text(0.175, 0.87, textstrz, transform=ax2.transAxes, fontsize=11, color = pbarcolor,verticalalignment='top', bbox=props, ha = 'right')
        
    for i in [ax0,ax1,ax2]:    
       i.legend(loc = 'upper right', fontsize = 10)
         
    ax0.set_xlim(minco,maxco) 
    ax1.set_xlim(minco,maxco) 
    ax2.set_xlim(minco,maxco) 

    ax0.set_xlim(minco+50,maxco-50) 
    ax1.set_xlim(minco+50,maxco-50) 
    ax2.set_xlim(minco+50,maxco-50) 

    ax0.set_ylim(0,) 
    ax1.set_ylim(0,) 
    ax2.set_ylim(0,)     
     

    plt.savefig("data_vertex_2D_dists_2019_Mo.pdf")
#################################################################################################################################

def plot_2D_vertex_distributions_cos(pbars__):

    minco = -200
    maxco =200

    rx = [minco,maxco]
    ry = [minco,maxco]
    rz = [minco,maxco]
    
    rx2 = rx
    ry2 = ry
    rz2 = rz
    
    #mask_bias = np.logical_and(np.logical_and(pbars_[:,35] < 5, pbars_[:,35] > -5),np.logical_and(pbars_[:,36] < 5, pbars_[:,36] > -5))
    
    #print pbars_[mask_bias]
    
    #np.savetxt("lol.dat", pbars_[mask_bias])

    #mdp_p = pbars_[:,31:34]
    
    print "len pbars before cut: ", len(pbars__)
    
    #pbars__ = pbars__[pbars__[:,38] > 1]
    
    print "len pbars after cut: ", len(pbars__)
  
    mdp_p = pbars__[:,40:43] # simulation
    
    
    
    mdp_p__ = pbars__[:,35:38] # largest cluster
    #mdp_p = pbars__[pbars__[:,4]==2][:,35:38]
    
    #mdp_p__ = pbars__[:,43:46] # vertex fit

   
    #diff_p = pbars__[:,43:46] - pbars__[:,40:43]
    diff_p = pbars__[:,35:38] - pbars__[:,40:43]
    #diff_p = pbars__[:,31:34] - pbars__[:,40:43]
    #diff_p = pbars__[:,28:31] - pbars__[:,40:43]
    
    #print len(mdp_c__[mdp_c__[:,0] != 0]) 
    
    
    
    
    #print mdp_c__[:,0]
    
    #exit(0)
    bins= 90
    
    hist_pz2, edges_pz2 = np.histogram(a=mdp_p[:,2], bins=bins, range=rz)    
    hist_pz, edges_pz = np.histogram(a=mdp_p__[:,2], bins=bins, range=rz)     
    bincenterspz2   = edges_pz2[:-1]+(edges_pz2[1]-edges_pz2[0])/2.
    bincenterspz   = edges_pz[:-1]+(edges_pz[1]-edges_pz[0])/2.
    norm_pz2 = 1.0*sum(np.diff(edges_pz2)*hist_pz2)
    norm_pz = 1.0*sum(np.diff(edges_pz)*hist_pz)
    
    
    hist_px2, edges_px2 = np.histogram(a=mdp_p[:,0], bins=bins, range=rx)   
    hist_px, edges_px = np.histogram(a=mdp_p__[:,0], bins=bins, range=rx)
    bincenterspx2   = edges_px2[:-1]+(edges_px2[1]-edges_px2[0])/2.
    bincenterspx   = edges_px[:-1]+(edges_px[1]-edges_px[0])/2.
    norm_px2 = 1.0*sum(np.diff(edges_px2)*hist_px2)
    norm_px = 1.0*sum(np.diff(edges_px)*hist_px)
    
    
    
    
    #FWHM_y = 
    
    #FWHM_z = 
    
    
    hist_py2, edges_py2 = np.histogram(a=mdp_p[:,1], bins=bins, range=ry)    
    hist_py, edges_py = np.histogram(a=mdp_p__[:,1], bins=bins, range=ry)     
    bincenterspy2   = edges_py2[:-1]+(edges_py2[1]-edges_py2[0])/2.
    bincenterspy   = edges_py[:-1]+(edges_py[1]-edges_py[0])/2.
    norm_py2 = 1.0*sum(np.diff(edges_py2)*hist_py2)
    norm_py = 1.0*sum(np.diff(edges_py)*hist_py) 
    
    
    ################################
    
    diff_x = max(hist_px) - min(hist_px)
    H_M = diff_x/2.0
    binmask = hist_px> H_M
    firstbinabove = bincenterspx[binmask][0]
    lastbinabove = bincenterspx[binmask][-1]
    
    FWHM_x = lastbinabove - firstbinabove
    
    print firstbinabove
    print lastbinabove
    
    print "FWHM X", FWHM_x
    
    ####################################
    
    diff_y = max(hist_py) - min(hist_py)
    H_M = diff_y/2.0
    binmask = hist_py> H_M
    firstbinabove = bincenterspy[binmask][0]
    lastbinabove = bincenterspy[binmask][-1]
    
    FWHM_y = lastbinabove - firstbinabove
    
    print firstbinabove
    print lastbinabove
    
    print "FWHM Y", FWHM_y
    
    diff_z = max(hist_pz) - min(hist_pz)
    H_M = diff_z/2.0
    binmask = hist_pz> H_M
    firstbinabove = bincenterspz[binmask][0]
    lastbinabove = bincenterspz[binmask][-1]
    
    FWHM_z = lastbinabove - firstbinabove
    
    print firstbinabove
    print lastbinabove
    
    print "FWHM Z", FWHM_z
    
    
    
    from scipy.stats import moment
    
    print "MOMENTS OF THE DISTRIBUTION Z -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,2], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,2], moment=2))
    
    #print "own fct "
    meanz = nmoment(bincenterspz, hist_pz, 0.0, 1)
    print meanz
    momentz = math.sqrt(nmoment(bincenterspz, hist_pz, meanz, 2))
    print "sqrt(second moment) ", momentz
    print "-----------------------------------------------------------"
    print "MOMENTS OF THE DISTRIBUTION X -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,0], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,0], moment=2))
    
    ##print "own fct "
    meanx = nmoment(bincenterspx, hist_px, 0.0, 1)
    momentx = math.sqrt(nmoment(bincenterspx, hist_px, meanx, 2))
    print meanx
    print "sqrt(second moment) ", momentx
    print "-----------------------------------------------------------"        
    print "MOMENTS OF THE DISTRIBUTION Y -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,1], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,1], moment=2))
    
    #print "own fct "
    meany = nmoment(bincenterspy, hist_py, 0.0, 1)
    momenty = math.sqrt(nmoment(bincenterspy, hist_py, meany, 2))
    print meany
    print "sqrt(second moment) ", momenty
    print "-----------------------------------------------------------"    
    
    
  
    
    ###########
    
    """hist_diffx, edges_diffx = np.histogram(a=diff_p[:,0], bins=bins, range=rx)      
    bincentersdiffx   = edges_diffx[:-1]+(edges_diffx[1]-edges_diffx[0])/2.   
    norm_diffx = 1.0*sum(np.diff(edges_diffx)*hist_diffx)
  
    
    hist_diffy, edges_diffy = np.histogram(a=diff_p[:,1], bins=bins, range=ry)      
    bincentersdiffy   = edges_diffy[:-1]+(edges_diffy[1]-edges_diffy[0])/2.
    norm_diffy = 1.0*sum(np.diff(edges_diffy)*hist_diffy)
   
    
    hist_diffz, edges_diffz = np.histogram(a=diff_p[:,2], bins=bins, range=rz)       
    bincentersdiffz   = edges_diffz[:-1]+(edges_diffz[1]-edges_diffz[0])/2.    
    norm_diffz = 1.0*sum(np.diff(edges_diffz)*hist_diffz)
   
    ########
    
    print " "
    print " "
    
    print "MOMENTS OF THE DIFFERENCE DISTRIBUTION Z -------------------------------" 
    
    print "first moment ", moment(diff_p[:,2], moment=1)
    print "sqrt(second moment) ", math.sqrt(moment(diff_p[:,2], moment=2))
    
    print "own fct "
    meanz2 = nmoment(bincentersdiffz, hist_diffz, 0.0, 1)
    print meanz2
    print "sqrt(second moment) ", math.sqrt(nmoment(bincentersdiffz, hist_diffz, meanz2, 2))
    print "-----------------------------------------------------------"
    print "MOMENTS OF THE DIFFERENCE DISTRIBUTION X -------------------------------" 
    
    print "first moment ", moment(diff_p[:,0], moment=1)
    print "sqrt(second moment) ", math.sqrt(moment(diff_p[:,0], moment=2))
    
    print "own fct "
    meanx2 = nmoment(bincentersdiffx, hist_diffx, 0.0, 1)
    print meanx2
    print "sqrt(second moment) ", math.sqrt(nmoment(bincentersdiffx, hist_diffx, meanx2, 2))
    print "-----------------------------------------------------------"        
    print "MOMENTS OF THE DIFFERENCE DISTRIBUTION Y -------------------------------" 
    
    print "first moment ", moment(diff_p[:,1], moment=1)
    print "sqrt(second moment) ", math.sqrt(moment(diff_p[:,1], moment=2))
    
    print "own fct "
    meany2 = nmoment(bincentersdiffy, hist_diffy, 0.0, 1)
    print meany2
    print "sqrt(second moment) ", math.sqrt(nmoment(bincentersdiffy, hist_diffy, meany2, 2))
    print "-----------------------------------------------------------"    #"""

    #norm_c = len(cosmics_)
    #norm_p = len(pbars_) # = 1.0
    
    #fig, ax = plt.subplots(1,3, figsize = (17,5))
    import matplotlib.gridspec as gridspec
    
    fig = plt.figure(figsize = (10,9))
    
    gs = gridspec.GridSpec(2, 2)

    
    ax0 = fig.add_subplot(gs[0, 0]) # row 0, col 0


    ax1 = fig.add_subplot(gs[0, 1]) # row 0, col 1


    ax2 = fig.add_subplot(gs[1, :]) # row 1, span all columns

    
    """rz_fit = [-100,100]
    outfitp, histp, bincentersp, normp = fit(bins, mdp_p__[:,2], np.array(rz_fit)) 
    x = np.linspace(rz_fit[0],rz_fit[1],100)
    yp = plotGaussResult(outfitp,x)
    ax[0][2].plot(x,yp, color = 'black')

    rx_fit = [-100,100]
    outfitp, histp, bincentersp, normp = fit(bins, mdp_p__[:,0], np.array(rx_fit)) 
    x = np.linspace(rz_fit[0],rz_fit[1],100)
    yp = plotGaussResult(outfitp,x)
    ax[0][0].plot(x,yp, color = 'black')
    
    
    ry_fit = [-100,100]
    outfitp, histp, bincentersp, normp = fit(bins, mdp_p__[:,1], np.array(ry_fit)) 
    x = np.linspace(rz_fit[0],rz_fit[1],100)
    yp = plotGaussResult(outfitp,x)
    ax[0][1].plot(x,yp, color = 'black')   """ 
  
    #norm_px22 = 1
    #norm_cy2  = 1
    #norm_cz2 = 1
    
    #ax[0][0].set_title("compare pbars")    
       
   
    ax0.errorbar(bincenterspx, hist_px/norm_px, yerr = np.sqrt(hist_px)/norm_px, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = cosmiccolor)#
    ax0.set_xlabel("x (mm)", fontsize = 11)
        
    ax1.errorbar(bincenterspy, hist_py/norm_py, yerr = np.sqrt(hist_py)/norm_py, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = cosmiccolor)#
    ax1.set_xlabel("y (mm)", fontsize = 11)
                
    ax2.errorbar(bincenterspz, hist_pz/norm_pz, yerr = np.sqrt(hist_pz)/norm_pz, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = cosmiccolor)#
    ax2.set_xlabel("z (mm)", fontsize = 11)
        
    #ax[0][2].axvline(x=-5,color='k', linestyle='--')
    #ax[0][2].axvline(x=5,color='k', linestyle='--')
    #ax[0][2].axvline(x=-50,color='k', linestyle='--')
    #ax[0][2].axvline(x=50,color='k', linestyle='--')
    #ax[0][2].text(-50, 0.03, "beampipe", rotation=90, verticalalignment='center')
    #ax[0][2].text(50, 0.03, "beampipe", rotation=90, verticalalignment='center')
       

    ax0.axvline(x=-45,color='k', linestyle='--', alpha = 0.6)
    ax0.axvline(x=45,color='k', linestyle='--', alpha = 0.6)
    ax0.axvline(x=-100,color='k', linestyle='--', alpha = 0.6)
    ax0.axvline(x=100,color='k', linestyle='--', alpha = 0.6)
    ax0.axvline(x=-175,color='k', linestyle='--', alpha = 0.6)
    ax0.axvline(x=175,color='k', linestyle='--', alpha = 0.6)        

    ax0.text(-44, 0.0015, "BGO", rotation=90, verticalalignment='center')
    ax0.text(30, 0.0015, "BGO", rotation=90, verticalalignment='center')
    
    ax0.text(-99, 0.004, "inner bars", rotation=90, verticalalignment='center')
    ax0.text(85, 0.004, "inner bars", rotation=90, verticalalignment='center')
    
    ax0.text(-172, 0.004, "outer bars", rotation=90, verticalalignment='center')
    ax0.text(160, 0.004, "outer bars", rotation=90, verticalalignment='center')
    
    
    ax1.axvline(x=-45,color='k', linestyle='--', alpha = 0.6)
    ax1.axvline(x=45,color='k', linestyle='--', alpha = 0.6)
    ax1.axvline(x=-100,color='k', linestyle='--', alpha = 0.6)
    ax1.axvline(x=100,color='k', linestyle='--', alpha = 0.6)
    ax1.axvline(x=-175,color='k', linestyle='--', alpha = 0.6)
    ax1.axvline(x=175,color='k', linestyle='--', alpha = 0.6)        

    ax1.text(-44, 0.0015, "BGO", rotation=90, verticalalignment='center')
    ax1.text(30, 0.0015, "BGO", rotation=90, verticalalignment='center')
    
    ax1.text(-99, 0.004, "inner bars", rotation=90, verticalalignment='center')
    ax1.text(85, 0.004, "inner bars", rotation=90, verticalalignment='center')
    
    ax1.text(-172, 0.004, "outer bars", rotation=90, verticalalignment='center')
    ax1.text(160, 0.004, "outer bars", rotation=90, verticalalignment='center')
        #ax[0][i].text(50, 0.03, "beampipe", rotation=90, verticalalignment='center')
        
        
   
    ax2.axvline(x=-2.5,color='k', linestyle='--')
    ax2.axvline(x=2.5,color='k', linestyle='--') 
    ax2.axvline(x=-150,color='k', linestyle='--')  
    ax2.axvline(x=150,color='k', linestyle='--')       
    ax2.text(-148, 0.0027, "inner bars", rotation=90, verticalalignment='center')
    ax2.text(140, 0.0027, "inner bars", rotation=90, verticalalignment='center')  
    ax2.text(10, 0.0005, "BGO", rotation=90, verticalalignment='center')
    #ax[i].text(30, 0.0015, "BGO", rotation=90, verticalalignment='center') 
    #ax[1][0].set_title("compare cosmics")
    
  
    #ax[0].errorbar(bincenterspx2, hist_px2/norm_px2, yerr = np.sqrt(hist_px2)/norm_px2, fmt = '-o', markersize = 0.2, label = "simulation", c = cosmiccolor)#
    #ax[1].errorbar(bincenterspy2, hist_py2/norm_py2, yerr = np.sqrt(hist_py2)/norm_py2, fmt = '-o', markersize = 0.2, label = "simulation", c = cosmiccolor)#
    #ax[2].errorbar(bincenterspz2, hist_pz2/norm_pz2, yerr = np.sqrt(hist_pz2)/norm_pz2, fmt = '-o', markersize = 0.2, label = "simulation", c = cosmiccolor)#    
 


        #ax[i].legend(loc = 'upper right', fontsize = 11)          
    ax0.set_ylabel("norm.counts" , fontsize = 11)
    #ax1.set_ylabel("norm.counts" , fontsize = 11)
    ax2.set_ylabel("norm.counts" , fontsize = 11)
        
    """ax[1][1].set_xlabel('y - y$_{sim}$ (mm)')  
    ax[1][2].set_xlabel('z - z$_{sim}$ (mm)')       
    ax[1][0].set_xlabel('x - x$_{sim}$ (mm)')
        
    ax[1][0].errorbar(bincentersdiffx, hist_diffx/norm_diffx, yerr = np.sqrt(hist_diffx)/norm_diffx, fmt = '-o', markersize = 0.2,  c = cosmiccolor)#  
    ax[1][1].errorbar(bincentersdiffy, hist_diffy/norm_diffy, yerr = np.sqrt(hist_diffy)/norm_diffy, fmt = '-o', markersize = 0.2,  c = cosmiccolor)# 
    ax[1][2].errorbar(bincentersdiffz, hist_diffz/norm_diffz, yerr = np.sqrt(hist_diffz)/norm_diffz, fmt = '-o', markersize = 0.2,  c = cosmiccolor)#
    
    #ax[1][2].axvline(x=-50,color='k', linestyle='--')
    #ax[1][2].axvline(x=50,color='k', linestyle='--')
    #ax[1][2].text(-50, 0.03, "beampipe", rotation=90, verticalalignment='center')
    #ax[1][2].text(50, 0.03, "beampipe", rotation=90, verticalalignment='center') #"""
    
    
    textstrx = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanx,  momentx)
    textstry = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meany,  momenty)
    textstrz = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz,  momentz)
    
    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='white', alpha=1)

    
    # place a text box in upper left in axes coords
    ax0.text(0.38, 0.97,textstrx, transform=ax0.transAxes, fontsize=11, verticalalignment='top', bbox=props, ha = 'right')
    ax1.text(0.38, 0.97, textstry, transform=ax1.transAxes, fontsize=11, verticalalignment='top', bbox=props, ha = 'right')     
    ax2.text(0.175, 0.97, textstrz, transform=ax2.transAxes, fontsize=11, verticalalignment='top', bbox=props, ha = 'right')
    
    #for i in range(0,3):    
        #ax[0][i].legend(loc = 'upper right', fontsize = 8)  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("data_vertex_2D_dists_cosmic_tenthFWHM.pdf")
  
#################################################################################################################################
#################################################################################################################################
def plot_covar(pbars):


    vf_covar = pbars[pbars[:,46]!=999]
    #vf_covar = pbars[pbars[:,46]!=999]
    vfit_covar = vf_covar[:,46:49] # vertex fit
    avg_cl_covar = vf_covar[:,49:52] # average of largest cluster
    avg_covar = vf_covar[:,52:55] # average of all
    upper = 400
    rx = [0,upper]
    ry = [0,upper]
    rz = [0,upper]
    
    fig, ax = plt.subplots(3,3, figsize = (13,10))
    
    bins = 50
    
    hist_cx, edges_cx = np.histogram(a=np.sqrt(vfit_covar[:,0]), bins=bins, range=rx)       
    bincentersx   = edges_cx[:-1]+(edges_cx[1]-edges_cx[0])/2.
    norm_cx = 1.0#*sum(np.diff(edges_cx)*hist_cx)

    hist_cy, edges_cy = np.histogram(a=np.sqrt(vfit_covar[:,1]), bins=bins, range=ry)       
    bincentersy   = edges_cy[:-1]+(edges_cy[1]-edges_cy[0])/2.
    norm_cy = 1.0#*sum(np.diff(edges_cy)*hist_cy)    

    hist_cz, edges_cz = np.histogram(a=np.sqrt(vfit_covar[:,2]), bins=bins, range=rz)       
    bincentersz   = edges_cz[:-1]+(edges_cz[1]-edges_cz[0])/2.
    norm_cz = 1.0#*sum(np.diff(edges_cz)*hist_cz) 
    
    
    ax[0][0].errorbar(bincentersx, hist_cx/norm_cx, yerr = np.sqrt(hist_cx)/norm_cx, fmt = '-o', markersize = 0.2, label = 'x vfit', c = 'b') #cosmiccolor)#
    ax[0][1].errorbar(bincentersy, hist_cx/norm_cy, yerr = np.sqrt(hist_cy)/norm_cy, fmt = '-o', markersize = 0.2, label = 'y vfit', c = 'b') #cosmiccolor)#
    ax[0][2].errorbar(bincentersz, hist_cz/norm_cz, yerr = np.sqrt(hist_cz)/norm_cz, fmt = '-o', markersize = 0.2, label = 'z vfit', c = 'b') #cosmiccolor)#
           
    #######################
    
    
    hist_cx, edges_cx = np.histogram(a=np.sqrt(avg_cl_covar[:,0]), bins=bins, range=rx)       
    bincentersx   = edges_cx[:-1]+(edges_cx[1]-edges_cx[0])/2.
    norm_cx = 1.0#*sum(np.diff(edges_cx)*hist_cx)

    hist_cy, edges_cy = np.histogram(a=np.sqrt(avg_cl_covar[:,1]), bins=bins, range=ry)       
    bincentersy   = edges_cy[:-1]+(edges_cy[1]-edges_cy[0])/2.
    norm_cy = 1.0#*sum(np.diff(edges_cy)*hist_cy)    

    hist_cz, edges_cz = np.histogram(a=np.sqrt(avg_cl_covar[:,2]), bins=bins, range=rz)       
    bincentersz   = edges_cz[:-1]+(edges_cz[1]-edges_cz[0])/2.
    norm_cz = 1.0#*sum(np.diff(edges_cz)*hist_cz)   
    
    
    ax[1][0].errorbar(bincentersx, hist_cx/norm_cx, yerr = np.sqrt(hist_cx)/norm_cx, fmt = '-o', markersize = 0.2, label = 'x avg cl', c = 'b') #cosmiccolor)#
    ax[1][1].errorbar(bincentersy, hist_cx/norm_cy, yerr = np.sqrt(hist_cy)/norm_cy, fmt = '-o', markersize = 0.2, label = 'y avg cl', c = 'b') #cosmiccolor)#
    ax[1][2].errorbar(bincentersz, hist_cz/norm_cz, yerr = np.sqrt(hist_cz)/norm_cz, fmt = '-o', markersize = 0.2, label = 'z avg cl', c = 'b') #cosmiccolor)#
         
       
    ##################
    hist_cx, edges_cx = np.histogram(a=np.sqrt(avg_covar[:,0]), bins=bins, range=rx)       
    bincentersx   = edges_cx[:-1]+(edges_cx[1]-edges_cx[0])/2.
    norm_cx = 1.0#*sum(np.diff(edges_cx)*hist_cx)

    hist_cy, edges_cy = np.histogram(a=np.sqrt(avg_covar[:,1]), bins=bins, range=ry)       
    bincentersy   = edges_cy[:-1]+(edges_cy[1]-edges_cy[0])/2.
    norm_cy = 1.0#*sum(np.diff(edges_cy)*hist_cy)    

    hist_cz, edges_cz = np.histogram(a=np.sqrt(avg_covar[:,2]), bins=bins, range=rz)       
    bincentersz   = edges_cz[:-1]+(edges_cz[1]-edges_cz[0])/2.
    norm_cz = 1.0#*sum(np.diff(edges_cz)*hist_cz)   
    
    
    ax[2][0].errorbar(bincentersx, hist_cx/norm_cx, yerr = np.sqrt(hist_cx)/norm_cx, fmt = '-o', markersize = 0.2, label = 'x avg', c = 'b') #cosmiccolor)#
    ax[2][1].errorbar(bincentersy, hist_cx/norm_cy, yerr = np.sqrt(hist_cy)/norm_cy, fmt = '-o', markersize = 0.2, label = 'y avg', c = 'b') #cosmiccolor)#
    ax[2][2].errorbar(bincentersz, hist_cz/norm_cz, yerr = np.sqrt(hist_cz)/norm_cz, fmt = '-o', markersize = 0.2, label = 'z avg', c = 'b') #cosmiccolor)#
      
    #norm_c = len(cosmics_)
    #norm_p = len(pbars_) # = 1.0
    
    
    
    for i in [0,1,2]:
        ax[i][0].set_ylim(0,)
        ax[i][1].set_ylim(0,)
        ax[i][2].set_ylim(0,)
        #ax[2].set_yscale('log')
        ax[i][0].legend() 
        ax[i][1].legend() 
        ax[i][2].legend()      
    
    plt.show()

#################################################################################################################################
#################################################################################################################################
def plot_diff_fwhm_gauss():

    pbars_1 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2.dat")
    pbars_2 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_halfFWHM_clsize_overlap.dat")
    pbars_3 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_fifthFWHM_clsize_overlap.dat")
    pbars_4 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_tenthFWHM_clsize_overlap.dat")
    #pbars_wo = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_nogauss_oct2.dat")


    minco = -150
    maxco =  150

    rzw = [minco,maxco]
    rzwo = [minco,maxco]
             
    p1 = pbars_1[:,35:38] # largest cluster
    p2 = pbars_2[:,35:38] # largest cluster
    p3 = pbars_3[:,35:38] # largest cluster
    p4 = pbars_4[:,35:38] # largest cluster
    
    bins= 80
    
    hist_1, edges_1 = np.histogram(a=p1[:,2], bins=bins, range=rzw)   
    bincenters1   = edges_1[:-1]+(edges_1[1]-edges_1[0])/2.   
    norm_1 = 1.0*sum(np.diff(edges_1)*hist_1)  
    meanz1 = nmoment(bincenters1, hist_1, 0.0, 1)    
    momentz1 = math.sqrt(nmoment(bincenters1, hist_1, meanz1, 2))
    
    hist_2, edges_2 = np.histogram(a=p2[:,2], bins=bins, range=rzwo)       
    bincenters2   = edges_2[:-1]+(edges_2[1]-edges_2[0])/2.
    norm_2 = 1.0*sum(np.diff(edges_2)*hist_2)
    meanz2 = nmoment(bincenters2, hist_2, 0.0, 1)    
    momentz2 = math.sqrt(nmoment(bincenters2, hist_2, meanz2, 2))
   
    hist_3, edges_3 = np.histogram(a=p3[:,2], bins=bins, range=rzwo)       
    bincenters3   = edges_3[:-1]+(edges_3[1]-edges_3[0])/2.
    norm_3 = 1.0*sum(np.diff(edges_3)*hist_3)
    meanz3 = nmoment(bincenters3, hist_3, 0.0, 1)    
    momentz3 = math.sqrt(nmoment(bincenters3, hist_3, meanz3, 2))
    
    hist_4, edges_4 = np.histogram(a=p4[:,2], bins=bins, range=rzwo)       
    bincenters4   = edges_4[:-1]+(edges_4[1]-edges_4[0])/2.
    norm_4 = 1.0*sum(np.diff(edges_4)*hist_4)
    meanz4 = nmoment(bincenters4, hist_4, 0.0, 1)    
    momentz4 = math.sqrt(nmoment(bincenters4, hist_4, meanz4, 2))
   


    fig, ax = plt.subplots()
   
    ax.errorbar(bincenters1, hist_1/norm_1, yerr = np.sqrt(hist_1)/norm_1, fmt = '-o', markersize = 0.2, label = "reconstructed vertex",  lw = 0.9)#
    ax.set_xlabel("z (mm)", fontsize = 12)
        
    ax.errorbar(bincenters2, hist_2/norm_2, yerr = np.sqrt(hist_2)/norm_2, fmt = '-o', markersize = 0.2, label = "reconstructed vertex",  lw = 0.9)#
    ax.set_xlabel("z (mm)", fontsize = 12)
    
    ax.errorbar(bincenters3, hist_3/norm_3, yerr = np.sqrt(hist_3)/norm_3, fmt = '-o', markersize = 0.2, label = "reconstructed vertex",  lw = 0.9)#
    ax.set_xlabel("z (mm)", fontsize = 12)

    ax.errorbar(bincenters4, hist_4/norm_4, yerr = np.sqrt(hist_4)/norm_4, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", lw = 0.9)#
    ax.set_xlabel("z (mm)", fontsize = 12)


    ax.axvline(x=-2.5,color='k', linestyle='--')
    ax.axvline(x=2.5,color='k', linestyle='--') 
    
    
    #ax.axvline(x=110,color='k', linestyle='-') 
    #ax.axvline(x=50,color='k', linestyle='-')
    #ax.axvline(x=20,color='k', linestyle='-')
    #ax[i].axvline(x=-150,color='k', linestyle='--')  
    #ax[i].axvline(x=150,color='k', linestyle='--')       
    #ax[i].text(-148, 0.0027, "inner bars", rotation=90, verticalalignment='center')
    #ax[i].text(140, 0.0027, "inner bars", rotation=90, verticalalignment='center')  
    ax.text(8, 0.0002, "BGO", rotation=90, verticalalignment='center')
             
    ax.set_ylabel("norm.counts" , fontsize = 12)
         
     
    textstr1 = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz1,  momentz1)
    textstr2 = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz2,  momentz2)
    textstr3 = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz3,  momentz3)
    textstr4 = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz4,  momentz4)
    
    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='white', alpha=1)

    ax.text(0.30, 0.97, textstr1, transform=ax.transAxes, fontsize=11,  color = "#1f77b4", verticalalignment='top', bbox=props, ha = 'right')
    ax.text(0.30, 0.80, textstr2, transform=ax.transAxes, fontsize=11,  color = "#ff7f0e", verticalalignment='top', bbox=props, ha = 'right')
    ax.text(0.97, 0.97, textstr3, transform=ax.transAxes, fontsize=11,  color = "#2ca02c", verticalalignment='top', bbox=props, ha = 'right')
    ax.text(0.97, 0.80, textstr4, transform=ax.transAxes, fontsize=11,  color = "#d62728", verticalalignment='top', bbox=props, ha = 'right')
    
    #for i in range(0,3):    
        #ax[0][i].legend(loc = 'upper right', fontsize = 8)  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbardiffFWHM.pdf")

#################################################################################################################################
#################################################################################################################################
def plot_diff_fwhm_gauss_cos():

    pbars_1 = np.loadtxt("./dat_files_3D/test_cosmic_8to17_firstpos_oct2.dat")
    pbars_2 = np.loadtxt("./dat_files_3D/test_cosmic_8to16_firstpos_oct2_tenthFWHM_clsize_overlap.dat")
    
    pbars_1 = pbars_1[pbars_1[:,3]> 0]

    pbars_2 = pbars_2[pbars_2[:,3]> 0]

    minco = -200
    maxco =  200

    rzw = [minco,maxco]
    rzwo = [minco,maxco]
             
    p1 = pbars_1[:,35:38] # largest cluster
    p2 = pbars_2[:,35:38] # largest cluster
    #p3 = pbars_3[:,35:38] # largest cluster
    
    bins= 60
    
    hist_1, edges_1 = np.histogram(a=p1[:,2], bins=bins, range=rzw)   
    bincenters1   = edges_1[:-1]+(edges_1[1]-edges_1[0])/2.   
    norm_1 = 1.0*sum(np.diff(edges_1)*hist_1)  
    meanz1 = nmoment(bincenters1, hist_1, 0.0, 1)    
    momentz1 = math.sqrt(nmoment(bincenters1, hist_1, meanz1, 2))
    
    hist_2, edges_2 = np.histogram(a=p2[:,2], bins=bins, range=rzwo)       
    bincenters2   = edges_2[:-1]+(edges_2[1]-edges_2[0])/2.
    norm_2 = 1.0*sum(np.diff(edges_2)*hist_2)
    meanz2 = nmoment(bincenters2, hist_2, 0.0, 1)    
    momentz2 = math.sqrt(nmoment(bincenters2, hist_2, meanz2, 2))
   
    """hist_3, edges_3 = np.histogram(a=p3[:,2], bins=bins, range=rzwo)       
    bincenters3   = edges_3[:-1]+(edges_3[1]-edges_3[0])/2.
    norm_3 = 1.0*sum(np.diff(edges_3)*hist_3)
    meanz3 = nmoment(bincenters3, hist_3, 0.0, 1)    
    momentz3 = math.sqrt(nmoment(bincenters3, hist_3, meanz3, 2)) """
   
    print "MOMENTS OF THE DISTRIBUTION Z -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,2], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,2], moment=2))
    
    #print "own fct "
    meanz1 = nmoment(bincenters1, hist_1, 0.0, 1)
    print meanz1
    moment1 = math.sqrt(nmoment(bincenters1, hist_1, meanz1, 2))
    print "sqrt(second moment) ", moment1
    print "-----------------------------------------------------------"
    print "MOMENTS OF THE DISTRIBUTION X -------------------------------" 
    
    #print "first moment ", moment(mdp_p__[:,0], moment=1)
    #print "sqrt(second moment) ", math.sqrt(moment(mdp_p__[:,0], moment=2))
    
    ##print "own fct "
    meanz2 = nmoment(bincenters2, hist_2, 0.0, 1)
    moment2 = math.sqrt(nmoment(bincenters2, hist_2, meanz2, 2))
    print meanz2
    print "sqrt(second moment) ", moment2
    print "-----------------------------------------------------------"        
    print "MOMENTS OF THE DISTRIBUTION Y -------------------------------" 
    
    


    fig, ax = plt.subplots()
   
    ax.errorbar(bincenters1, hist_1/norm_1, yerr = np.sqrt(hist_1)/norm_1, fmt = '-o', markersize = 0.2, label = "exp. FWHM", c = cosmiccolor)#
    ax.set_xlabel("z (mm)", fontsize = 12)
        
    ax.errorbar(bincenters2, hist_2/norm_2, yerr = np.sqrt(hist_2)/norm_2, fmt = '-o', markersize = 0.2, label = "tenth of exp. FWHM", c = "goldenrod")#
    ax.set_xlabel("z (mm)", fontsize = 12)
    
    #ax.errorbar(bincenters3, hist_3/norm_3, yerr = np.sqrt(hist_3)/norm_3, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = "blue")#
    #ax.set_xlabel("z (mm)", fontsize = 12)

    ax.axvline(x=-2.5,color='k', linestyle='--')
    ax.axvline(x=2.5,color='k', linestyle='--') 
    
    
    #ax.axvline(x=110,color='k', linestyle='-') 
    #ax.axvline(x=50,color='k', linestyle='-')
    #ax.axvline(x=20,color='k', linestyle='-')
    #ax[i].axvline(x=-150,color='k', linestyle='--')  
    #ax[i].axvline(x=150,color='k', linestyle='--')       
    #ax[i].text(-148, 0.0027, "inner bars", rotation=90, verticalalignment='center')
    #ax[i].text(140, 0.0027, "inner bars", rotation=90, verticalalignment='center')  
    ax.text(8, 0.0002, "BGO", rotation=90, verticalalignment='center')
             
    ax.set_ylabel("norm.counts" , fontsize = 12)
         
     
    textstr1 = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz1,  moment1)
    textstr2 = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz2,  moment2)
    #textstr3 = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanz3,  moment3)
    
    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='white', alpha=1)

    ax.text(0.30, 0.97, textstr1, transform=ax.transAxes, fontsize=11, color = cosmiccolor, verticalalignment='top', bbox=props, ha = 'right')
    ax.text(0.30, 0.80, textstr2, transform=ax.transAxes, fontsize=11, color = "goldenrod", verticalalignment='top', bbox=props, ha = 'right')
    #ax.text(0.30, 0.63, textstr3, transform=ax.transAxes, fontsize=11, color = "blue", verticalalignment='top', bbox=props, ha = 'right')
    
    
    ax.legend()
    
    #for i in range(0,3):    
        #ax[0][i].legend(loc = 'upper right', fontsize = 8)  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("cos_diffFWHM.pdf")
    

#################################################################################################################################
from sklearn.neighbors import KernelDensity

def mode_(nums):
    corresponding={}
    occurances=[]
    for i in nums:
            count = nums.count(i)
            corresponding.update({i:count})

    for i in corresponding:
            freq=corresponding[i]
            occurances.append(freq)

    maxFreq=max(occurances)

    keys=corresponding.keys()
    values=corresponding.values()

    index_v = values.index(maxFreq)
    global mode
    mode = keys[index_v]
    return mode
#################################################################################################################################
def plot_diff_track_nrs_covar():

    pbars_orig1 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_covarweight_testcut.dat")
    #pbars_orig2 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_covarweight_tenthFWHM.dat")
    from matplotlib.ticker import MaxNLocator

    ax = plt.figure().gca()

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    cuts_ = [2,3,4,5,6]
    
    labels_ = []
    
    ry = [0,200]
    
    bins = 60
    
    #colors_ = ["#1f77b4", "#ff7f0e", "#2ca02c"]  
    #colors_ = ["black", "crimson", "darkgoldenrod", "grey"] 
    
    save_arr = []
    
    for n,i in enumerate(cuts_):
             
        p1 =   pbars_orig1[pbars_orig1[:,56]==i] # number of tracks in vertex
        #p1 =   pbars_orig1[pbars_orig1[:,27]==i] # number of tracks total
        
        print len(p1)
          
        p1 = p1[p1[:,51]!=999]
        p1 = p1[p1[:,51]>=0]
        
        print len(p1)
        
        p1 = np.sqrt(p1[:,51])
        
        p1 = p1[p1<400]
        
        hist_, edges_ = np.histogram(a=p1, bins=bins, range=ry)    
        bincenters   = edges_[:-1]+(edges_[1]-edges_[0])/2.
        norm_ = 1.0*sum(np.diff(edges_)*hist_)
        ################################
        
        diff_x = max(hist_) - min(hist_)
        H_M = diff_x/2.0
        binmask = hist_> H_M
        firstbinabove = bincenters[binmask][0]
        lastbinabove = bincenters[binmask][-1]
        
        FWHM = lastbinabove - firstbinabove
        
        print FWHM#"""
        
        range1 = 0
        range2 = 200   
        r = np.array([range1,range2])
        
        p1 = p1.reshape(len(p1), 1)
        
        X_plot = np.linspace(0, 200, len(p1))[:,np.newaxis]
        print X_plot.shape
       
        # Gaussian KDE
        sigma = np.std(p1)
        bandw_rule_thumb = (4.0*sigma**5/(3.0*len(p1)))**(1.0/5.0) 
        print "bandw: ", bandw_rule_thumb 
        kde = KernelDensity(kernel='gaussian', bandwidth=bandw_rule_thumb).fit(p1)
        log_dens = kde.score_samples(X_plot)
        
        
        print "mode: ", (X_plot[np.argmax(np.exp(log_dens))])
        modee = X_plot[np.argmax(np.exp(log_dens))][0]
             
        p1_m = np.mean(p1)
        p1_m2 = np.median(p1)
        p1_err = np.std(p1)
        
        
        save_arr.append([p1_m, p1_m2])

      
    save_arr = np.array(save_arr)
       
    ax.set_ylabel('mean of $\sqrt{\overline{C}(33)}$', color='black')
    #ax.tick_params('y', colors='black')        
    ax.errorbar(cuts_, save_arr[:,0], fmt = 'v', color = "black", markersize = 8)#
    ax.set_ylim(35,60)
    #ax2 = ax.twinx()

    #ax2.set_ylabel('median of $\sqrt{\overline{C}(33)}$', color='crimson')
    #ax2.tick_params('y', colors='crimson')
    #ax2.errorbar(cuts_, save_arr[:,1], fmt = '+', color = "crimson", markersize = 7)#
    #ax2.set_ylim(30,40)
    
    
        #p2 =   pbars_orig2[pbars_orig2[:,27]==i]   
        #p2 = p2[p2[:,51]!=999]
             
        #p2_m = np.mean(np.sqrt(p2[:,51]))
        #p2_err = np.std(np.sqrt(p2[:,51]))
       
        #ax.errorbar(i, p2_m,  fmt = 'x', color = "gray", markersize = 5)#
        
    ax.set_xlabel("number of tracks in vertex", fontsize = 12)
    plt.tight_layout()        
   
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbardifftracknr_covar.pdf")

#################################################################################################################################
def plot_diff_track_nrs_covar_width():

    pbars_orig1 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_covarweight_testcut.dat")
    #pbars_orig2 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_covarweight_tenthFWHM.dat")
    from matplotlib.ticker import MaxNLocator

    ax = plt.figure().gca()

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    cuts_ = [2,3,4,5,6]
    
    labels_ = []
    
    ry = [0,200]
    
    bins = 60
    
    #colors_ = ["#1f77b4", "#ff7f0e", "#2ca02c"]  
    #colors_ = ["black", "crimson", "darkgoldenrod", "grey"] 
    
    save_arr = []
    
    for n,i in enumerate(cuts_):
             
        p1 =   pbars_orig1[pbars_orig1[:,56]==i] # number of tracks in vertex
        #p1 =   pbars_orig1[pbars_orig1[:,27]==i] # number of tracks total
        
        print len(p1)
          
        p1 = p1[p1[:,51]!=999]
        p1 = p1[p1[:,51]>=0]
        
        print len(p1)
        
        p1 = np.sqrt(p1[:,51])
        p1 = p1[p1<400]
       
        
        hist_, edges_ = np.histogram(a=p1, bins=bins, range=ry)    
        bincenters   = edges_[:-1]+(edges_[1]-edges_[0])/2.
        norm_ = 1.0*sum(np.diff(edges_)*hist_)
        ################################
        
        diff_x = max(hist_) - min(hist_)
        H_M = diff_x/2.0
        binmask = hist_> H_M
        firstbinabove = bincenters[binmask][0]
        lastbinabove = bincenters[binmask][-1]
        
        FWHM = lastbinabove - firstbinabove
        
        print FWHM#"""
        
        range1 = 0
        range2 = 200   
        r = np.array([range1,range2])
        
        p1 = p1.reshape(len(p1), 1)
        
        X_plot = np.linspace(0, 200, len(p1))[:,np.newaxis]
        print X_plot.shape
       
        # Gaussian KDE
        sigma = np.std(p1)
        bandw_rule_thumb = (4.0*sigma**5/(3.0*len(p1)))**(1.0/5.0) 
        print "bandw: ", bandw_rule_thumb 
        kde = KernelDensity(kernel='gaussian', bandwidth=bandw_rule_thumb).fit(p1)
        log_dens = kde.score_samples(X_plot)
        
        
        print "mode: ", (X_plot[np.argmax(np.exp(log_dens))])
        modee = X_plot[np.argmax(np.exp(log_dens))][0]
             
        p1_m = np.mean(p1)
        p1_m2 = np.median(p1)
        #p1_err = np.std(p1)
        
        
        save_arr.append([sigma, FWHM])

      
    save_arr = np.array(save_arr)
       
    ax.set_ylabel('$\sigma$ of $\sqrt{\overline{C}(33)}$')
    #ax.tick_params('y', colors='teal')        
    ax.errorbar(cuts_, save_arr[:,0], fmt = 'x', color = 'teal', markersize = 8)#
    ax.set_ylim(20,60)
    #ax2 = ax.twinx()

    #ax2.set_ylabel('FWHM of $\sqrt{\overline{C}(33)}$', color='peru')
    #ax2.tick_params('y', colors='peru')
    #ax2.errorbar(cuts_, save_arr[:,1], fmt = 'd', color = 'peru', markersize = 5)#
    #ax2.set_ylim(5,30)
    
    
        #p2 =   pbars_orig2[pbars_orig2[:,27]==i]   
        #p2 = p2[p2[:,51]!=999]
             
        #p2_m = np.mean(np.sqrt(p2[:,51]))
        #p2_err = np.std(np.sqrt(p2[:,51]))
       
        #ax.errorbar(i, p2_m,  fmt = 'x', color = "gray", markersize = 5)#
        
    ax.set_xlabel("number of tracks in vertex", fontsize = 12)
    plt.tight_layout()        
   
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbardifftracknr_covar_width.pdf")

#################################################################################################################################


def plot_diff_track_nrs():

    pbars_orig = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2.dat")
    
    fig, ax = plt.subplots(figsize = (7,5))
    
    bins= 30
    minco = -300
    maxco =  300

    rzw = [minco,maxco]
    rzwo = [minco,maxco]
    
    cuts_ = [1,3,6,9,100]
    
    labels_ = []
    
    #colors_ = ["#1f77b4", "#ff7f0e", "#2ca02c"]  
    colors_ = ["black", "crimson", "darkgoldenrod", "grey"] 
    
    for n,i in enumerate(colors_):
             
        p1 =   pbars_orig[np.logical_and(pbars_orig[:,38]>=cuts_[n], pbars_orig[:,38]<cuts_[n+1] )]   
             
        p1 = p1[:,35:38] # largest cluster
        
        print " "
        print "len", len(p1)
        print np.mean(p1[np.logical_and(p1[:,2]>minco, p1[:,2]<maxco)][:,2]), np.std(p1[np.logical_and(p1[:,2]>minco, p1[:,2]<maxco)][:,2])
        
        hist_1, edges_1 = np.histogram(a=p1[:,2], bins=bins, range=rzw)   
        bincenters1   = edges_1[:-1]+(edges_1[1]-edges_1[0])/2.   
        norm_1 = 1.0*sum(np.diff(edges_1)*hist_1)  
        meanz1 = nmoment(bincenters1, hist_1, 0.0, 1)    
        momentz1 = math.sqrt(nmoment(bincenters1, hist_1, meanz1, 2))
        
        labels_.append(momentz1)
        print i, meanz1, momentz1
       
        ax.errorbar(bincenters1, hist_1/norm_1, yerr = np.sqrt(hist_1)/norm_1, fmt = '-o', color = colors_[n], markersize = 0.2, label  = "# of tracks: " + "{}".format(i),  lw = 1)#
        ax.set_xlabel("z (mm)", fontsize = 12)
            
   

    ax.axvline(x=-2.5,color='k', linestyle='--')
    ax.axvline(x=2.5,color='k', linestyle='--') 
    
    
    #ax.axvline(x=110,color='k', linestyle='-') 
    #ax.axvline(x=50,color='k', linestyle='-')
    #ax.axvline(x=20,color='k', linestyle='-')
    #ax[i].axvline(x=-150,color='k', linestyle='--')  
    #ax[i].axvline(x=150,color='k', linestyle='--')       
    #ax[i].text(-148, 0.0027, "inner bars", rotation=90, verticalalignment='center')
    #ax[i].text(140, 0.0027, "inner bars", rotation=90, verticalalignment='center')  
    ax.text(8, 0.0002, "BGO", rotation=90, verticalalignment='center')
             
    ax.set_ylabel("norm.counts" , fontsize = 12)
         
   
         
    for n,(i,c) in enumerate(zip(labels_,colors_)):
     
        textstr1 = '$\sqrt{var}=%.1f$ mm'%(i)
        
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='white', alpha=1)

        ax.text(0.26, 0.97-n*0.08, textstr1, transform=ax.transAxes, fontsize=11,  color = c, verticalalignment='top', bbox=props, ha = 'right')
        """ax.text(0.30, 0.80, textstr2, transform=ax.transAxes, fontsize=11,  color = "#ff7f0e", verticalalignment='top', bbox=props, ha = 'right')
        ax.text(0.97, 0.97, textstr3, transform=ax.transAxes, fontsize=11,  color = "#2ca02c", verticalalignment='top', bbox=props, ha = 'right')
        ax.text(0.97, 0.80, textstr4, transform=ax.transAxes, fontsize=11,  color = "#d62728", verticalalignment='top', bbox=props, ha = 'right')"""
    
    #for i in range(0,3):    
    ax.legend()  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbardifftracknr.pdf")

#################################################################################################################################
#################################################################################################################################
def histogram_intersection(h1, h2, bins_):
   bins_ = np.diff(bins_)
   sm = 0
   for i in range(len(bins_)):
       sm += min(bins_[i]*h1[i], bins_[i]*h2[i])
   return sm
#################################################################################################################################
#################################################################################################################################
def plot_diff_wall_pos():

    pbars_0 = np.loadtxt("./dat_files_3D/test_pbars_0_0_firstpos_gauss_oct2_covarweight_testcut_tenthFWHM.dat")
    pbars_1 = np.loadtxt("./dat_files_3D/test_pbars_intowall_189_firstpos_gauss_oct2_covarweight_testcut_tenthFWHM.dat")
    pbars_2 = np.loadtxt("./dat_files_3D/test_pbars_intowall_195_firstpos_gauss_oct2_covarweight_testcut_tenthFWHM.dat")
    pbars_3 = np.loadtxt("./dat_files_3D/test_pbars_intowall_198_firstpos_gauss_oct2_covarweight_testcut_tenthFWHM.dat")
    pbars_4 = np.loadtxt("./dat_files_3D/test_pbars_intowall_185_firstpos_gauss_oct2_covarweight_testcut.dat")
    #pbars_wo = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_nogauss_oct2.dat")
    
    pbars__ = [pbars_0, pbars_1, pbars_2, pbars_3]#, pbars_4]

    norm0 = 3000
    norm1 = 10000
    norm2 = 6000
    norm3 = 6000
    norm4 = 8000
    
    nums__ = [3000, 10000, 6000, 6000, 8000]
    
    pos__ = [0, 110, 50, 20, 150]
    
    print len(pbars_0), len(pbars_1), len(pbars_2), len(pbars_3), len(pbars_4)
    
    pbars_0 = pbars_0[pbars_0[:,27]>1]
    pbars_1 = pbars_1[pbars_1[:,27]>1]
    pbars_2 = pbars_2[pbars_2[:,27]>1]
    pbars_3 = pbars_3[pbars_3[:,27]>1]
    pbars_4 = pbars_4[pbars_4[:,27]>1]    
    
    pbars_0 = pbars_0[pbars_0[:,35]!=999]
    pbars_1 = pbars_1[pbars_1[:,35]!=999]
    pbars_2 = pbars_2[pbars_2[:,35]!=999]
    pbars_3 = pbars_3[pbars_3[:,35]!=999]
    pbars_4 = pbars_4[pbars_4[:,35]!=999]
    
    print len(pbars_0), len(pbars_1), len(pbars_2), len(pbars_3), len(pbars_4)
    
    
    #pbars_0 = pbars_0[pbars_0[:,3]>0]
    #pbars_1 = pbars_1[pbars_1[:,3]>0]
    #pbars_2 = pbars_2[pbars_2[:,3]>0]
    #pbars_3 = pbars_3[pbars_3[:,3]>0]
    #pbars_4 = pbars_4[pbars_4[:,3]>0]
    
    colors_ = [pbarcolor, "goldenrod", "grey", "black", "blue"]

    labels_ = []
    minco = -200
    maxco = 200
    fig, ax = plt.subplots()
    for c, (p_,n_,pos_) in enumerate(zip(pbars__, nums__, pos__)):

        rz = [pos_ - maxco, pos_ + maxco]
        #rz = [minco, maxco]
                 
        p0 = p_[:,35:38] # largest cluster
        #p0[:,2] = p0[:,2] - pos_
        
        p0 = p0[np.logical_and(p0[:,2]>rz[0], p0[:,2]<rz[1])]
       
       
        print len(p0)
        #norm0 = len(p1)
        #norm2 = len(p2)
        #norm3 = len(p3)
        #norm4 = len(p4)
        
        bins= 70
        #bins = np.arange(np.floor(p1[:,2].min()),np.ceil(p1[:,2].max()))

        hist_0, edges_0 = np.histogram(a=p0[:,2], bins=bins, range=rz)       
        bincenters0   = edges_0[:-1]+(edges_0[1]-edges_0[0])/2.
        norm_0 = 1.0*sum(np.diff(edges_0)*hist_0)
        meanz0 = nmoment(bincenters0, hist_0, 0.0, 1)    
        momentz0 = math.sqrt(nmoment(bincenters0, hist_0, meanz0, 2))

        if c == 0:
            save1 = hist_0/norm_0
            save2 = edges_0
            
        if c>0:    
            print "intersect", histogram_intersection(save1, hist_0/norm_0, save2)
            
            
            
        labels_.append(momentz0)
        
        #x_ = bincenters1[np.argmax(hist_1)] # x of highest bin
        
        """print np.percentile(p0[:,2], 25) - np.percentile(p0[:,2], 75)
        print np.percentile(p0[:,2], 10) - np.percentile(p0[:,2], 90)
        print np.percentile(p0[:,2], 50), meanz0
        print ""
        #print np.sum(hist_1/norm_1*np.diff(edges_1)) #"""

        err_0 = np.sqrt(hist_0)/n_
        hist_0 = hist_0/n_    
       
        ax.errorbar(bincenters0, hist_0, yerr = err_0, fmt = '-o', markersize = 0.2, label = str(pos_), c = colors_[c])#
        
        
        
    ax.legend(loc = "right")    
    ax.set_xlabel("z (mm)", fontsize = 12)

    ax.axvline(x=-2.5,color='k', linestyle='--')
    ax.axvline(x=2.5,color='k', linestyle='--') 
    
    ax.axvline(x=160,color='k', linestyle='-') 
    ax.axvline(x=110,color='k', linestyle='-') 
    ax.axvline(x=50,color='k', linestyle='-')
    ax.axvline(x=20,color='k', linestyle='-')
    
    ax.axvline(x=-150,color='k', linestyle='--')  
    ax.axvline(x=150,color='k', linestyle='--')       
    #ax.text(-148, 0.015, "inner bars", rotation=90, verticalalignment='center')
    #ax.text(140, 0.015, "inner bars", rotation=90, verticalalignment='center')  
    #ax.text(-17, 0.015, "BGO", rotation=90, verticalalignment='center')
             
    ax.set_ylabel("norm.counts" , fontsize = 12)
    
    for n,(i,c) in enumerate(zip(labels_,colors_)):
     
        textstr1 = '$\sqrt{var}=%.1f$ mm'%(i)
        
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='white', alpha=1)

        ax.text(0.28, 0.97-n*0.08, textstr1, transform=ax.transAxes, fontsize=11,  color = c, verticalalignment='top', bbox=props, ha = 'right')
    
         
     
    #textstrzw = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanzw,  momentzw)
    #textstrzwo = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanzwo,  momentzwo)
    
    # these are matplotlib.patch.Patch properties
    #props = dict(boxstyle='round', facecolor='white', alpha=1)

    #ax.text(0.30, 0.97, textstrzw, transform=ax.transAxes, fontsize=11, color = pbarcolor, verticalalignment='top', bbox=props, ha = 'right')
    #ax.text(0.30, 0.80, textstrzwo, transform=ax.transAxes, fontsize=11, color = "goldenrod", verticalalignment='top', bbox=props, ha = 'right')
    
    #for i in range(0,3):    
        #ax[0][i].legend(loc = 'upper right', fontsize = 8)  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbardiff_pos.pdf")
    
    plt.show()

#################################################################################################################################
#################################################################################################################################
def plot_diff_hitpos_on_BGO():

    pbars_1 = np.loadtxt("./dat_files_3D/test_pbars_-1_5_-1_5_firstpos_gauss_oct2.dat")
    pbars_2 = np.loadtxt("./dat_files_3D/test_pbars_0_0_firstpos_gauss_oct2.dat")
    pbars_3 = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2.dat")
    #pbars_wo = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_nogauss_oct2.dat")


    minco = -100
    maxco = 100

    rzw = [minco,maxco]
    rzwo = [minco,maxco]
             
    p1 = pbars_1[:,35:38] # largest cluster
    p2 = pbars_2[:,35:38] # largest cluster
    p3 = pbars_3[:,35:38] # largest cluster
    
    bins= 50
    
    hist_1, edges_1 = np.histogram(a=p1[:,0], bins=bins, range=rzw)   
    bincenters1   = edges_1[:-1]+(edges_1[1]-edges_1[0])/2.   
    norm_1 = 1.0*sum(np.diff(edges_1)*hist_1)  
    meanz1 = nmoment(bincenters1, hist_1, 0.0, 1)    
    momentz1 = math.sqrt(nmoment(bincenters1, hist_1, meanz1, 2))
    
    hist_2, edges_2 = np.histogram(a=p2[:,0], bins=bins, range=rzwo)       
    bincenters2   = edges_2[:-1]+(edges_2[1]-edges_2[0])/2.
    norm_2 = 1.0*sum(np.diff(edges_2)*hist_2)
    meanz2 = nmoment(bincenters2, hist_2, 0.0, 1)    
    momentz2 = math.sqrt(nmoment(bincenters2, hist_2, meanz2, 2))
   
    hist_3, edges_3 = np.histogram(a=p3[:,0], bins=bins, range=rzwo)       
    bincenters3   = edges_3[:-1]+(edges_3[1]-edges_3[0])/2.
    norm_3 = 1.0*sum(np.diff(edges_3)*hist_3)
    meanz3 = nmoment(bincenters3, hist_3, 0.0, 1)    
    momentz3 = math.sqrt(nmoment(bincenters3, hist_3, meanz3, 2))
   


    fig, ax = plt.subplots()
   
    ax.errorbar(bincenters1, hist_1/norm_1, yerr = np.sqrt(hist_1)/norm_1, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = pbarcolor)#
    ax.set_xlabel("y (mm)", fontsize = 12)
        
    ax.errorbar(bincenters2, hist_2/norm_2, yerr = np.sqrt(hist_2)/norm_2, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = "goldenrod")#
    ax.set_xlabel("y (mm)", fontsize = 12)
    
    ax.errorbar(bincenters3, hist_3/norm_3, yerr = np.sqrt(hist_3)/norm_3, fmt = '-o', markersize = 0.2, label = "reconstructed vertex", c = "blue")#
    ax.set_xlabel("y (mm)", fontsize = 12)

    ax.axvline(x=-45,color='k', linestyle='--')
    ax.axvline(x=45,color='k', linestyle='--') 
    
    
    #ax.axvline(x=110,color='k', linestyle='-') 
    #ax.axvline(x=50,color='k', linestyle='-')
    #ax.axvline(x=20,color='k', linestyle='-')
    #ax[i].axvline(x=-150,color='k', linestyle='--')  
    #ax[i].axvline(x=150,color='k', linestyle='--')       
    #ax[i].text(-148, 0.0027, "inner bars", rotation=90, verticalalignment='center')
    #ax[i].text(140, 0.0027, "inner bars", rotation=90, verticalalignment='center')  
    #ax.text(8, 0.0005, "BGO", rotation=90, verticalalignment='center')
             
    ax.set_ylabel("norm.counts" , fontsize = 12)
         
     
    #textstrzw = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanzw,  momentzw)
    #textstrzwo = '$\mu=%.1f$ mm\n$\sqrt{var}=%.1f$ mm'%(meanzwo,  momentzwo)
    
    # these are matplotlib.patch.Patch properties
    #props = dict(boxstyle='round', facecolor='white', alpha=1)

    #ax.text(0.30, 0.97, textstrzw, transform=ax.transAxes, fontsize=11, color = pbarcolor, verticalalignment='top', bbox=props, ha = 'right')
    #ax.text(0.30, 0.80, textstrzwo, transform=ax.transAxes, fontsize=11, color = "goldenrod", verticalalignment='top', bbox=props, ha = 'right')
    
    #for i in range(0,3):    
        #ax[0][i].legend(loc = 'upper right', fontsize = 8)  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbar_hitpos_bgo_y.pdf")
    
#################################################################################################################################
################################################################################################################################# 





#################################################################################################################################
#################################################################################################################################
def plot_diff_cuts():

    pbars_orig = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2_covarweight_testcut.dat")
    
    fig, ax = plt.subplots(figsize = (7,5))
    
    bins= 25
    minco = -200
    maxco =  200

    rzw = [minco,maxco]
    rzwo = [minco,maxco]
    
    cuts_ = [[0, 30],[30, 70], [70,200]]
    
    labels_ = []
    
    #colors_ = ["#1f77b4", "#ff7f0e", "#2ca02c"]  
    colors_ = ["black", "crimson", "darkgoldenrod", "grey"] 
    
    for n,i in enumerate(cuts_):
             
        p1 =   pbars_orig[np.logical_and(pbars_orig[:,39]<i[1], pbars_orig[:,39]>i[0])]   
             
        p1 = p1[:,35:38] # largest cluster
        
        
        
        print np.mean(p1[np.logical_and(p1[:,2]>minco, p1[:,2]<maxco)][:,2]), np.std(p1[np.logical_and(p1[:,2]>minco, p1[:,2]<maxco)][:,2])
        
        hist_1, edges_1 = np.histogram(a=p1[:,2], bins=bins, range=rzw)   
        bincenters1   = edges_1[:-1]+(edges_1[1]-edges_1[0])/2.   
        norm_1 = 1.0*sum(np.diff(edges_1)*hist_1)  
        meanz1 = nmoment(bincenters1, hist_1, 0.0, 1)    
        momentz1 = math.sqrt(nmoment(bincenters1, hist_1, meanz1, 2))
        
        labels_.append(momentz1)
        print i, meanz1, momentz1
       
        ax.errorbar(bincenters1, hist_1/norm_1, yerr = np.sqrt(hist_1)/norm_1, fmt = '-o', color = colors_[n], markersize = 0.2, label  = "$\overline{d}_{cl}$ $\in$" + "{}".format(i),  lw = 1)#
        ax.set_xlabel("z (mm)", fontsize = 12)
            
   

    ax.axvline(x=-2.5,color='k', linestyle='--')
    ax.axvline(x=2.5,color='k', linestyle='--') 
    
    
    #ax.axvline(x=110,color='k', linestyle='-') 
    #ax.axvline(x=50,color='k', linestyle='-')
    #ax.axvline(x=20,color='k', linestyle='-')
    #ax[i].axvline(x=-150,color='k', linestyle='--')  
    #ax[i].axvline(x=150,color='k', linestyle='--')       
    #ax[i].text(-148, 0.0027, "inner bars", rotation=90, verticalalignment='center')
    #ax[i].text(140, 0.0027, "inner bars", rotation=90, verticalalignment='center')  
    ax.text(8, 0.0002, "BGO", rotation=90, verticalalignment='center')
             
    ax.set_ylabel("norm.counts" , fontsize = 12)
         
   
         
    for n,(i,c) in enumerate(zip(labels_,colors_)):
     
        textstr1 = '$\sqrt{var}=%.1f$ mm'%(i)
        
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='white', alpha=1)

        ax.text(0.26, 0.97-n*0.08, textstr1, transform=ax.transAxes, fontsize=11,  color = c, verticalalignment='top', bbox=props, ha = 'right')
        """ax.text(0.30, 0.80, textstr2, transform=ax.transAxes, fontsize=11,  color = "#ff7f0e", verticalalignment='top', bbox=props, ha = 'right')
        ax.text(0.97, 0.97, textstr3, transform=ax.transAxes, fontsize=11,  color = "#2ca02c", verticalalignment='top', bbox=props, ha = 'right')
        ax.text(0.97, 0.80, textstr4, transform=ax.transAxes, fontsize=11,  color = "#d62728", verticalalignment='top', bbox=props, ha = 'right')"""
    
    #for i in range(0,3):    
    ax.legend()  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbardiffmeandist.pdf")
    
    plt.show()

#################################################################################################################################
#################################################################################################################  
def plot_diff_covars():

    pbars_orig = np.loadtxt("./dat_files_3D/test_pbars_2_2_firstpos_gauss_oct2.dat")
    
    fig, ax = plt.subplots(figsize = (7,5))
    
    bins= 30
    minco = -200
    maxco =  200

    rzw = [minco,maxco]
    rzwo = [minco,maxco]
    
    cuts_ = [[0, 30],[30, 50], [50, 100]]
    
    labels_ = []
    
    #colors_ = ["#1f77b4", "#ff7f0e", "#2ca02c"]  
    colors_ = ["black", "crimson", "darkgoldenrod", "grey"] 
    
    for n,i in enumerate(cuts_):
    
        p1 = np.sqrt(pbars_orig[:,51])
        p2 = pbars_orig[:,35:38]
             
        p2 =   p2[np.logical_and(p1<i[1], p1>i[0])]   
             
        #print np.mean(p1[np.logical_and(p1[:,2]>minco, p1[:,2]<maxco)][:,2]), np.std(p1[np.logical_and(p1[:,2]>minco, p1[:,2]<maxco)][:,2])
        
        hist_1, edges_1 = np.histogram(a=p2[:,2], bins=bins, range=rzw)   
        bincenters1   = edges_1[:-1]+(edges_1[1]-edges_1[0])/2.   
        norm_1 = 1.0*sum(np.diff(edges_1)*hist_1)  
        meanz1 = nmoment(bincenters1, hist_1, 0.0, 1)    
        momentz1 = math.sqrt(nmoment(bincenters1, hist_1, meanz1, 2))
        
        labels_.append(momentz1)
        print i, meanz1, momentz1
        
        ax.errorbar(bincenters1, hist_1/norm_1, yerr = np.sqrt(hist_1)/norm_1, fmt = '-o', color = colors_[n], markersize = 0.2, label = "sqrt of covar $\in$" + "{}".format(i),  lw = 1)#
        ax.set_xlabel("z (mm)", fontsize = 12)
            
   

    ax.axvline(x=-2.5,color='k', linestyle='--')
    ax.axvline(x=2.5,color='k', linestyle='--') 
    
    
    #ax.axvline(x=110,color='k', linestyle='-') 
    #ax.axvline(x=50,color='k', linestyle='-')
    #ax.axvline(x=20,color='k', linestyle='-')
    #ax[i].axvline(x=-150,color='k', linestyle='--')  
    #ax[i].axvline(x=150,color='k', linestyle='--')       
    #ax[i].text(-148, 0.0027, "inner bars", rotation=90, verticalalignment='center')
    #ax[i].text(140, 0.0027, "inner bars", rotation=90, verticalalignment='center')  
    ax.text(8, 0.0002, "BGO", rotation=90, verticalalignment='center')
             
    ax.set_ylabel("norm.counts" , fontsize = 12)
         
   
         
    for n,(i,c) in enumerate(zip(labels_,colors_)):
     
        textstr1 = '$\sqrt{var}=%.1f$ mm'%(i)
        
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='white', alpha=1)

        ax.text(0.26, 0.97-n*0.08, textstr1, transform=ax.transAxes, fontsize=11,  color = c, verticalalignment='top', bbox=props, ha = 'right')
        """ax.text(0.30, 0.80, textstr2, transform=ax.transAxes, fontsize=11,  color = "#ff7f0e", verticalalignment='top', bbox=props, ha = 'right')
        ax.text(0.97, 0.97, textstr3, transform=ax.transAxes, fontsize=11,  color = "#2ca02c", verticalalignment='top', bbox=props, ha = 'right')
        ax.text(0.97, 0.80, textstr4, transform=ax.transAxes, fontsize=11,  color = "#d62728", verticalalignment='top', bbox=props, ha = 'right')"""
    
    #for i in range(0,3):    
    ax.legend()  
    #    ax[i].set_ylim(0,0.05)  

    plt.savefig("pbardiff_covars.pdf")


##############################################################################################################################
def plot_nr_points_how_often_cos():
    #pbars_orig = np.loadtxt("./dat_files_3D/test_pbars_0_0_firstpos_gauss_oct2_covarweight.dat")
    #pbars_orig = np.loadtxt("./dat_files_3D/test_pbars_0_0_firstpos_gauss_oct2_covarweight_tenthFWHM.dat")
    
    pbars_orig = np.loadtxt("./dat_files_3D/test_cosmics_8to14_covarweight_testcut.dat")
    #pbars_orig2 = np.loadtxt("./dat_files_3D/test_cosmics_8to14_firstpos_gauss_oct2_covarweight_tenthFWHM.dat")
    #pbars_orig2 = np.loadtxt("./dat_files_3D/test_cosmics_8to14_firstpos_oct2_tenthFWHM_clsize_overlap.dat")
    
    norm = len(pbars_orig)
    #norm2 = len(pbars_orig2)
    
    p1 = pbars_orig[pbars_orig[:,3] >= 3] 
    #p2 = pbars_orig2[pbars_orig2[:,3] >= 3] 

    p1[p1[:,38]==999] = 0
    #p2[p2[:,38]==999] = 0
    
    
    
    should_X = [0, 1, 2, 3, 4, 5, 6]
    should_Y = [ 33+343, 652,0,  41,0,0,   4 ] # for zero points, i add the bins of 0 and 1 track
    
    unique, counts = np.unique(p1[:,38], return_counts=True)
    #unique2, counts2 = np.unique(p2[:,38], return_counts=True)
    print np.asarray((unique, counts)).T
    
    
    #mask_ = np.isin(unique, should_X)
    
    #unique_2 = unique[mask_]
    
    #counts_2 = counts[mask_]
    
    #print unique_2

    from matplotlib.ticker import MaxNLocator

    ax = plt.figure().gca()

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    #fig, ax = plt.subplots()
    ax.set_ylabel("counts" , fontsize = 12)
    ax.set_xlabel("number of points in cluster" , fontsize = 12)
    #ax.errorbar(should_X, should_Y - counts_2, marker = "x", markersize = 3, capsize = 2,elinewidth =1)#, color = "indianred")
   
    ax.errorbar(unique, np.array(counts), yerr = np.sqrt(np.array(counts)), marker = "D", markersize = 3, capsize = 2,elinewidth =1, color = "black", label = "found by tracking")
    #ax.errorbar(unique2, np.array(counts2), yerr = np.sqrt(np.array(counts2)), marker = "D", markersize = 3, capsize = 2,elinewidth =1, color = "grey", label = "found by tracking (tenth exp. FWHM$_{bar}$)")
    ax.errorbar(should_X, np.array(should_Y), yerr = np.sqrt(np.array(should_Y)), marker = "x", markersize = 3, capsize = 2,elinewidth =1, color = "indianred", label = "combinatorics")
    #ax.errorbar(should_X, np.array(should_Y2), yerr = np.sqrt(np.array(should_Y2)), marker = "x", markersize = 3, capsize = 2,elinewidth =1, color = "crimson", label = "combinatorics2")
    
    ax.set_ylim(0,)
    ax.set_xlim(0,6)
    plt.legend()
    plt.savefig("cluster_points_hist_cosmic.pdf")
    plt.show()
###############################################################################################################################

def plot_nr_points_how_often():
    pbars_orig = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts_final/timepix_17_december_pbar_all_smaller_errors_eps90_2.dat")
    #pbars_orig = np.loadtxt("./dat_files_3D/test_pbars_0_0_firstpos_gauss_oct2_covarweight_tenthFWHM.dat")
    
    #pbars_orig = np.loadtxt("./dat_files_3D/test_cosmic_8to14_firstpos_oct2.dat")
    #pbars_orig2 = np.loadtxt("./dat_files_3D/test_cosmics_8to14_firstpos_oct2_tenthFWHM_clsize_overlap.dat")
    
    norm = len(pbars_orig)
    #norm2 = len(pbars_orig2)
    
    p1 = pbars_orig#[pbars_orig[:,3] >= 3] 
    #p2 = pbars_orig#[pbars_orig2[:,3] >= 3] 

    p1[p1[:,38]==999] = 0
    #p2[p2[:,38]==999] = 0
    
    should_X = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,21]
    #should_Y = [383+6, 682, 0, 758, 0, 0, 529, 0, 0, 0, 303, 0, 0, 0, 0, 144, 0, 0, 0, 0, 0, 59]
    
    #should_Y = [423, 961,0, 741,0,0, 394,0,0,0, 240,0,0,0,0, 102 ,0,0,0,0,0, 41] # tracks in vertex

    should_Y = [4+298, 6669, 0, 7589,0, 0,4536,0,0,0, 1505,0,0,0,0, 353,0,0,0,0,0,   68]
    
    
    
    should_Y = [3909, 7819,0, 8265,0,0, 4814,0,0,0, 1629,0,0,0,0,  368,0,0,0,0,0,   78]
    #should_Y2 = [ 16+349, 684, 0,789,0,0, 553,0,0,0, 293,0,0,0,0, 156,0,0,0,0,0, 55] # all found tracks 
    
    
    #should_X = [0, 1, 2, 3, 4, 5, 6]
    #should_Y = [403+39, 518, 0, 35, 0, 0, 5] # for zero points, i add the bins of 0 and 1 track
    
    unique, counts = np.unique(p1[:,38], return_counts=True)
    #unique2, counts2 = np.unique(p2[:,38], return_counts=True)
    print np.asarray((unique, counts)).T
    
    
    #mask_ = np.isin(unique, should_X)
    
    #unique_2 = unique[mask_]
    
    #counts_2 = counts[mask_]
    
    #print unique_2

    from matplotlib.ticker import MaxNLocator

    ax = plt.figure().gca()

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    #fig, ax = plt.subplots()
    ax.set_ylabel("counts" , fontsize = 12)
    ax.set_xlabel("number of points in cluster" , fontsize = 12)
    #ax.errorbar(should_X, should_Y - counts_2, marker = "x", markersize = 3, capsize = 2,elinewidth =1)#, color = "indianred")
   
    ax.errorbar(unique, np.array(counts), yerr = np.sqrt(np.array(counts)), marker = "D", markersize = 3, capsize = 2,elinewidth =1, color = "black", label = "found by 3D tracking")
    #ax.errorbar(unique2, np.array(counts2), yerr = np.sqrt(np.array(counts2)), marker = "D", markersize = 3, capsize = 2,elinewidth =1, color = "grey", label = "found by tracking (tenth exp. FWHM$_{bar}$)")
    #ax.errorbar(should_X, np.array(should_Y), yerr = np.sqrt(np.array(should_Y)), marker = "x", markersize = 3, capsize = 2,elinewidth =1, color = "indianred", label = "combinatorics")
    ax.errorbar(should_X, np.array(should_Y), yerr = np.sqrt(np.array(should_Y)), marker = "x", markersize = 3, capsize = 2,elinewidth =1, color = "crimson", label = "combinatorics")
    
    ax.set_ylim(0,)
    ax.set_xlim(0,20)
    plt.legend()
    plt.savefig("cluster_points_hist_pbar_data.pdf")
    plt.show()


##############################################################################################################################
###############################################################################################################################

if __name__ == '__main__':

            
    pbardata_orig = []
        
    for n, filei in enumerate(argv[1:]):
        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_orig = data_orig
        else: 
            pbardata_orig = np.vstack((pbardata_orig, data_orig))
    

    
    plot_2D_vertex_distributions_pbar(pbardata_orig)  
    
    #plot_2Dhist_vertex_compare_pbar(pbardata_orig)
    #plot_diff_fwhm_gauss()
    #  
    #########################################################
    
    #plot_diff_cuts()
    #plot_nr_points_how_often_cos()
    #plot_nr_points_how_often()
    
    #plot_diff_track_nrs_covar()
    #plot_diff_track_nrs_covar_width()
    plt.show()
    
    
    #plot_nr_points_how_often()
    #unique, counts = np.unique(pbars1[:,38], return_counts=True)
    #print np.asarray((unique, counts)).T
    #plot_diff_track_nrs()
    #plt.show()
    #plot_diff_wall_pos()
    #plot_diff_fwhm_gauss()
    
    #plot_diff_cuts()
    #plot_diff_covars()
    







    
