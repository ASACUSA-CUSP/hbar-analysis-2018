#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

from sys           import argv
from scipy         import stats

import math


import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt



#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


################################################################################################################################


if __name__ == '__main__':


    pbardata_orig = []
        
    for n, filei in enumerate(argv[1:]):
        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_orig = data_orig
        else: 
            pbardata_orig = np.vstack((pbardata_orig, data_orig))
    
    ##############################################################################################

    dists1 = pbardata_orig[pbardata_orig[:,3]>0]
    
    fig, ax = plt.subplots()
    ax.patch.set_facecolor='w'

        
    bins = 70
    for dists, lab in zip([dists1],["timepix measurements, Mo"]):#, dists1, dists2,dists3], ["exp. FWHM", "half exp. FWHM", "fifth exp. FWHM", "tenth exp. FWHM"]):
    
        #dists = dists[dists[:,38]>2]
               
        print "len ", len(dists)
        dists = dists[dists[:,51]!=999]
        dists = dists[dists[:,37] < 200]
        dists = dists[dists[:,37] > -200]
        #dists = dists[dists[:,27]==4]

        
        print "len2 ", len(dists)
     
        if lab  == "G4 simulations":
            dists = np.sqrt(dists[:,51])
        else:
            dists = np.sqrt(dists[:,48])
        
        dists = dists[dists<1000]
        
        print lab, "mean", np.mean(dists)
        print lab, "median", np.median(dists)
        print " "
        
        
        y,binEdges = np.histogram(dists,bins=bins, range = [0,120])
        bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
        menStd     = np.sqrt(y)
        width      = 100.0/(bins*1.0)
        norm = 1.0*sum(np.diff(binEdges)*y)
        plt.xlim(0,100)
        plt.ylabel("norm. counts", fontsize = 12)
        plt.xlabel("$\sqrt{\overline{C}(33)}$", fontsize = 12)
        #plt.bar(bincenters, y/norm, width=width, alpha = 0.2, yerr=menStd/norm, edgecolor = 'black', label = "exp. FWHM", lw = 0.5, error_kw=dict(ecolor='black', lw = 1, capsize = 5, capthick=1))
        if lab == "G4 simulations":
            c = "gray"
        else:
            c = '#990001'
        plt.errorbar(bincenters, y/norm, yerr=menStd/norm, lw = 1, label = lab, color =  c)#, color = "black")
        ax.set_ylim(0,)
        ax.set_xlim(0,120)
        plt.legend(fontsize = 11)
    plt.tight_layout()    
    plt.savefig("l_covar_x_hist_all_exp_tpx_2019.pdf")
    plt.show()
   

    #fig2, ax2 = plt.subplots()
    #ax2.patch.set_facecolor='w'
    #ax2.set_yscale('log')    
   
    #vertex_x = events_cut # pbardata_orig[:,20]
    #vertex_y = events_cut # pbardata_orig[:,21]
    
    
    #plt.hist(dists2, bins=100, range = (0, 100),normed = True) 
    #plt.savefig("2016_data_BGO_vertex_16bins_log.pdf", facecolor=fig.get_facecolor())


    
    
  
