#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import os
import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':
    
    pbardata_orig0 = [] 
    pbardata_orig1 = [] 
    filenames_0 = []
    filenames_1 = []
   
    basepath = "/home/bernadette/Dropbox/hbar_cuts_final/data_files_tpx_Mo/"
    for entry in os.listdir(basepath):
        if os.path.isfile(os.path.join(basepath, entry)):
            filenames_0.append(basepath + entry)
            
    basepath = "/home/bernadette/Dropbox/hbar_cuts_final/data_files_tpx_Au/"
    for entry in os.listdir(basepath):
        if os.path.isfile(os.path.join(basepath, entry)):
            filenames_1.append(basepath + entry)
            #print basepath + entry
    
               
    for n, filei in enumerate(filenames_0):        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        #print filei
        #print data_orig.shape
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_orig0 = data_orig
        else: 
            pbardata_orig0 = np.vstack((pbardata_orig0, data_orig))

    for n, filei in enumerate(filenames_1):        
        data_orig = np.loadtxt(filei)   
        fist_ts_i = data_orig[0,-1]
        data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
        if n == 0:        
            pbardata_orig1 = data_orig
        else: 
            pbardata_orig1 = np.vstack((pbardata_orig1, data_orig))            
   
    """all_actual_cosmic = 282935.0 # half break 1... hodoscope+BGO trigger
    cosmic_time = 603659.0 # in seconds, half break 1... hodoscope+BGO trigger
   
    pbar_time = 4*(102.0 - 9.0) # 4 seconds, 102 runs, 9 empty
    area_hit = math.pi*45.0*0.5*25.0*0.5
    area_BGO = math.pi*45*45 #- area_hit
        
    print "all in file cosmic: ", len(pbardata_orig1), all_actual_cosmic
    print "all in file pbar: ", len(pbardata_orig0)"""
    
    #BGO_cut = 0
    Track_cut = 0   
    pbardata_orig1 = pbardata_orig1[pbardata_orig1[:,6]>=Track_cut]
    pbardata_orig0 = pbardata_orig0[pbardata_orig0[:,6]>=Track_cut]

    """estimated_cosmics = len(pbardata_orig1)*1.0/cosmic_time*pbar_time # scale to pbar time
    estimated_cosmics = estimated_cosmics*area_hit/area_BGO # scale to hit area    
    print "estimated cosmics scaled to pbar time and hit area", estimated_cosmics    
    print "estimated cosmics in test set (1/3)", estimated_cosmics*0.33"""
    
    
    fig, ax = plt.subplots()
    
    range1 = -0.5
    range2 = 6.5
    binnumber = 7
    
    ax.set_xlim([range1, 7])
    
    tr_pbar0_plot = pbardata_orig0[:,27]
    tr_pbar1_plot = pbardata_orig1[:,27]
    
    y0, bin_edges = np.histogram(tr_pbar0_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm1 = 1.0*sum(np.diff(bin_edges)*y0)
    
    err1 = np.sqrt(y0)/norm1
    y0 = y0/norm1
            
    y1, bin_edges = np.histogram(tr_pbar1_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm2 = 1.0*sum(np.diff(bin_edges)*y1)
    
    err2 = np.sqrt(y1)/norm2
    y1 = y1/norm2   
    
    index = np.arange(7)
    bar_width = 0.35
    error_config = {'ecolor': '0.3'}
    
    rects1 = plt.bar(bin_centers - 0.5*bar_width, y1, bar_width,
                 color="darkgray",
                 yerr=err2,
                 #error_kw=error_config,
                 label='$\overline{p}$ Mo' )#"""

    rects2 = plt.bar(bin_centers + 0.5*bar_width, y0, bar_width,                 
                 color="gold",
                 yerr=err1,
                 #error_kw=error_config,
                 label='$\overline{p}$ Au' )
    
    
    plt.xticks(index + bar_width / 2, ('0', '1', '2', '3', '4', '5', '6'))
    
    # compare pbar and cosmics: ##########################################################################################
    #n, bins, patches = ax.hist( [tr_pbar0_plot, tr_pbar1_plot], yerr= 1, histtype='bar',
    #    bins = binnumber, range=(range1, range2), align='mid', label = ["$\overline{p}$","cosmics"], normed = True, color = [pbarcolor, cosmiccolor]) 
    ######################################################################################################################
    
    ax.legend()
    
    plt.xlabel("number of 3D tracks")
    plt.ylabel("norm. counts")
    
    ##################################################################################################################
    
    
    plt.savefig('number_of_tracks_tpx_pbars_2019.pdf') #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

