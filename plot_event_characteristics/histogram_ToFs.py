#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats


import re

import matplotlib 
matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats
from scipy.odr import ODR, Model, RealData

from scipy.optimize import fsolve, fmin, minimize, brute



pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


def findIntersection(func1, func2, x0):
    return fsolve(lambda x: func1(x) - func2(x), x0)


def gauss(c,X):
    #c[0]..amplitude
    #c[1]..location
    #c[2]..sigma    

    #return c[0]*(stats.cosine.pdf(x=X, loc=c[1], scale=c[2]))
    return c[0]*(stats.norm.pdf(x=X, loc=c[1], scale=c[2]))

    
def plotGaussResult2(output,X):
    return gauss(output,X)

def plotGaussResult(output,X):
    c = output.beta
    return gauss(c,X)
    

def fit(data, r):
    # data is unbinned
    # r is a list for the range
    
    
    binSize = 2*(np.percentile(data,75)- np.percentile(data,25))/(len(data)**(1./3.)) # Freedmann-Diaconis
    nBins   = (r[1]-r[0])/binSize  


    print "hier 1 "
    print nBins
    print data.shape
    #weights = np.ones_like(data)/float(len(data))

    hist, edges = np.histogram(a=data, bins=int(nBins)+1, range=r)   
    hist = hist.astype(np.float) 
    #print "sum: ",
    #histsum = hist.sum()     
    
    norm = 1.0*sum(np.diff(edges)*hist) # area   
            
    
    cut = hist > 0.	

    
    #print "hier 1.0 "
    binCentre   = edges[:-1]+(edges[1]-edges[0])/2.
    #print bin

    hist = hist[cut]
    #edges = edges[cut]
    binCentre = binCentre[cut]

    model       = Model(gauss)
    #print "hier 2 "

    xErrors     = np.ones_like(binCentre)*binSize/2.
    yErrors     = np.sqrt(hist)     # Poisson error



    #yErrors[yErrors==0.] = 999999999999 # ignore everything with zero counts
    

    
    fitData     = RealData(x=binCentre, y=hist/norm, sx=xErrors, sy=yErrors/norm)

    #print "hier 3 "
    start_vals = []
    #vals1 = [0,2]
            

    #start_vals = [700,100,122,700,200,122] # bar 0 outer
    
    start_vals = [np.max(hist)/norm, binCentre[np.argmax(hist)], np.std(data)*0.90] #, np.max(hist), binCentre[np.argmax(hist)], np.std(data)]
            
    #start_vals = [700, 200, 150, 400, 50, 150]
    #1 2 
              
    
    print "....................................................................................... ", start_vals
        
    #myodr = ODR(fitData, model, beta0=np.array([np.max(hist) , binCentre[np.argmax(hist)], np.std(data), np.max(hist), binCentre[np.argmax(hist)], np.std(data)]))
    myodr = ODR(fitData, model, beta0=np.array(start_vals))
    #print "-------------------- ", barnr, start_vals_center, np.max(hist), std_val
    
    #myodr = ODR(fitData, model, beta0=np.array([np.max(hist), start_vals_center[0] , std_val, np.max(hist), start_vals_center[1] , std_val]))
    #myodr.set_job(fit_type=2)
    myoutput = myodr.run()
    myoutput.pprint()
    

    print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"     #myoutput.pprint() # optional
    
    #"Quasi-chi-squared" is defined to be the [total weighted sum of squares]/dof
    #	i.e. same as numpy.sum((residual/adjusted_err)**2)/dof or
    #       numpy.sum(((output.xplus-x)/x_sigma)**2
    #                                +((y_data-output.y)/y_sigma)**2)/dof
    #	Converges to the conventional chi-squared for zero x uncertainties.
      
    return myoutput, hist, binCentre, norm
    #plt.errorbar(binCentre[:-1], hist,xerr=xErrors, yerr=np.sqrt(hist),fmt='o')
    #x=np.linspace(r[0], r[1], 20000)
    #plt.plot( x,plotGaussResult(myoutput,x))



#################################################################################################################################


if __name__ == '__main__':

            if len(argv) < 2:
                print "usage: ", argv[0], "txt file"
                exit(-1)
        
            print "HI!"
            


        
            #test_array = np.loadtxt(argv[1])[:,1]
            
            cosmics1 = np.loadtxt(argv[1])     
            cosmics1 = cosmics1[cosmics1[:,3]>3]       
            cosmics1 = cosmics1[cosmics1[:,6]>1]
            
            pbars1 = np.loadtxt(argv[2]) 
            pbars1 = pbars1[pbars1[:,3]>3]           
            pbars1 = pbars1[pbars1[:,6]>1]            
            
            
            mtds_pbars1 = []
            mtds_pbars2 = []
            mtds_cosmics1 = []
            mtds_cosmics2 = []
            mtds_cosmics_min_max = []
            mtds_cosmics_std = []
            mtds_pbars_min_max = []
            mtds_pbars_std = []            
            angles_cosmic = []

            nr_of_tr_c = []
            nr_of_tr_p = []

            for cand in cosmics1:
                tracks = cand[6]
                #bgo_E = cand[3]
                #mtd_combi = cand[11]
                #mtd_version2 = cand[12]
                mtd_std = cand[16]
                mtd_min_max = cand[17]
                angle = cand[7:10]
                #angles_cosmic.append(angle)#print mtd_combi
                nr_of_tr_c.append(tracks)
                #if min(angle) < 20:

                #mtds_cosmics2.append(mtd_version2)
                if not np.isnan(mtd_std):
                    mtds_cosmics_min_max.append(mtd_min_max)
                    mtds_cosmics_std.append(mtd_std)                
                #mtds_cosmics1.append(mtd_combi)
               # mtds_cosmics2.append(mtd_version2)

            for cand in pbars1:
                tracks = cand[6]
                #bgo_E = cand[3]
                #mtd_combi = cand[11]
                #mtd_version2 = cand[12]
                mtd_std = cand[16]
                mtd_min_max = cand[17]
                angle = cand[7:10]
                nr_of_tr_p.append(tracks)
                #if min(angle) < 20:
                #mtds_pbars2.append(mtd_version2)
                #mtds_pbars1.append(mtd_combi)
                if not np.isnan(mtd_std):
                    mtds_pbars_min_max.append(mtd_min_max)
                    mtds_pbars_std.append(mtd_std)                 
                #mtds_pbars2.append(mtd_version2)


            #mtds_pbars1 = np.array(mtds_pbars1)
            #mtds_pbars2 = np.array(mtds_pbars2)
            #mtds_cosmics1 = np.array(mtds_cosmics1)
            #mtds_cosmics2 = np.array(mtds_cosmics2)
            mtds_pbars_min_max = np.array(mtds_pbars_min_max)
            mtds_pbars_std  = np.array(mtds_pbars_std)
            mtds_cosmics_min_max = np.array(mtds_cosmics_min_max)
            mtds_cosmics_std  = np.array(mtds_cosmics_std)
            
            
            mtds_cosmic_plot = mtds_cosmics_std
            #mtds_cosmic_plot2 = mtds_cosmics_min_max
            
            mtds_pbar_plot = mtds_pbars_std
            #mtds_pbar_plot2 = mtds_pbars_min_max
            

            print mtds_cosmics_std


            
            range1 = 0.0
            range2 = 1.2
        
            
           # outfit, hist, bincenters = fit(np.array(test_array), np.array([range1,range2]))
            outfitp, histp, bincentersp, normp = fit(np.array(mtds_pbar_plot), np.array([range1,range2]))

            print "#########################################"
            #print "cosmics: "
            #print outfit.beta[0], outfit.sd_beta[0]
            #print outfit.beta[1], outfit.sd_beta[1]
            #print outfit.beta[2], outfit.sd_beta[2]
            print "pbars: "
            print outfitp.beta[0], outfitp.sd_beta[0]
            print outfitp.beta[1], outfitp.sd_beta[1]
            print outfitp.beta[2], outfitp.sd_beta[2]

            print " "
            print "FWHM: ", outfitp.beta[2]*2.35*1000, "ps"
            #print outfit.res_var, outfitp.res_var
            print "#########################################"
            
            
          
            fig, ax = plt.subplots()
            x = np.linspace(range1,range2,100)
            #y = plotGaussResult(outfit,x)
            yp = plotGaussResult(outfitp,x)
            #ax.plot(x,y)
            ax.plot(x,yp, color = 'black')
            #ax.errorbar(bincenters, hist, yerr = np.sqrt(hist))
            ax.errorbar(bincentersp, histp/normp, yerr = np.sqrt(histp)/normp, label = 'pbars', color = pbarcolor)
            plt.xlabel("tof (ns)")
            plt.ylabel("counts")
            
            ##################################################################################################################
            outfitc, histc, bincentersc, normc = fit(np.array(mtds_cosmic_plot), np.array([range1,range2]))

            print "#########################################"
            #print "cosmics: "
            #print outfit.beta[0], outfit.sd_beta[0]
            #print outfit.beta[1], outfit.sd_beta[1]
            #print outfit.beta[2], outfit.sd_beta[2]
            print "cosmics: "
            print outfitc.beta[0], outfitc.sd_beta[0]
            print outfitc.beta[1], outfitc.sd_beta[1]
            print outfitc.beta[2], outfitc.sd_beta[2]

            print " "
            print "FWHM: ", outfitc.beta[2]*2.35*1000, "ps"
            #print outfit.res_var, outfitp.res_var
            print "#########################################"
            
            
            x = np.linspace(range1,range2,100)
            #y = plotGaussResult(outfit,x)
            yc = plotGaussResult(outfitc,x)
            #ax.plot(x,y)
            ax.plot(x,yc, color = 'black')
            #ax.errorbar(bincenters, hist, yerr = np.sqrt(hist))
            ax.errorbar(bincentersc, histc/normc, yerr = np.sqrt(histc)/normc, label = 'cosmics', color = cosmiccolor)
            plt.xlabel("ToF (ns)")
            plt.ylabel("norm. counts")
            #legend = ax.legend(loc='upper right')
            
            
            textstrp = '$\overline{p}$:\n$\mu=%.3f$ ns\n$\sigma=%.3f$ ns'%(outfitp.beta[1],  outfitp.beta[2])
            textstrc = 'cosmics:\n$\mu=%.3f$ ns\n$\sigma=%.3f$ ns'%(outfitc.beta[1],  outfitc.beta[2])

            # these are matplotlib.patch.Patch properties
            props = dict(boxstyle='round', facecolor='white', alpha=0.5)

            ax.set_xlim(0.0,1.2)
            ax.set_ylim(0.0,3.4)
            
            # place a text box in upper left in axes coords
            ax.text(0.97, 0.97,textstrp, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props, ha = 'right', color = pbarcolor)
            ax.text(0.97, 0.77, textstrc, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props, ha = 'right', color = cosmiccolor)
            
            
            
            plt.savefig('ToF_mtd_via_std_new.pdf') #, facecolor=fig.get_facecolor())
            #fig,close()
            plt.show()
    
        #thefile.close()   

