#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import os
import logging 
mpl_logger = logging.getLogger('matplotlib') 
mpl_logger.setLevel(logging.WARNING) 

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


#################################################################################################################################


if __name__ == '__main__':
    
    element = "carbon" 
    
    barplot_ = False
    errorbars_ = True

    tracks3D = True
    NrHits_inner = False
    NrHits_outer = False
    #####################################################################################
    
    pbardata_data = []     
    filenames_data = []
    
    if element != "carbon":   
    	if element == "gold":
    		ele_name = "Gold"
        else:
        	ele_name = "Molybdenum"
           
        basepath = "../data_files_tpx_" + ele_name + "/"
        for entry in os.listdir(basepath):
            if os.path.isfile(os.path.join(basepath, entry)):
                filenames_data.append(basepath + entry)
                  
        for n, filei in enumerate(filenames_data):        
            data_orig = np.loadtxt(filei)   
            fist_ts_i = data_orig[0,-1]
            #print filei
            #print data_orig.shape
            data_orig = data_orig[np.logical_and( np.absolute(data_orig[:,-1] - fist_ts_i) < 61, np.absolute(data_orig[:,-1] - fist_ts_i) > 35)]
            if n == 0:        
                pbardata_data = data_orig
            else: 
                pbardata_data = np.vstack((pbardata_data, data_orig))

    else:
        pbardata_data = np.loadtxt("../data_file_tpx_Carbon/timepix_17_december_pbars_all.dat")
        ts_0 = pbardata_data[0][-1]
        print "ts_0", ts_0
        print len(pbardata_data)
        pbardata_data = pbardata_data[np.logical_and(np.absolute(pbardata_data[:,-1] - ts_0) < 50, np.absolute(pbardata_data[:,-1] - ts_0) > 30)]
        print len(pbardata_data)
    
    #pbardata_ftf_bic = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts_final/simulations_tracking/data_files_results/tracking_results_g4_ftf_"+element+"_0_foilpos-26_8mm_detailed_tpx.dat")
    #pbardata_chips = np.loadtxt("/home/bernadette/Dropbox/hbar_cuts_final/simulations_tracking/data_files_results/tracking_results_g4_chips_"+element+"_0_foilpos-26_8mm_detailed_tpx.dat")           
   
    pbardata_ftf_bic = np.loadtxt("../simulations_tracking/all_tracking_results_converted_g4_ftf_bic_" + element + "_foilpos-26_8mm_all.dat")
    pbardata_ftfp_bert = np.loadtxt("../simulations_tracking/all_tracking_results_converted_g4_ftfp_bert_" + element + "_foilpos-26_8mm_all.dat")
    pbardata_chips = np.loadtxt("../simulations_tracking/all_tracking_results_converted_g4_qgsp_bert_chips_" + element + "_foilpos-26_8mm_all.dat")           
   
   
    #####################################################################################    
    #BGO_cut = 0
    Track_cut = 0   
    pbardata_ftf_bic = pbardata_ftf_bic[pbardata_ftf_bic[:,27]>=Track_cut]
    pbardata_ftfp_bert = pbardata_ftfp_bert[pbardata_ftfp_bert[:,27]>=Track_cut]
    pbardata_chips = pbardata_chips[pbardata_chips[:,27]>=Track_cut]
    pbardata_data = pbardata_data[pbardata_data[:,27]>=Track_cut]
    
    pbardata_ftf_bic = pbardata_ftf_bic[pbardata_ftf_bic[:,27]!=999]
    pbardata_ftfp_bert = pbardata_ftfp_bert[pbardata_ftfp_bert[:,27]!=999]
    pbardata_chips = pbardata_chips[pbardata_chips[:,27]!=999]
    pbardata_data = pbardata_data[pbardata_data[:,27]!=999]
    
    pbardata_ftf_bic = pbardata_ftf_bic[pbardata_ftf_bic[:,3]>0]
    pbardata_ftfp_bert = pbardata_ftfp_bert[pbardata_ftfp_bert[:,3]>0]
    pbardata_chips = pbardata_chips[pbardata_chips[:,3]>0]
    pbardata_data = pbardata_data[pbardata_data[:,3]>0]
    
    pbardata_ftf_bic = pbardata_ftf_bic[pbardata_ftf_bic[:,27]!=999]
    pbardata_ftfp_bert = pbardata_ftfp_bert[pbardata_ftfp_bert[:,27]!=999]
    pbardata_chips = pbardata_chips[pbardata_chips[:,27]!=999]
    pbardata_data = pbardata_data[pbardata_data[:,27]!=999]
     
    
    fig, ax = plt.subplots()
    
    plt.title(element)
    
    range1 = 0.5
    range2 = 8.5
    binnumber = 8 # int(abs(range1) + abs(range2))
    
    ax.set_xlim([range1, int(abs(range1) + abs(range2))])
    

    
    if tracks3D == True:        
        tr_pbardata_plot = pbardata_data[:,27]
        tr_pbarftf_bic_plot = pbardata_ftf_bic[:,27]
        tr_pbarftfp_bert_plot = pbardata_ftfp_bert[:,27]
        tr_pbarchips_plot = pbardata_chips[:,27]
                
        print "element: ", element
        print "data", np.mean(tr_pbardata_plot), "+/-", np.std(tr_pbardata_plot)
        print "FTF_BIC", np.mean(tr_pbarftf_bic_plot),  "+/-", np.std(tr_pbarftf_bic_plot)
        print "FTFP_BERT", np.mean(tr_pbarftfp_bert_plot),  "+/-", np.std(tr_pbarftfp_bert_plot)
        print "CHIPS", np.mean(tr_pbarchips_plot),  "+/-", np.std(tr_pbarchips_plot)

    elif NrHits_inner == True:        
        tr_pbardata_plot = pbardata_data[:,4]
        tr_pbarftf_bic_plot = pbardata_ftf_bic[:,4]
        tr_pbarftfp_bert_plot = pbardata_fttp_bert[:,4]
        tr_pbarchips_plot = pbardata_chips[:,4]

    elif NrHits_outer == True:        
        tr_pbardata_plot = pbardata_data[:,5]
        tr_pbarftf_bic_plot = pbardata_ftf_bic[:,5]
        tr_pbarftfp_bert_plot = pbardata_fttp_bert[:,5]
        tr_pbarchips_plot = pbardata_chips[:,5]

    
    y_data, bin_edges = np.histogram(tr_pbardata_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm_data = 1.0*sum(np.diff(bin_edges)*y_data)    
    err_data = np.sqrt(y_data)/norm_data
    #y_data = y_data/norm_data
            
    y_ftf_bic, bin_edges = np.histogram(tr_pbarftf_bic_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm_ftf_bic = 1.0*sum(np.diff(bin_edges)*y_ftf_bic)    
    err_ftf_bic = np.sqrt(y_ftf_bic)/norm_ftf_bic
    #y_ftf_bic = y_ftf_bic/norm_ftf_bic   
        
    y_ftfp_bert, bin_edges = np.histogram(tr_pbarftfp_bert_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm_ftfp_bert = 1.0*sum(np.diff(bin_edges)*y_ftfp_bert)    
    err_ftfp_bert = np.sqrt(y_ftfp_bert)/norm_ftfp_bert
    #y_ftfp_bert = y_ftfp_bert/norm_ftfp_bert   
    
    y_chips, bin_edges = np.histogram(tr_pbarchips_plot, bins=binnumber, range = [range1,range2])
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    norm_chips = 1.0*sum(np.diff(bin_edges)*y_chips)    
    err_chips = np.sqrt(y_chips)/norm_chips
    #y_chips = y_chips/norm_chips 
    
    if barplot_ == True:     
        index = np.arange(7)
        bar_width = 0.25
        error_config = {'ecolor': '0.3'}
        
        rects_data = plt.bar(bin_centers - bar_width, y_data/norm_data, bar_width,
                    color="indianred",
                     yerr=err_data,
                     #error_kw=error_config,
                     label='data' )#"""
                     
        rects_ftf_bic = plt.bar(bin_centers , y_ftf_bic/norm_ftf_bic, bar_width,                 
                     color="lightseagreen",
                     yerr=err_ftf_bic,
                     #error_kw=error_config,
                     label='ftf_bic' )             

        rects_ftfp_bert = plt.bar(bin_centers , y_ftfp_bert/norm_ftfp_bert, bar_width,                 
                     color="darkseagreen",
                     yerr=err_ftfp_bert,
                     #error_kw=error_config,
                     label='ftfp_bert' )
         

        rects_chips = plt.bar(bin_centers + bar_width, y_chips/norm_chips, bar_width,                 
                     color="cornflowerblue",
                     yerr=err_chips,
                     #error_kw=error_config,
                     label='chips' )    
        
        plt.xticks(index + bar_width, ('0', '1', '2', '3', '4', '5', '6', '7', '8'))
    
    if errorbars_ == True:

        plt.errorbar(bin_centers, y_data/norm_data, yerr=np.sqrt(y_data)/norm_data, lw = 1, label = 'data', color =  "indianred")
        plt.errorbar(bin_centers, y_ftf_bic/norm_ftf_bic, yerr=np.sqrt(y_ftf_bic)/norm_ftf_bic, lw = 1, label = 'ftf_bic', color =  "lightseagreen")
        plt.errorbar(bin_centers, y_ftfp_bert/norm_ftfp_bert, yerr=np.sqrt(y_ftfp_bert)/norm_ftfp_bert, lw = 1, label = 'ftfp_bert', color =  "darkseagreen")
        plt.errorbar(bin_centers, y_chips/norm_chips, yerr=np.sqrt(y_chips)/norm_chips, lw = 1, label = 'chips', color =  "cornflowerblue")
    
    # compare pbar and cosmics: ##########################################################################################
    #n, bins, patches = ax.hist( [tr_pbardata_plot, tr_pbarftf_plot], yerr= 1, histtype='bar',
    #    bins = binnumber, range=(range1, range2), align='mid', label = ["$\overline{p}$","cosmics"], normed = True, color = [pbarcolor, cosmiccolor]) 
    ######################################################################################################################
    
    ax.legend()
    
    if tracks3D == True:
        plt.xlabel("number of 3D tracks")
    if NrHits_inner == True:
        plt.xlabel("number of hits, inner hodoscope")
    if NrHits_outer == True:
        plt.xlabel("number of hits, outer hodoscope")
        
    plt.ylabel("norm. counts")
    
    ##################################################################################################################
    if tracks3D == True:
        name_ = "number_of_tracks"
    if NrHits_inner == True:
        name_ = "number_hits_inner"
    if NrHits_outer == True:
        name_ = "number_hits_outer"
        
    if barplot_ == True:
        plt.savefig(name_ + '_tpx_pbars_2019_' + element + '_barplot_newest.pdf') #, facecolor=fig.get_facecolor())
    if errorbars_ == True:
        plt.savefig(name_ + '_tpx_pbars_2019_' + element + '_newest.pdf') #, facecolor=fig.get_facecolor())
        
        
    print " "
    print " "
    print "DONE!"
    print " "
    print " "         
        
        
    #fig,close()
    plt.show()

#thefile.close()   

