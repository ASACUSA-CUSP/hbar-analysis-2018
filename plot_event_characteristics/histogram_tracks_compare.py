#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

#from rootpy.tree   import TreeChain
from sys           import argv
#from scipy         import stats

#import re

import matplotlib.pyplot as plt
#from scipy.stats import gaussian_kde
#from matplotlib.cm import coolwarm
#from matplotlib.lines import Line2D
#from scipy import stats
#from scipy.odr import ODR, Model, RealData

#from scipy.optimize import fsolve, fmin, minimize, brute

from scipy.special import binom

from MLsrc.prepare_data_testing import *



#################################################################################################################################
#################################################################################################################################
def get_proba_M_MH(M, MH):
    # M ... actual multiplicity
    # MH ... multiplicity measured by hodoscope
    
    # Probability to measure MH although there were actually M :
    pH = 0.8
        
    P_M_MH = (0.8)**MH * (1.0 - 0.8)**(M - MH) * binom(M, MH)
    
    return P_M_MH



#################################################################################################################################
#################################################################################################################################

if __name__ == '__main__':

           
            print "HI!"
            
            ################################################################
            # Hori data:
            
            mult = np.array([0,1,2,3,4,5,6])
            branchs = np.array([4.17, 16.4, 41.5, 59.7, 47.3, 23.4, 4.25])#, 0.4])
            norm = 196.72
            branchs = branchs/norm 
 
            #################################################################
            mult2 = np.array([0,1,2,3,4,5,6])
            chamberlain = np.array([8.96,13.89,33.23,32.16, 19.35,5.26,1.41])
            norm = np.sum(chamberlain)
            chamberlain = chamberlain/norm
            
            #print chamberlain
            
            corr = np.zeros(7)
            #corr2 = np.zeros(7)
            
            #corr2[0] = 0
            corr[0] = 0
            
            for M, probM in enumerate(branchs):
                for MH in range(7):
                    if M > MH:
                        
                        #Mcombs[M][MH] = probM * get_proba_M_MH(M, MH)
                        
                        if MH == 2:
                            corr[2] += probM * get_proba_M_MH(M, MH)
                            
                            
                        if MH == 1:
                            corr[1] += probM * get_proba_M_MH(M, MH)   
                            
                            
                        if MH == 3:
                            corr[3] += probM * get_proba_M_MH(M, MH)  
                            
                            
                        if MH == 4:
                            corr[4] += probM * get_proba_M_MH(M, MH) 
                            
                            
                        if MH == 5:
                            corr[5] += probM * get_proba_M_MH(M, MH)  
                        
                        if MH == 6:
                            corr[6] += probM * get_proba_M_MH(M, MH)
                            
                            
                            
                            
                        ################    
                            
                            
                            

                        """if M == 2:
                            corr2[2] += probM * get_proba_M_MH(M, MH)
                            
                            
                        if M == 1:
                            corr2[1] += probM * get_proba_M_MH(M, MH)   
                            
                            
                        if M == 3:
                            corr2[3] += probM * get_proba_M_MH(M, MH)  
                            
                            
                        if M == 4:
                            corr2[4] += probM * get_proba_M_MH(M, MH) 
                            
                            
                        if M == 5:
                            corr2[5] += probM * get_proba_M_MH(M, MH)  
                        
                        if M == 6:
                            corr2[6] += probM * get_proba_M_MH(M, MH)                            
                            
                                   
                        #print M, MH, probM * get_proba_M_MH(M, MH)"""
                     
                     
                     # TODO MUss ich dann die Proba auch vom fall anziehen wenn, also von P(MH) 
            
            #print Mcombs
            
            print " "
            
            # solid angle correction
            #correction_SA = np.sum(Mcombs, axis = 1)
            
            #exit(0)
            
            
            
        
            #test_array = np.loadtxt(argv[1])[:,1]
            
            #cosmicdata_orig = np.loadtxt(argv[1])   
            #cosmicdata_orig = cosmicdata_orig[cosmicdata_orig[:,3]>0]         
            #cosmics1 = cosmics1[cosmics1[:,6]==3]
            
            pbardata_orig = np.loadtxt(argv[2])         
            pbardata_orig = pbardata_orig[pbardata_orig[:,3]> 15]  
            #pbardata_orig = pbardata_orig[pbardata_orig[:,15] < 0.4]   
            #pbardata_orig = pbardata_orig[pbardata_orig[:,7] < 170]  
            pbardata_orig = pbardata_orig[pbardata_orig[:,27] > - 13]  # for seconds, see pbars_arriving.pdf
            pbardata_orig = pbardata_orig[pbardata_orig[:,27] < - 9]
            #print "pbar events after cut on timestamp: ", pbardata.shape
            pbardata_orig = pbardata_orig[pbardata_orig[:,21] <= 1] # no pbar hits with several hits!   
            pbardata_orig, pbardata_out_orig = cut_on_vertex(pbardata_orig)
 
            
         
            tr_pbar_plot = pbardata_orig[:,6]
            #tr_cosmic_plot = cosmicdata_orig[:,6]
            
            range1 = -0.5
            range2 = 6.5
            binnumber = 7
        
                            
            fig, ax = plt.subplots()           
            redc = (176.0/255,70.0/255,35.0/255)            
            bluec = (36.0/255,75.0/255,166.0/255)
            
            # compare pbar and cosmics: ##########################################################################################
            #n, bins, patches = ax.hist( [tr_pbar_plot, tr_cosmic_plot], histtype='bar',
            #    bins = binnumber, range=(range1, range2), align='mid', label = ["# tracks, pbars","# tracks, cosmics"], normed = True, color = [redc, bluec]) 
            ######################################################################################################################

            # compare pbar with hori: ############################################################################################
            #n, bins, patches = ax.hist( [tr_pbar_plot], histtype='bar',
            #    bins = binnumber, range=(range1, range2), align='mid', label = ["# tracks, pbars"], normed = True, color = [redc]) 
                
            y, bin_edges = np.histogram(tr_pbar_plot, bins=binnumber, range = [range1,range2])
            bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
            norm1 = 1.0*sum(np.diff(bin_edges)*y)
            
            
            y2 = y/norm1 + corr #- corr2
            
            norm = 1.0*sum(np.diff(bin_edges)*y2)
           
            ax.errorbar(bin_centers, y2/norm, yerr = np.sqrt(y)/norm1, marker = '.', drawstyle = 'steps-mid', label = 'asacusa corr.')
            #ax.errorbar(bin_centers, y/norm1, yerr = np.sqrt(y)/norm1, marker = '.', drawstyle = 'steps-mid', label = 'asacusa uncorr.')     
            #ax.errorbar(bin_centers, y/norm - lost_solid_ang2, yerr = np.sqrt(y)/norm, marker = '.', drawstyle = 'steps-mid', label = 'asacusa')
            #ax.bar(mult, branchs, color = bluec, label = "Hori", alpha = 0.5)
            ax.errorbar(mult, branchs, color = 'green', label = "Hori", drawstyle = 'steps-mid', linestyle = 'dashdot')
            #ax.errorbar(mult2, chamberlain, color = 'red', label = "Chamberlain", drawstyle = 'steps-mid', linestyle = 'dashed')
            ######################################################################################################################
            
            plt.xlabel("number of tracks", fontsize = 15)
            plt.ylabel("norm. counts", fontsize = 15)
            legend = ax.legend(loc='upper right', shadow=True)
            
            
             
            #ax.set_xlim(0,1.1)
            #ax.set_ylim(0,)
            
            plt.savefig('tracks_histo_16_hori.pdf', facecolor=fig.get_facecolor())
            #fig,close()
            plt.show()
    
        #thefile.close()   

