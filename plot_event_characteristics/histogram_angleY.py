#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division


import numpy as np

import logging
# Most verbose log level
logging.basicConfig(level=logging.DEBUG)

from rootpy.tree   import TreeChain
from sys           import argv
from scipy         import stats

import math

import re

import matplotlib 
#matplotlib.use('ps')

import matplotlib.pyplot as plt

from scipy import stats

pbarcolor = '#990001ff'

cosmiccolor = '#51989fff'

#from matplotlib import rc
#rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage{color}')


def calc_ellipse(x, y, ellpar):

    center_x = ellpar[0]  
    center_y = ellpar[1]  
    Rx = ellpar[3]  
    Ry = ellpar[2]  
    angle = ellpar[4]/180*math.pi
    #angle = 30.0/180*math.pi

    #x_new = (x - center_x)*math.cos(angle) - (y - center_y)*math.sin(angle)
    #y_new = (y - center_y)*math.cos(angle) + (x - center_x)*math.sin(angle)
    dx = (x - center_x)
    dy = (y - center_y)
      
    condition = (dx*math.cos(angle) + dy*math.sin(angle))*(dx*math.cos(angle) + dy*math.sin(angle))/(Rx*Rx) + (dx*math.sin(angle) - dy*math.cos(angle))*(dx*math.sin(angle) - dy*math.cos(angle))/(Ry*Ry)
    
    if condition <= 1:
        return True
    #if dx*dx/(Rx*Rx) + dy*dy/(Ry*Ry) <= 1:    # outside of circle
    #    return True    
    else:
        return False

#################################################################################################################################

def cut_on_vertex(new_cands):
    events_cut = []
    events_out = []
    
    width = 45 # first vals
    height = 25 # first vals
    
    xy = [-23,-23]
    angle = -50
    #ellipse = Ellipse(xy= xy, width=width, height=height, angle=angle,edgecolor='r', fc='None', lw=2)   
    ell_par = [xy[0], xy[1], width*0.5, height*0.5, angle]
    
    for i in new_cands:
        if calc_ellipse(i[20], i[21], ell_par) == True: # 19 20 are x and y coords of vertex 
            #print i[9]
            #vertex_x.append(i[9])
            #vertex_y.append(i[10])
            events_cut.append(i)
        else:
        #    vertex_x.append(i[9])
        #    vertex_y.append(i[10])
            events_out.append(i)     
        
               
    events_cut = np.array(events_cut)
    events_out = np.array(events_out)


    return events_cut, events_out


#################################################################################################################################


if __name__ == '__main__':

    print "HI!"
        
    #pbardata_orig = np.loadtxt("../training_data/all_pbars_data_2016.dat")
    
    #cosmicdata_orig = np.loadtxt("../training_data/cosmics_data_2016_break1.dat")
    pbardata_orig = np.loadtxt("../training_data/all_pbars_data_2016_yangle.dat")
    cosmicdata_orig = np.loadtxt("../training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")

    #midas = np.unique(cosmicdata_orig[:,1])
    
    #print midas
    
    
    #print midas.shape
    
    #exit(0)
    
    
    
    
    #cosmicdata_orig = np.loadtxt("../training_data/cosmics_data_2016_halfbreak2.dat")
        
    pbardata_orig = pbardata_orig[pbardata_orig[:,28] > - 13]  # four seconds, see pbars_arriving.pdf
    pbardata_orig = pbardata_orig[pbardata_orig[:,28] < - 9]
    #print "pbar events after cut on timestamp: ", pbardata.shape
    pbardata_orig = pbardata_orig[pbardata_orig[:,22] <= 1] # no pbar hits with several hits!
    
    bool_cut_vertex = True
    
    #if bool_cut_vertex == True:
    #    pbardata_orig, pbardata_out_orig = cut_on_vertex(pbardata_orig)
   
    all_actual_cosmic = 282935.0 # half break 1... hodoscope+BGO trigger
    cosmic_time = 603659.0 # in seconds, half break 1... hodoscope+BGO trigger
    #all_actual_cosmic = 314776.0 # half break 2... BGO trigger
    #cosmic_time = 197074.0 # half second break
    
    pbar_time = 4*93.0 # 4 seconds, 102 runs
    area_hit = math.pi*45.0*0.5*25.0*0.5
    area_BGO = math.pi*45*45 #- area_hit
    
    
    print "all in file cosmic: ", len(cosmicdata_orig), all_actual_cosmic
    print "all in file pbar: ", len(pbardata_orig)
    
    BGO_cut = 0
    Track_cut = 0
   
    cosmicdata_orig = cosmicdata_orig[cosmicdata_orig[:,3]>=BGO_cut]
    cosmicdata_orig = cosmicdata_orig[cosmicdata_orig[:,6]>Track_cut]
    
    pbardata_orig = pbardata_orig[pbardata_orig[:,3]>=BGO_cut]
    pbardata_orig = pbardata_orig[pbardata_orig[:,6]>Track_cut]
     
    #scan_E_cut(cosmicdata_orig, pbardata_orig)
 
    estimated_cosmics = len(cosmicdata_orig)*1.0/cosmic_time*pbar_time # scale to pbar time
    estimated_cosmics = estimated_cosmics*area_hit/area_BGO # scale to hit area
    
    print "estimated cosmics scaled to pbar time and hit area", estimated_cosmics
    
    print "estimated cosmics in test set (1/3)", estimated_cosmics*0.33
    
    
    fig, ax = plt.subplots()
    
    range1 = 0.
    range2 = 180.0
    bins = 180
    
    ax.set_xlim([range1, range2])
    #ax.set_ylim(0,0.13)
    
    # max angle
    #angle_pbar = pbardata_orig[:,7]
    #angle_cosmic = cosmicdata_orig[:,7]
    
    # angle Y
    #angle_pbar = pbardata_orig[:,10]
    angle_cosmic = cosmicdata_orig[:,10] 
    angle_pbar = pbardata_orig[:,10] 
    
    #range1 = min( np.min(angle_cosmic), np.min(angle_pbar))
    #range2 = max(np.max(angle_cosmic), np.max(angle_pbar))
    
    
    hist, edges = np.histogram(angle_pbar, bins=bins, range=[range1, range2])
    norm = 1.0*sum(np.diff(edges)*hist)
    err  = np.sqrt(hist)
    offset = (edges[1]-edges[0])/2.
    centres = edges[:-1]+offset/2  

    ax.errorbar(centres, hist/norm, yerr = err/norm, label = '$\overline{p}$', color = pbarcolor, linewidth = 1)
    
    hist, edges = np.histogram(angle_cosmic, bins=bins, range=[range1, range2])
    norm = 1.0*sum(np.diff(edges)*hist)
    err  = np.sqrt(hist)
    offset = (edges[1]-edges[0])/2.
    centres = edges[:-1]+offset/2  

    ax.errorbar(centres, hist/norm, yerr = err/norm, label = 'cosmics', color = cosmiccolor, linewidth = 1)
    
    ax.legend()
    
    plt.xlabel("Y angle (degree)")
    plt.ylabel("norm. counts")
    
    ##################################################################################################################
    
    
    plt.savefig('Y_angle_tracks{0}_all.pdf'.format(Track_cut)) #, facecolor=fig.get_facecolor())
    #fig,close()
    plt.show()

#thefile.close()   

