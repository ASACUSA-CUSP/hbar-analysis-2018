# Hbar analysis program (2018 version)

This repository contains the original code written by B. Kolbinger.

All the data files have been wiped out before uploading to gitlab. The folder size was originally 3.5GB, now 50MB. For future inspection of the original directory content use the `ncdu_output_hbars_cuts_final_BEFORE_CLEANING.txt` file (a json-like text file obtained with ncdu).
```bash
ncdu e -f ncdu_output_hbars_cuts_final_BEFORE_CLEANING.txt
```

**Python2.7** and **older ROOT** versions are required! (do not use ROOT versions newer than 6.18/04)

To set up the enviroment to run the code refer to [enviroment instructions](https://gitlab.cern.ch/ASACUSA-CUSP/hbar-analysis-2018/-/blob/master/Enviroment_instructions.md).


# Instructions written by Bernadette on the SMI e-log in 2021.

[[_TOC_]]

## Machine learning documentation

NOTES:

- I did not prepare a list of necessary python packages that need to be installed. But by trying to run the script, it will be printed in the console what packages are missing

- the python package **imblearn** (and probably also other packages I used) has been updated since I last used the ML code, and it requires **scikit-learn >= 0.21** now, which in turn requires python3. However, all the code is written in python 2.7. An older version of the packages can be installed then (**imbalanced-learn==0.4.2**), however I would recommend migrating the code to python3 which I didnt have time to do.

----

Folder: `/u/kolbinger/machine_learning_final` on smilx0

This is after selection of features and hyperparameter tuning (see below):
training data and hbar data found at:
- `./training_data/all_pbars_data_2016_yangle.dat`
- `./training_data/all_cosmics_data_2016_halfbreak1_yangle.dat`
- `./training_data/all_hbar_fi_runs_2016_triggercut.dat`

The whole analysis until the scan of the p-value to find the cut on the prediction score is done by the following bash script: `process_ML.sh`

The bash script will call a series of python scripts. The variable NR in the bash script is the number of iterations of training and testing

1) first script it calls: `xgboost_train_test.py` which calls functions saved in .py files in `MLsrc`, such as `prepare_data.py` which prepares training and testing sets, chooses the right columns (=features) of the .dat files listed above and applies some cuts on the pbar data set (cut on time of arrival at detector and cut on hit position on BGO). It will build the ensemble of iterations of training, testing and predicting (e.g. 500 times as used for the thesis results, nr_of_xgboosts = 500)
It saves the test set data predictions to:
`"./events_pred_cos_pbar_rounds/events_and_prediction_pbar_{}.dat".format(nr_xgb)`
`"./events_pred_cos_pbar_rounds/events_and_prediction_cosmic_{}.dat".format(nr_xgb)`
with nr_xgb being the current round.
Afterwards it uses the build model of the round to predict Hbar event classes and saves them to:
`'./hbar_xgboost_cands_rounds/hbars_xgboost_round_{}.dat'.format(nr_xgb)`
After the ensemble is fully build, there will be NR files of cosmic predictions, NR files of pbar predictions amd NR files of hbar predictions.

2) afterwards, `conv_hbar_FI.py` is called, with just adds the FI settings to the events, so it is easier to select certain events later

3) Then, `make_mean_hbar_score.py` is called which calculates the median and mean of the prediction scores from the NR runs

4) `determine_prediction_score_cut.py` can be used to scan the cut for the prediction score and find the minimum of the p-value/maximum of the significance
After the cut has been found, it can be set in the same script to calculate cosmic rejection and pbar efficiency etc.
The resulting Hbar candidate events will be saved into a file (`all_hbar_cands_after_xgb_cut.dat`) which can then be used to produce the final plots of n-distribution etc.

___

**Feature selection:**

path: `/machine_learning/feature_selection`

feature selection using SFS methods as discussed in my thesis are done by the script: `mlxtend_feature_select.py`

the directory `/machine_learning_final/plot_feature_select_mlxtend/` contains scripts to plot the results e.g. `nr_of_features_vs_performance.py`

a few other feature selection methods are tested in `feature_select_test.py`, such as univariate, Recursive Feature Elimination, Extra Trees. Also, PCA is implemented in there (these results are not described in my thesis, since SFS has been chosen, but could be interesting to look at)

grid search is implemented in the script: `mlx_feature_select_grid_search.py`

___

**hyper-parameter tuning:**

Baysian optimisation:

Hyper-parameter tuning with Tree Parzen Estimator is implemented in: `TPE_opt_XGB.py in /machine_learning_final/`

Hyper-parameter tuning with Gaussian Processes is implemented in: `bayes_opt_XGB.py` in `/machine_learning_final/`
the scripts will print out the results which then should be added to the scripts discussed below

comment: Those scripts should not be used blindly as a black box. In case one wants to tune the hyper-parameters again, one should look at the scripts in a bit more detail. I have not tested those scripts since I produced the results for the thesis and I didnt have time to do that now. It could be that they are not compatible with the functions in `prepare_data.py` where I made small changes afterwards. However, in that case, it should be easy to spot the incomp
___

The benchmark with TMVA can be found in the folder `TMVA_rectcuts`
code is in script: `tmva_rect_cuts.py`
___

Plotting the results (n-distribution and time distribution):
folder: `./plotting_q_dist/`
All plotting functions can be found in the script: `plot_fi_results_all_features_thesis.py`

By commenting the function calls in lines 1466 - 1473 in and out.
time distrinbution: plotted by *function plot_time_dist(...)*
time distribution compared to AMT: *plot_time_distr_AMT(...)*
n-distribution (commulative): *plot_q_dist(...)*
n-distribution (differences btw bins): *plot_q_dist_fatbin(...*

## Tracking code 

This is a summary on how to use the tracking code that I developed in the course of my thesis and that is used for extracting the number of tracks (=number of pions) seen by the hodoscope.
The same scripts are also used to extract the event features for the machine learning analysis.

The code is in the directory: ``/u/kolbinger/hbar_cuts_final/`` on smilx0

Angela has access rights to my directory, I will also give her my password next time I see her, I have changed it to ~~(some instructions here)~~

Here, the tracking scripts for measured and simulated data can be found.


#### Data:

The main script is called: `tracking_data_3d_final_newest.py` and it will produce .dat files with the following structure: N x d, where N (=rows) is the numer of events and d (= columns) is the number of event characteristics.
In order to analyse a run contained in a rootfile produced by the raw data analyser, execute: 
```bash
python tracking_data_3d_final_newest.py rootfile.root
```
Here, a loop over all entries in the roottree (=events) will analyse them and write them into the .dat file

The analysed timepix runs can be found in `data_files_tpx_Gold/ data_files_tpx_Molybdenum/` etc.

Before running the script, check the list of flags/settings at the start of the script. If for example the flag plotyes3D is set to True, then event plots will be saved to the folder `event_plots`

The folder `calibrationData` contains calibration and correction data for hodoscope, fibre detector and BGO. They are read in by the main script

Classes of the detector parts are stored in `src`. The folder contains also .py files containing collections of functions used for tracking and so on, e.g. the file `trackfinding_fitting_3D_final.py` contains all functions needed for 3D tracking. All classes and .py functions needed in the main script are listed in the beginning where they are imported.

I added a lot of comments and explanations in the script, so I hope it's clear.

The folder `plot_event_characteristics` contains scripts to apply on the .dat files e.g. plotting histograms of the number of tracks etc.

Also, the script comparing the simulated and measured number of tracks can be found in `histogram_tracks_tpx_compare_sims_data.py`

#### Simulations:

The scripts for analysing the simulations are contained in

`/u/kolbinger/hbar_cuts_final/simulations_tracking`

They have the same basic structure as the scripts for the measured data

Before they can be applied to the Geant4 rootfiles produced by the hbar_gshfs simulation code, those rootfiles need to be converted with the script `convert_rootfile_to_data_structure.cc` that can be found in `/u/kolbinger/hbar_gshfs_CHIPS/rootfiles/` and `/u/kolbinger/hbar_gshfs_FTF/rootfiles/`

This converts the Geant4 rootfile to a rootfile with very similar structure as the rootfiles from the measurements, so the same tracking algorithm can be applied.

Afterwards, the files can be analysed via
```bash
python tracking_simulations_3d_2019_hbar_gshfs.py converted_rootfile.root
```
Again, .dat files are created with the same structure as the data files mentioned above. 

The script `tracking_simulations_3d_2019_hbar_gshfs.py` looks similar to `tracking_data_3d_final_newest.py` with a few adjustments.

The analysed simulated data is saved in .dat files called "tracking_results_" + converted_rootfile + ".dat"





