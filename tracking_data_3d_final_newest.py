#!/usr/bin/python
# -*- coding: utf-8 -*-
#############################################################################################################
#                                         author: Bernadette Kolbinger                                      #
#                                                                                                           #
#     python analysing script for the ASACUSA detector - hodoscope + BGO and hodoscope + Timepix3 quad      #
#                                                                                                           #
#     determines event features such as number of tracks, ToF,... and also vertex etc.                      #
#     NO antihydrogen/antiproton event candidate selection in this script                                   #
#                                                                                                           #
#     last changes: 27/08/2019                                                                              #
#                                                                                                           #
#     usage:                                                                                                #
#     first check the flags/settings (starting at line 63)                                                  #
#         -> set file name of output txt file                                                               #
#         -> start script with: python *script_name* rootfile                                               #
#                                                                                                           #
#     notes:                                                                                                #
#     main function starts at line 222, loops over events of chained rootfiles                              #
#                                                                                                           #
#                                                                                                           #
#############################################################################################################

import numpy as np
#import Image
import math
import matplotlib.backends.backend_pdf
from matplotlib.lines import Line2D             
from matplotlib.cm import  gray_r, gray, bone
from matplotlib import lines as mpl_lines 
from matplotlib.patches import Ellipse
from matplotlib.patches import Circle
import itertools as it 
#from itertools import product, combinations
from numpy import random

from itertools import groupby
from operator import itemgetter
import itertools
import matplotlib.pyplot as plt
import re

import scipy.stats as stats

from src.trackplot_final import *               # # collection of functions needed for event plotting (2D). the 3D plotting function is in "trackfinding_fitting_3D_final"
from src.trackfinding_fitting_3D_final import * # collection of functions needed for the 3D fitting
from src.trackfinding_fitting_2D_final import * # collection of functions needed for the 2D fitting
from src.hodoscope_trackhelper import *         # a few extra functions needed for the tracking

from src.vertex_reconstruction_2D import *      # 
from src.hodoscope_tracking import *            # hodoscope class. same as just hodoscope with a few extra functions for tracking
from src.bgo import *                           # bgo class, newest calibration (2017)... 2018 calibration not done! 
from src.scalar import *                        # scalar class 

from numpy import ndarray

from sys import argv
import ROOT
from rootpy.tree import TreeChain
#from skimage.transform import (hough_line, probabilistic_hough_line, hough_line_peaks)
#from skimage.color import rgb2gray
#from skimage.filters import gaussian_filter
#from skimage.morphology import skeletonize

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

Timeintervallconstr = False  # are we only checking between mixing start and stop?

time_stamp_mode = "midas_millisec" #"midas_millisec" # "midas_sec" # "ADC_ts"

printVerbose = False #True   # print a lot of additional infos

printSummary = False    # print summary of event at the end of loop, can be useful for documentation
writeROOT = False  # create a ROOT file with the candidate events FIXME: this has not been used for a long time and is also not necessary for the current analysis
                   # it can be updated if necessary
                   
writeTxt = False    # write out data to text files ... This is used for machine learning analysis and plotting event characteristics

#if writeTxt == True:
    #file_mtds = open("./data_files_tpx_Gold/timepix_midas674.dat", 'w') 
    
    #file_mtds = open("./data_files_tpx_Au/timepix_midas{}.dat".format(midas_run_nr), 'w') 
    

plotyes2D = False  # plot the 2D x-y event drawings
plot_2Dtrackfinding = [False,False] # plots details of 2D Hough-like track finding
                    # this is mostly used for debugging. [points, lines] # rotation lines and their rotation points (in bgo) for plotting  
plotyes3D = True                    
plotshow = False #True    # show the event plots during analysis (event by event)

simplecuts = False # some VERY rough cuts, this is no proper event selection. simple cuts to identify rough Hbar candidates -- the usual cuts were BGOE > 15MeV, 
PreCutBool = True  # pre cuts set before analysing events -- check further down for the set cuts
BGOon = False      # BGO in setup?
TPXon = True       # Timepix in setup?
BGOcut = 0.0       # BGO energy cut?
fibrebool = True   # enable 3D tracking

##################################################################################################################################################################
##################################################################################################################################################################
##################################################################################################################################################################
def get_mixtime_from_scalar(SclT):

    startmixtime = 0.0
    endmixtime = 0.0
    
    for event in SclT:
            s = scalar(event)
            
            if printVerbose:
                print s.mixingStart

            if s.mixingStart==1.0 and startmixtime != 0.0:
                if printVerbose:
                    print "######################################"
                    print "# MORE THAN ONE MIXING START SIGNAL! #"
                    print "######################################"
          
            if s.mixingStart==1.0:# and startmixtime == 0.0: #NOTE FIXME first mixing start signal is taken!!          
                if time_stamp_mode == "midas_sec":
                    startmixtime = s.midasTime
                    #mixtime.append([startmixtime, endmixtime])                
                elif time_stamp_mode == "midas_millisec":
                    startmixtime = s.timeStamp 
                    #mixtime.append([startmixtime, endmixtime])
           
            if s.mixingStop>0.0:
                if time_stamp_mode == "midas_sec":
                    endmixtime = s.midasTime
                elif time_stamp_mode == "midas_millisec":
                    endmixtime = s.timeStamp

            ## FIXME: won't work for runs without mixing stop, like capture tests
            if  startmixtime > 0.0 and endmixtime > 0.0:
                mixtime.append([startmixtime, endmixtime])
                startmixtime = 0.0
                endmixtime = 0.0
        ##########################################################################################################
        
    if printVerbose:
            print "start and end timestamp of mixing: ", startmixtime, endmixtime, " Cusp run number: ", s.cuspRunNumber
            print len(mixtime)
            #mixtime[23][1] = endmixtime
            print "//////////////////////// ", mixtime
           
            print "start and end timestamp of mixing: ", mixtime, " Cusp run number: ", s.cuspRunNumber
    #if len(mixtime)>0:                     # FIXME if length of last element of mixtime is longer than 2, then take the last one .... then this works with more than one file
    #    mixtime = [mixtime[-1]]

    return mixtime
##################################################################################################################################################################
##################################################################################################################################################################    
def get_time_stamp(event): 
    #"midas_millisec" # "midas_sec" # "ADC_ts"  
    adcTimestamp = -9999
    ts = 9999
    if Timeintervallconstr:       
        if time_stamp_mode == "midas_sec":
            ts = event["midasTimeStamp"][0]
            
        elif time_stamp_mode == "midas_millisec":
            ts = event["midasMilliTimeStamp"][0] 

        elif time_stamp_mode == "ADC_ts":
            adcTimestamp = event["mixingTimeStamp"][0]
            
    else:
        if time_stamp_mode == "midas_sec":
            ts = event["midasTimeStamp"][0]
            
        elif time_stamp_mode == "midas_millisec":
            ts = event["midasMilliTimeStamp"][0] 

    return ts, adcTimestamp          
##################################################################################################################################################################
##################################################################################################################################################################    
def check_if_in_mix_timeinterval(startmixAdc, adcTimestamp, mixtime):  
    #######################################################
    if time_stamp_mode == "ADC_ts":
        
        if not startmixAdc and adcTimestamp > 0.0:
            startmixAdc = adcTimestamp - 0.021 

        if adcTimestamp != -9999:
            adcTimestamp = adcTimestamp - startmixAdc
    else:
        startmixAdc = 999
                
    if adcTimestamp > 25.0 or adcTimestamp == -9999:
        return False, adcTimestamp, startmixAdc
    else:
        return True, adcTimestamp, startmixAdc
        
    ########################################################         
    if time_stamp_mode == "midas_millisec":
        mix = False
        for i in mixtime:
            mixtcorr =  i[0] - 0.021
            if mixtcorr - ts > 0.0 and mixtcorr - ts < 25.0:
                mix = True
                startmixtime = mixtcorr

            if ts - startmixtime > 25 or startmixtime - ts < 0:
                return False, ts, startmixtime

        if mix == False:
            return False, ts, startmixtime
            
        elif mix == True:
            return True, ts, startmixtime
            
    ######################################################## 
    if time_stamp_mode == "midas_sec":
            mix = False
            for i in mixtime:                       
                if ts >= i[0] and ts <= i[1]:
                    mix = True
                    startmixtime = i[0] - 0.021
        
            if ts - startmixtime > 25: #or ts - startmixtime < 2:
                return False, ts, startmixtime

            if mix == False:
                return False, ts, startmixtime
                
            elif mix == True: 
                return True, ts, startmixtime       
##################################################################################################################################################################
##################################################################################################################################################################    
##################################################################################################################################################################
##################################################################################################################################################################
if __name__ == "__main__":

    if len(argv) < 2:
        print "usage: ", argv[0], "rootfile1.root rootfile2.root ... rootfileN.root  "
        exit(-1)

    ####################################################################################################################################################################################
    # calibration and corrections data: (see folder calibrationData in this directory)
    ####################################################################################################################################################################################
    # load calibraton data:
    
    bgoCalibData = readBGOCalibrationData("./calibrationData/gainBGOcalibration.dat")
    correctionData = readTimingCorrectionData("./calibrationData/hodoscopeTimingCorrectionTable.dat")  
    #corrtiming = np.loadtxt("./calibrationData/cable_length_corrections.dat")
    
    # hodoscope cable length correction tables:
    cable_corr_inner = np.loadtxt('./calibrationData/cable_corrections_inner_up_down_2_15mev.dat')
    cable_corr_outer = np.loadtxt('./calibrationData/cable_corrections_outer_up_down_2_15mev.dat')
    t_glob_o = 1.39542395
    t_glob_i = 1.84575840    
    
    #  cuts on fibre channels:
    thresi = np.loadtxt("./calibrationData/inner_charge_cuts_up_down_pbar_ang.dat")
    threso = np.loadtxt("./calibrationData/outer_charge_cuts_up_down_pbar_ang.dat")
    #threso = np.loadtxt("outer_amp_cuts_up_down_cosmic.dat")
    fibre_inner_cuts = np.loadtxt('./calibrationData/inner_fibre_tot_cuts_2019.dat')
    fibre_outer_cuts = np.loadtxt('./calibrationData/outer_fibre_tot_cuts_2019.dat')   

    ####################################################################################################################################################################################
    ####################################################################################################################################################################################

    # extract midas run number from rootfile name: ... careful when chaining a few together!
    filename = str(argv[1])
    ind_str_1 = filename.find('run-')  
    ind_str_2 = filename.find('.root')       
    midas_run_nr = int(filename[ind_str_1+4:ind_str_2])

    
    if printVerbose == True:
    
        print "##########################################################################################"
        print "########################             MIDAS RUN:", midas_run_nr, "        #############################"
        print "##########################################################################################"
  
    # load scalar data tree
    SclT = TreeChain("ScalarDataTree", argv[1:])

    cand_list = [] # FIXME writing out rootfiles is not uptodate ... list of candidates, used for filling the root tree

    adcTimestamp = 0.0   # timestamo from hodoscope waveform digitisers
    mixtime = []         # array for saving mixing start/stop signals
    startmixtime = 0.0   # start signal of mixing

    
    #########################################################################################################################################################################
    #########################################################################################################################################################################    
    # get mixing start and stop signals from the scalar data tree, in case event search is restricted to time interval
    if Timeintervallconstr:    
        mixtime = get_mixtime_from_scalar(SclT)        
    #########################################################################################################################################################################
    #########################################################################################################################################################################    
    #list_candidates = []
    
    # root tree containing events:
    T = TreeChain("HbarEventTree", argv[1:])
        
    cuspRunNumber = 0        # initialise cusp run number integer
    startmixAdc = None       
    mtd = 999               # initialise ToF variable
    numbers = np.arange(32) # array of numbers, used for bar hodoscope
    tot_events = 0        # total number of events that are being analysed
  
    #candidates_evts = np.loadtxt("/home/bernadette/Dropbox/machine_learning/post_processing_2018/all_hbar_cands_after_xgb_cut_2018.dat")
    
    #########################################################################################################################################################################
    #########################################################################################################################################################################
    # MAIN LOOP OVER EVENTS
    #########################################################################################################################################################################
    #########################################################################################################################################################################
    
    for evtnumber, event in enumerate(T):
            
        cuspRunNumber = event["CUSPRunNumber"][0]   # get cusp run number
        #ts = event["midasMilliTimeStamp"][0]
        
        #print event["isMixingSignal"]
        #continue
        ts, adctimestamp = get_time_stamp(event)   # get time stamp

        if printVerbose:  
            print "EVENT ----- ", evtnumber, ts
        
        ##################################################################################################################            
        if Timeintervallconstr:
            check_if_in_mix_timeinterval(startmixAdc, adctimestamp, mixtime) 
        ##################################################################################################################
        
        if BGOon == True:
            b = BGO(event, bgoCalibData)
            bgo_E = b.getCharge()
            
        else:
            bgo_E = 999   # if BGO is not in use, set total energy deposit to 999
                        
        # initialise hodoscope instance for this event:                
        h = hodoscope(event, thresi, threso, fibre_inner_cuts, fibre_outer_cuts)
        
        # initialise fibre layers, if in use:
        if fibrebool ==True:            
            innerF = h.fi     # inner layer
            outerF = h.fo     # outer layer
                      
            fI = innerF.active # bool array, active fibres inner layer
            fO = outerF.active # bool array, active fibres outer layer
                    
                    
        nI, nO, I, O = h.getActiveBar() # number of active bars inner, number of active bars outer, bool array active bars inner,  bool array active bars outer
        currI = numbers[I] # array with channel numbers of active inner bars
        currO = numbers[O] # array with channel numbers of active outer bars
        innerCF, outerCF = h.getTimingCF() # arrays of inner and outer constant fraction time stamps
        innerLE, outerLE = h.getTimingLE() # arrays of inner and outer leading edge time stamps           
   
        # check if hodoscope hits meantimes and z pos fit in the distributions             
        innerCFs, outerCFs, innerzPos, outerzPos, currI_cut, currO_cut = is_it_a_hit(h, cable_corr_inner, cable_corr_outer) 

        number_of_noise_hits_I = len(currI) - len(currI_cut) 
        number_of_noise_hits_O = len(currO) - len(currO_cut)
                
        noise_bars_I = list(set(currI) - set(currI_cut)) # array of bars having a noise hit (according to is_it_a_hit() )
        noise_bars_O = list(set(currO) - set(currO_cut))
        
        #if number_of_noise_hits_O == 0: # TODO use this if you want to write out noise hits!
        #    continue
                                    
        currI_all = currI_cut
        currO_all = currO_cut
        nO_all = currO_all.size
        nI_all = currI_all.size
        innerCF = innerCFs[:,1:]
        outerCF = outerCFs[:,1:] 
        
        I_new_all = np.zeros((32), dtype=bool)
        for nr,i in enumerate(I_new_all):
            if nr in currI_all:
                I_new_all[nr] = True
                
        O_new_all = np.zeros((32), dtype=bool)
        for nr,o in enumerate(O_new_all):
            if nr in currO_all:
                O_new_all[nr] = True        
                      
        h.setActiveBars(I_new_all, O_new_all) # set new active bars after is_it_a_hit() check       

        # count all events in file, or, if timeintervallconstr is on, then between mixing start and stop signals
        tot_events = tot_events + 1        
        ##################################################################################################################
        # select only certain events in a file listing eventnumber and cusp run number
        """if cuspRunNumber not in candidates_evts[:,3] :
             continue

        evts = (candidates_evts[candidates_evts[:,3] == cuspRunNumber])[:,5]
        
        #print evts
        
        #exit(0)
        

        if evtnumber not in evts:
            continue # """
        ##################################################################################################################   
       
        if PreCutBool == True:
            if nI_all<1:       
                    continue
                      
            if nO_all<1: 
                    continue

            if nI_all >= 16 or nO_all >= 16:
            #    print evtnumber, "too many hits"
                    continue
            if BGOon:
                if bgo_E < BGOcut or bgo_E > 300:    # charge too small or too big
                    if printVerbose:  
                        print evtnumber, "charge too small or too big"
                    continue

        if printVerbose:  
            print "##########################################################################################"
            print "########################              NEW EVENT!             #############################"
            print "##########################################################################################"
            print "CUSP, Midas, Event:", cuspRunNumber, midas_run_nr, evtnumber

            if BGOon:
                print "BGO ENERGY (MeV): ", bgo_E
            print "I uncut:", nI, currI
            print "O uncut:", nO, currO
                    
            print "currI : ", currI_cut
            print "currO : ", currO_cut
                    
            print "number of noise hits inner: ", number_of_noise_hits_I
            print "number of noise hits outer: ", number_of_noise_hits_O
               
        #currI_uncut = currI
        #currO_uncut = currO
        #####################################################################################################################
        #####################################################################################################################
               
        # exclude orphans from the tracking algorithm:
        currI_o, currO_o = is_it_an_orphan(currI_all, currO_all) # orphans excluded
        
        #print "ATTENTION: AT THE MOMENT THE NOISE HITS ARE NOT CUT AWAY -- TESTING MODE"
        # set currI to currI_o for cutting away noise hits
        
        if printVerbose:  
            print "currI for tracking (after orphan removing and cuts):", currI_o
            print "currO for tracking (after orphan removing and cuts):", currO_o
        
        number_of_orphans_I = len(currI_all) - len(currI_o)
        number_of_orphans_O = len(currO_all) - len(currO_o)
        
        if printVerbose:  
            print "number of orphan hits inner: ", number_of_orphans_I
            print "number of orphan hits outer: ", number_of_orphans_O
        
        # continue only with events that have more than 0 hits in inner and outer layer:        
        if nI > 0 or nO > 0:
        
            currI = currI_o
            currO = currO_o
            
            nO = currO.size
            nI = currI.size
          
            if PreCutBool == True:
                if nI < 1  or nO < 1:
                    continue
                
            I = np.zeros((32), dtype=bool)
            for nr,i in enumerate(I):
                if nr in currI:
                    I[nr] = True
                    
            O = np.zeros((32), dtype=bool)
            for nr,o in enumerate(O):
                if nr in currO:
                    O[nr] = True  
                    
            h.setActiveBars(I, O)   # set new active bars with orphans excluded
            
            chargeI, chargeO = h.getCharge()
                           
            cornerplotI, cornerplotO = getCornerMap()   # cornermap is an array (one for inner, one for outer) containing 
                                                        # the corners of the x-y cross section of the bars (4 times 2 values per bar)
            pmapI, pmapO = prepare_PosMap(I,O,nI, nO)   # posmap contains the center points of all the hodoscope bars
            cmap = prepare_CornerMap(I,O, nI, nO)       # get only those entries of cornermap that show a hit, plus merge the inner and outer corner map to one
            
            #####################################################################################################################         
            ynI, clusI = find_consecutive_nr(currI)   # number of clusters, array containing clusters (= several bars with hits next to each other)
            ynO, clusO = find_consecutive_nr(currO)
                        
            clusI = [s for s in clusI if len(s) > 1]
            clusO = [s for s in clusO if len(s) > 1]
            
            #if len(clusI) < 1:
            #    continue
            if printVerbose: 
                print "Inner cluster: ", clusI
                print "Outer cluster: ", clusO
                print " " 
             

        #####################################################################################################################
        #####################################################################################################################

        # if BGO is activated, find the vertex by calculating the average x-y position of charge weighting the pixels of largest cluster in
        # BGO pixel map:        
        if BGOon == True:
            if printVerbose: 
                print "#########################       VERTEX via BGO Pixelmap:      ############################"    
            orig = np.copy(b.dataMap)
            coords = np.copy(b.coord) 
            coords_copy = np.copy(b.coord)  # I need two!!
       
            x,y =  np.unravel_index(orig.argmax(), orig.shape) 
            xm = coords[x,y,0] # x of pixel with highest E value in BGO map
            ym = coords[x,y,1] # y of pixel with highest E value in BGO map             
               
            bounds_for_2D_vertex = [xm, ym]

            # find clusters in BGO pixel map and return:
            # number of cluster, array of x-y average positions of clusters, distances btw clusters, largest cluster
            nr_of_cluster, cluster_points, distances, max_cluster = get_bgo_multi(orig, coords_copy)  
            if len(distances) > 0: # get largest distance between clusters in case of several clusters
                cluster_dist = np.max(np.array(distances))   
            else:
                cluster_dist = 999   
                
            #print max_cluster
            #print max_cluster[0]
            #print coords[max_cluster[0][0]]
            #maxhit = np.amax(orig)
            #coord_mask =  orig > maxhit*0.2
            #print "****************** maximum BGO pixel: ", maxhit
           
            vertex2 = max_cluster[2][0:2] # vertex determined via largest cluster
            
            # create a list with the polygons of the BGO hit (pixel with E deposit)
            bgo_hit_bounds = [[100000,-100000],[100000,-100000]] # x_min, x_max, y_min, y_max
            polygon_hitlist, all_hitrects = get_list_of_polygons(max_cluster, coords, bgo_hit_bounds)
            #print bgo_hit_bounds
            if printVerbose: 
                print "Number of BGO pixel over threshold: ", len(polygon_hitlist)            
                print "Vertex from BGO pixelmap (charge weighted pixels of largest cluster): ", vertex2
        
        if BGOon != True:
            nr_of_cluster = 999
            cluster_dist = 999
            polygon_hitlist = []
            #vertex = [999,999]
            vertex2 = [999,999]

        ##############################################################################################################################################################################################
        ##############################################################################################################################################################################################
        if printVerbose: 
            print " "
            print "###########################################################################################"
            print "############################         2D TRACKING:        ##################################"    
            print "---------------------------        TRACK FINDING 2D        --------------------------------"

        
        trackcollection = 999
        linepoints = 999
        bgopoints = 999

        if TPXon == True:
            tpx_length = 28.0 # mm      
            tpx_hit_bounds = [[-0.5*tpx_length,0.5*tpx_length],[-0.5*tpx_length,0.5*tpx_length]]
            ext = [(0.5*tpx_length, 0.5*tpx_length),  (-0.5*tpx_length, 0.5*tpx_length), (-0.5*tpx_length, -0.5*tpx_length), (0.5*tpx_length, -0.5*tpx_length)]            
            tpx_polygon = Polygon(ext)  
            
            bounds_for_2D_vertex = [-0.5*tpx_length, 0.5*tpx_length] 
               
               
            # when fitting in 2D with timepix, we assume a vertex at (0,0)!!   (need a 3rd point to fit)               
            vertex2 = [0.0,0.0]
            vertex_err = [tpx_length,tpx_length]
        
        
            if nI > 0 or nO > 0:
                trackcollection, linepoints, bgopoints = track_finding_polygons_tpx(pmapI, cmap, currI, currO, tpx_polygon,
                                                         plot_2Dtrackfinding, tpx_hit_bounds, innerCFs, outerCFs, innerzPos, outerzPos, printVerbose) #, xm, ym)
                #hitcollection, linepoints = track_finding_circle(pmapI, cmap, currI, currO, circle, xm, ym)
                
        elif BGOon == True:
            if nI > 0 or nO > 0:
                trackcollection, linepoints, bgopoints = track_finding_polygons(pmapI, cmap, currI, currO, polygon_hitlist,
                                                         plot_2Dtrackfinding, bgo_hit_bounds,innerCFs, outerCFs, innerzPos, outerzPos, printVerbose) #, xm, ym)
                                                                         
                #circle = Circle(xy=(0.0,0.0), radius=45.0,  color = 'white', lw=2, alpha = 0.5)
                #vertex_err = [5,5]
                #vertex2 = [0,0]
                #trackcollection, linepoints, bgopoints = track_finding_circle(pmapI, cmap, currI, currO, circle,
                                               #plot_2Dtrackfinding, bgo_hit_bounds,innerCFs, outerCFs, innerzPos, outerzPos, 0.0, 0.0)        
        
        if trackcollection == 999:
            nr_of_tracks_2D = 0 
        else:      
            nr_of_tracks_2D = len(trackcollection)  
        #nr_of_tracks_2D = 0 
        
        if nr_of_tracks_2D == 0:
            trackcollection = None
        
        if printVerbose: 
            print "Final Hitcollection:      #################################################################" 
            if nr_of_tracks_2D > 0:
                for i in trackcollection:
                    print i
                       
                print " "

            print "--------------------------        TRACK FITTING 2D        ---------------------------------"
            print "Fitting", nr_of_tracks_2D, "tracks "
            
        line_params = []
        angle = 999
     
        if nr_of_tracks_2D > 1:
            vertex_err = [20,20]
            line_params, fitlines_points, trackscoll = do_le_2Dfitting(trackcollection, I, O, vertex2, vertex_err, chargeI, chargeO) 

        if printVerbose: 
            print "########################        VERTEX 2D through fitting:       ##########################"       
        
        if TPXon == True:
            if printVerbose: 
                print "Vertex at (0,0) assumed for fitting !! "     
               
        angle1 = 999
        angle2 = 999
        angle3 = 999
        angleY = 999
        #mean_angleY = 999 
        orientatio = [999,999,999]
        mtd3 = 999
        mtd_min_max = 999
        fitlines_points = 999
        line_paras = 999
        vertex_points = 999
        vertex_2d_lines = [999,999]
                    
        #vertex_points = 999
        if nr_of_tracks_2D > 1:
            vertex_points, vertex_2d_lines = determine_vertex(line_params, bounds_for_2D_vertex) # vertex is the arithm mean of vertex points

        if math.isnan(vertex_2d_lines[0]) or math.isnan(vertex_2d_lines[1]):
            vertex_2d_lines = [999,999]
            
        if printVerbose: 
            print "Vertex via 2D lines: ", vertex_2d_lines
        
        if nr_of_tracks_2D == 1:
            angleY, orientatio = calc_angle_for_1track(trackcollection, vertex2)
            

        if nr_of_tracks_2D > 1:
            mtd3, angle_all, angleY, mtd_min_max, orientatio = calc_mtds_of_tracks_corr_std(trackcollection, innerCFs, outerCFs, innerzPos, outerzPos, vertex2) #, rinner, router)   
            angle1 = np.amax(angle_all)
            
            angle_all = np.setdiff1d(angle_all,angle1)
            angle2 = 0
            angle3 = 0        
            
            if nr_of_tracks_2D  == 2:
                angle = angle1
            
            if nr_of_tracks_2D > 2:
                angle2 = np.amax(angle_all)
                angle_all = np.setdiff1d(angle_all,angle2)
                if len(angle_all) > 1:
                    angle3 = np.amax(angle_all)
                else:
                    angle3 = angle2
                    angle2 = angle1
                    #angle3 = angle1
                #angle_all = np.setdiff1d(angle_all,angle3)
                    
        mean_angleY = np.mean(np.array(angleY)) 
        
   
        # initialise variables for 3D tracking
        nr_of_fit_lines_ = 999 
        mean_d_pt_ = [999,999,999]
        av_d_pt_ = [999,999,999]
        n_cluster_= 999
        biggest_cluster_ = [999,999,999,999,999]
        dists_ = [999]
        vertex_fitted_ = [999,999,999]
        vf_covar_ = [[999,999,999],[999,999,999],[999,999,999]]
        cov_l_cl_ = [[999,999,999],[999,999,999],[999,999,999]]
        tot_cov_ = [[999,999,999],[999,999,999],[999,999,999]]
        av_dist_cl_ = 999
        plt_yes = False
        hess_bool_ = 999
        tracks_in_vert_ = 999
                
        if fibrebool == True:
            if printVerbose: 
                print " "
                print "###########################################################################################"
                print "############################         FIND 3D TRACKS:        ###############################"
                #innerzPos = -innerzPos
                #outerzPos = -outerzPos
                           
            zerrI = 59 #           
            zerrO = 73 #
                                                                      
            mps__, nr_of_fit_lines_, mean_d_pt_, av_d_pt_, n_cluster_, biggest_cluster_, dists_, vertex_fitted_, vf_covar_, cov_l_cl_, tot_cov_, av_dist_cl_, plt_yes, hess_bool_, tracks_in_vert_ = track_finding_4layers(polygon_hitlist, h, nO_all, nI_all, I_new_all, O_new_all,
                                                                                    innerzPos[:,1], outerzPos[:,1], zerrI, zerrO, cuspRunNumber, evtnumber, plotyes3D, printVerbose)
            """if len(mps__)> 0:
                for di_, dis_ in zip(mps__, dists_):
                    file_dists.write("%s " % evtnumber)
                    file_dists.write("%s " % bgo_E)
                    file_dists.write("%s " % nI)
                    file_dists.write("%s " % nO)
                    #file_dists.write("%s " % biggest_cluster_[0])
                    #file_dists.write("%s " % av_dist_cl_)
                    #file_dists.write("%s " % av_all_dists)
                    file_dists.write("%s " % di_[0]) 
                    file_dists.write("%s " % di_[1]) 
                    file_dists.write("%s " % di_[2]) 
                    file_dists.write("%s " % dis_) 
                    
                    file_dists.write("\n")
                file_dists.flush()  # """        
            
            
              
        if simplecuts == True:
            if printVerbose: 
                print "##############################         SIMPLE CUTS        #################################"
                
            if nr_of_tracks_2D < 2: 
                continue
                
            if BGOon == True:    
                if bgo_E < BGOcut:
                    continue
                              
            if nr_of_tracks_2D == 2 and angle1 > 160:
                continue
                    
            if mtd > 0.4:
                continue 

        ###################################################################################################

        timestampx = 999
                        
        if time_stamp_mode == "ADC_ts":
            timestampx = adcTimestamp                                            
        else:
            timestampx = startmixtime-ts  
        if printSummary == True:           
            print "###########################################################################################"#
            print "##############################        EVENT SUMMARY        ################################"
            print " "
            print "Cusp, Midas, Event #: ", cuspRunNumber, midas_run_nr, evtnumber
            if Timeintervallconstr:
                print "Time after mixing: ", timestampx, "s"
            if BGOon:
                print "BGO Energy (MeV): ", bgo_E
                print "Number of cluster in BGO pixel map: ", nr_of_cluster
            print "Number of hits in inner and outer hodoscope: ", nI_all, nO_all
            print " "
            print "--------------------------------- 2D tracking results: ------------------------------------"
            print "2D tracks: ", nr_of_tracks_2D
            print "Largest angles between tracks: ", angle1, angle2, angle3, "degree" 
            print "Mean Angle with Y axis: ", mean_angleY, "degree"
            print "Vertex from 2D line fitting: ", vertex_2d_lines
            if orientatio:
                print "#tracks up/down/horizontal: ", orientatio
            print "Mean time difference (ToF) ", mtd3, "ns" 
            print " "
            print "------------------------- 3D tracking results:  ----- PRELIMINARY -------------------------"
            print "3D tracks: ", nr_of_fit_lines_
            print "Weighted average of largest cluster: ", biggest_cluster_[2], biggest_cluster_[3], biggest_cluster_[4]
            print "Fitted vertex: ", vertex_fitted_
            print "###########################################################################################"#

          
        if plt_yes == True:
            if plotshow == True:
                plt.show() 
            plt.close('all')
            

        if plotyes2D == True:         
            if BGOon == True:             
                bgopoints = np.array(bgopoints)
                plot_event_2D_2(evtnumber,cuspRunNumber,h, b, linepoints, trackcollection, all_hitrects, bgopoints, cluster_points, line_params,
                fitlines_points, mtd3, ts, plot_2Dtrackfinding, vertex_points, vertex_2d_lines, vertex2) 
            elif TPXon == True:
                plot_event_2D_tpx(evtnumber, cuspRunNumber, h, linepoints, trackcollection, tpx_polygon,
                line_params, fitlines_points, ts, plot_2Dtrackfinding, vertex_points, vertex_2d_lines, nr_of_tracks_2D)
                
            if plotshow == True:
                plt.show()     #"""
            plt.close('all')
        
        if writeTxt == True:
                    file_mtds = open("./data_files_tpx_Molybdenum/timepix_midas{}.dat".format(midas_run_nr), 'w') 
        
            #for nb in noise_bars_O:
        
                    #file_mtds.write("%s " % nb)
                    #file_mtds.write("%s " % h.o.CFU[nb])
                    #file_mtds.write("%s " % h.o.CFD[nb])
                    #file_mtds.write("%s " % h.o.AmpU[nb])
                    #file_mtds.write("%s " % h.o.AmpD[nb])
                    file_mtds.write("%s " % cuspRunNumber) # 0             # cusp run number
                    file_mtds.write("%s " % midas_run_nr)                  # midas run number
                    file_mtds.write("%s " % evtnumber)                     # event number (starts at 0)
                    file_mtds.write("%s " % bgo_E)     # 3                 # total energy deposit in BGO
                    file_mtds.write("%s " % nI)           # 4              # number of bars showing a hit in the inner layer
                    file_mtds.write("%s " % nO)           # 5              # number of bars showing a hit in the outer layer
                    file_mtds.write("%s " % nr_of_tracks_2D) # 6           # number of 2D tracks (Hough like algorithm)
                    file_mtds.write("%s " % angle1) # 7                    # largest angle between all tracks of an event
                    file_mtds.write("%s " % angle2) # 8                    # second largest angle between all tracks of an event
                    file_mtds.write("%s " % angle3) # 9                    # third largest angle between all tracks of an event
                    file_mtds.write("%s " % mean_angleY) #10               # mean of all angles of tracks with y-axis 
                    file_mtds.write("%s " % orientatio[0]) #11             # number of tracks in upper half of hodoscope
                    file_mtds.write("%s " % orientatio[1]) #12             # number of tracks in bottom half of hodoscope
                    file_mtds.write("%s " % orientatio[2])    #13          # number of horizontal tracks (tracks in panels perpendicular to ground)
                    file_mtds.write("%s " % tot_events) # 14               # number of total events
          
                    file_mtds.write("%s " % mtd3) #15                      # ToF calculated via std-method (see thesis)
                    file_mtds.write("%s " % mtd_min_max) #16               # ToF calculated via (max-min)-method (see thesis)
                    
                    file_mtds.write("%s " % vertex_2d_lines[0])            # x component of vertex calculated with crossing of 2D fitted tracks
                    file_mtds.write("%s " % vertex_2d_lines[1])            # y component of vertex calculated with crossing of 2D fitted tracks

                    file_mtds.write("%s " % vertex2[0])                    # x component of vertex found via cluster finding in BGO pixel map
                    file_mtds.write("%s " % vertex2[1])                    # y component of vertex found via cluster finding in BGO pixel map
                    
                    file_mtds.write("%s " % nr_of_cluster) # 21            # number of clusters found in BGO pixel map
                    file_mtds.write("%s " % cluster_dist)                  # largest distance between clusters in pixel map
                    file_mtds.write("%s " % number_of_noise_hits_I)        # number of noise hits, inner layer (see function is_it_a_hit)
                    file_mtds.write("%s " % number_of_noise_hits_O)        # number of noise hits, outer layer (see function is_it_a_hit)
                    file_mtds.write("%s " % number_of_orphans_I)           # number of orphan hits, inner layer
                    file_mtds.write("%s " % number_of_orphans_O)           # number of orphan hits, outer layer
                    
                    file_mtds.write("%s " % nr_of_fit_lines_) # 27         # number of 3D tracks
                    
                    file_mtds.write("%s " % mean_d_pt_[0]) # 28            # x component of vertex, determined from 3D tracks via mean of all mid-points between track pairs 
                    file_mtds.write("%s " % mean_d_pt_[1])                 # y component of vertex, determined from 3D tracks via mean of all mid-points between track pairs 
                    file_mtds.write("%s " % mean_d_pt_[2])                 # z component of vertex, determined from 3D tracks via mean of all mid-points between track pairs 
                    
                    file_mtds.write("%s " % av_d_pt_[0]) # 31              # x component of vertex, determined from 3D tracks via average of all mid-points between track pairs (weighted with distance) 
                    file_mtds.write("%s " % av_d_pt_[1])                   # y component of vertex, determined from 3D tracks via average of all mid-points between track pairs (weighted with distance) 
                    file_mtds.write("%s " % av_d_pt_[2])                   # z component of vertex, determined from 3D tracks via average of all mid-points between track pairs (weighted with distance) 
                    
                    file_mtds.write("%s " % n_cluster_)                    # number of clister found in the cloud of mid-points
                    file_mtds.write("%s " % biggest_cluster_[2])  # 35     # x component of vertex found via largest cluster
                    file_mtds.write("%s " % biggest_cluster_[3])           # y component of vertex found via largest cluster
                    file_mtds.write("%s " % biggest_cluster_[4])           # z component of vertex found via largest cluster
                    
                    file_mtds.write("%s " % biggest_cluster_[0]) # 38      # nr of mid-points in cluster 
                    file_mtds.write("%s " % biggest_cluster_[1])           # "density" of cluster = sum of all distances between mid-points in cluster devided by number of mid-points
                                                    
                    file_mtds.write("%s " % vertex_fitted_[0]) # 40        # x component of vertex determined by vertex-fit
                    file_mtds.write("%s " % vertex_fitted_[1])             # y component of vertex determined by vertex-fit
                    file_mtds.write("%s " % vertex_fitted_[2])             # z component of vertex determined by vertex-fit

                    file_mtds.write("%s " % vf_covar_[0][0]) # 43          # diagonal entry (0,0) of covariance matrix of method using vertex fit
                    file_mtds.write("%s " % vf_covar_[1][1])               # diagonal entry (1,1) of covariance matrix of method using vertex fit
                    file_mtds.write("%s " % vf_covar_[2][2])               # diagonal entry (2,2) of covariance matrix of method using vertex fit
                    
                    file_mtds.write("%s " % cov_l_cl_[0][0]) # 46          # diagonal entry (0,0) of covariance matrix of method using largest cluster
                    file_mtds.write("%s " % cov_l_cl_[1][1])               # diagonal entry (1,1) of covariance matrix of method using largest cluster
                    file_mtds.write("%s " % cov_l_cl_[2][2])               # diagonal entry (2,2) of covariance matrix of method using largest cluster 
                     
                    file_mtds.write("%s " % tot_cov_[0][0]) # 49           # diagonal entry (0,0) of covariance matrix of method using mean of all mid-points
                    file_mtds.write("%s " % tot_cov_[1][1])                # diagonal entry (1,1) of covariance matrix of method using mean of all mid-points
                    file_mtds.write("%s " % tot_cov_[2][2])                # diagonal entry (2,2) of covariance matrix of method using mean of all mid-points 
                    
                    file_mtds.write("%s " %tracks_in_vert_ )  #52          # number of tracks contributing to biggest cluster                                                                                                        
                                                             
                    if writeTxt == True:    
                            file_mtds.write("%s " % timestampx)            # timestamp
                            file_mtds.write("\n")
                            file_mtds.flush()   
            
            
                
        """cand_list.append([cuspRunNumber, evtnumber, bgo_E, nI_all, nO_all, nr_of_tracks_2D, timestampx, 0.0, mtd3, mtd_min_max, angle1, angle2, angle3,
        vertex[0], vertex[1], vertex2[0], vertex2[1], nr_of_cluster, cluster_dist, mean_angleY, orientatio[0], orientatio[1], orientatio[2], number_of_noise_hits_I,
        number_of_noise_hits_O, number_of_orphans_I, number_of_orphans_O])"""
        
        if printVerbose:      
            print "------------------------------------------------------------------------------------------"    
            print "Total events in run:", tot_events #, timestampx
            print "------------------------------------------------------------------------------------------"
                
    ########################################################################################################################################################################
    ############################################################################################################################################################################ 
    ############################################################################################################################################################################
    ############################################################################################################################################################################ 
    # write ROOT files if ordered
    #for what in cand_list:
    #        print what
            
    # FIXME this is NOT up to date !!!        
    if writeROOT and len(cand_list) > 0:
        from rootpy.tree import Tree
        from rootpy.io import root_open

        filename = "./rootfiles_hbarcands/{}.root".format(cuspRunNumber)

        f = root_open(filename, "recreate")

        outT = Tree("candidates")

        outT.create_branches(
            {'cuspRunNumber': 'I',
             'midasRunNumber': 'I',
             'eventNumber': 'I',
             'bgoEdep': 'D',
             'hitsInner': 'I',
             'hitsOuter': 'I',
             'NrofTracks': 'I',
             'startMix': 'D',
             'time': 'D',
             #'MTDCombi': 'D',
             'MTDStd': 'D',
             'MTDMinMax': 'D',
             'Angle1': 'D',
             'Angle2': 'D',
             'Angle3': 'D',
             'Vertexx': 'D',
             'Vertexy': 'D',
             'Vertexx2': 'D',
             'Vertexy2': 'D',
             'NrCluster' : 'D',
             'ClDist' : 'D',
             'meanAngleY' : 'D',
             'Nup' : 'I',             
             'Ndown' : 'I',
             'Nhoriz' : 'I',
             'NoiseHI' : 'I',
             'NoiseHO' : 'I',
             'OrphHI' : 'I',
             'OrphHO' : 'I'             
             })
     
        for n,i in enumerate(cand_list):
                outT.cuspRunNumber = cuspRunNumber
                outT.midasRunNumber = 999
                outT.eventNumber = i[1]
                outT.bgoEdep = i[2]
                outT.hitsInner = i[3]
                outT.hitsOuter = i[4]
                outT.NrofTracks = i[5]
                outT.startMix = startmixAdc
                outT.time = i[6]
               
                #outT.MTDCombi = i[7]
                outT.MTDStd= i[8]
                outT.MTDMinMax = i[9]
                
                outT.Angle1 = i[10]
                outT.Angle2 = i[11]
                outT.Angle3 = i[12]
                outT.Vertexx = i[13]
                outT.Vertexy = i[14]
                outT.Vertexx2 = i[15]
                outT.Vertexy2 = i[16]
                outT.NrCluster = i[17]
                outT.ClDist = i[18]
                outT.meanAngleY = i[19]
                outT.Nup = i[20]       
                outT.Ndown = i[21]
                outT.Nhoriz =i[22]
                outT.NoiseHI =i[23]
                outT.NoiseHO =i[24]
                outT.OrphHI =i[25]
                outT.OrphHO = i[26]

                outT.fill()
        outT.write()
        f.close()
